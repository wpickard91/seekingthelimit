'use strict';
var concatCss = require('gulp-concat-css');
var minifyCss = require('gulp-minify-css');
var watchify = require('watchify');
var browserify = require('browserify');
var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var assign = require('lodash.assign');
var babelify = require("babelify");
var notify = require("gulp-notify");
var $ = require("jquery");

// Process CSS into one file
gulp.task('css', function() {
    return gulp.src('./resources/css/*.css')
        .pipe(concatCss('bundle.css'))
        .pipe(minifyCss())
        .pipe(gulp.dest('public/css'));
});

// add custom browserify options here
var customOpts = {
    entries: ['./resources/js/v2/router.js'],
    sourceType: "module",
    debug: true
};
var opts = assign({}, watchify.args, customOpts);
var b = watchify(browserify(opts));

b.transform(babelify);
// add transformations here
// i.e. b.transform(coffeeify);

gulp.task('js', bundle); // so you can run `gu  lp js` to build the file
b.on('update', bundle); // on any dep update, runs the bundler
b.on('log', gutil.log); // output build logs to terminal
gulp.task('trumbo', bundleTrumboWyg);

function bundle() {
    return b.bundle()
        // log errors if they happen
        .on('error', gutil.log.bind(gutil, 'Browserify Error'))
        .pipe(source('bundle.js'))
        // optional, remove if you don't need to buffer file contents
        .pipe(buffer())
        // optional, remove if you dont want sourcemaps
        .pipe(sourcemaps.init({loadMaps: true})) // loads map from browserify file
        // Add transformation tasks to the pipeline here.
        .pipe(sourcemaps.write('./')) // writes .map file
        .pipe(gulp.dest('./public/js'))
        .pipe(notify({
            title: "Ayyyy it compiled",
            message: "nah it's done for real though"
        }));
}

function bundleTrumboWyg() {
    gulp.src('node_modules/trumbowyg/dist/trumbowyg.min.js')
    .pipe(gulp.dest('./public/js'));

    gulp.src('node_modules/trumbowyg/dist/ui/trumbowyg.min.css')
    .pipe(gulp.dest('./public/css'));

    gulp.src('node_modules/trumbowyg/dist/ui/icons.svg')
    .pipe(gulp.dest('./js/icons'));
}
