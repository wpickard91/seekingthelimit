// Main entry point for all client side js calls
import React from 'react'
import ReactDOM from 'react-dom'

import { Router, Route, Link } from 'react-router'
import createBrowserHistory from 'history/lib/createBrowserHistory'
import injectTapEventPlugin from 'react-tap-event-plugin';
import Home from './Pages/Home';
import Articles from './Pages/Articles';
import Galleries from './Pages/Galleries';

// Needed for onTouchTap
// Can go away when react 1.0 release
// Check this repo:
// https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();

const history = createBrowserHistory();

ReactDOM.render(
    (
        <Router history={history}>
            <Route path="/" component={Home} />
            <Route path="home" component={Home} />
            <Route path="articles" component={Articles} />
            <Route path="galleries" component={Galleries} />
        </Router>
    ),
    document.getElementById('contentContainer')
);