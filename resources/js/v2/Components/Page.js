import React from 'react'

export default class Page extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <section>
                <h1>Base page</h1>
                { this.props.children }
            </section>
        )
    }
}