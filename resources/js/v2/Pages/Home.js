import React from 'react';
import Page from '../Components/Page';
import { Link } from 'react-router';

export default class Home extends Page {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <h1>Home</h1>
                <div>
                    <p>todo: put some recent pics</p>
                    <p>todo: put some recent articles</p>
                    <p>todo: put a bio</p>
                    <p>todo: put a contact</p>
                    <p><Link to="/galleries">Galleries</Link></p>
                    <p><Link to="/articles">Articles</Link></p>
                </div>
            </div>
        )
    }
}