// Main entry point for all client side js calls

/**
 * Start by setting up global cache system
 */
console.log("router");
import EntityCache from './Util/EntityCache'

var cache = EntityCache.installCache();
cache.setLifeSpan('posts', 1000);

import React from 'react'
import ReactDOM from 'react-dom'
import ContentContainer from './components/ContentContainer'
import Post from './components/Posts/Post'

import App from './components/App'
import Contact from './pages/Contact'
import { Router, Route, Link } from 'react-router'
import createBrowserHistory from 'history/lib/createBrowserHistory'
import PostIndex from './pages/PostIndex'
import AdminPostIndex from './pages/AdminPostIndex'
import Admin from './pages/Admin'
import AdminCategoriesIndex from './pages/AdminCategoriesIndex'
import CategoryCreate from './pages/CategoryCreate'
import EditIndex from './pages/Admin/EditIndex'
import EditPost from './pages/Admin/EditPost'
import CreatePost from './pages/Admin/CreatePost'

import EditStoryIndex from './pages/Admin/Stories/EditStoryIndex';
import CreateStory from './pages/Admin/Stories/CreateStory';
import AdminStoriesIndex from './pages/Admin/Stories/AdminStoriesIndex';
import EditStory from './pages/Admin/Stories/EditStory';

import Home from './pages/Home'
import About from './pages/About'
import injectTapEventPlugin from 'react-tap-event-plugin';

// Needed for onTouchTap
// Can go away when react 1.0 release
// Check this repo:
// https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();

const history = createBrowserHistory();

ReactDOM.render((
        <Router history={history}>
            <Route path="/" component={App}>
                <Route path="home" component={Home} />
                <Route path="about" component={About} />
                <Route path="contact" component={Contact} />
                <Route path="posts" component={PostIndex}>
                    <Route path=":id" component={Post} />
                </Route>
            </Route>

            <Route path="admin" component={Admin}>
                <Route path="posts" component={AdminPostIndex}>
                    <Route path="create" component={CreatePost} />
                    <Route path="edit" component={EditIndex}>
                        <Route path=":id" component={EditPost} />
                    </Route>
                </Route>

                <Route path="categories" component={AdminCategoriesIndex}>
                    <Route path="create" component={CategoryCreate}/>
                </Route>

                <Route path="stories" component={AdminStoriesIndex}>
                    <Route path="create" component={CreateStory} />
                    <Route path="edit" component={EditStoryIndex}>
                        <Route path=":id" component={EditStory} />
                    </Route>
                </Route>
            </Route>

        </Router>
    ),
    document.getElementById('contentContainer')
);