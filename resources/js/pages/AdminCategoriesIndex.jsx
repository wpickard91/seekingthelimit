import React from 'react'

var AdminCategoriesIndex = React.createClass({
    render() {
        return (
            <div className="adminCategoriesIndex">
                <p>Admin Categories Index</p>
                {this.props.children}
            </div>
        );
    }
});

export default AdminCategoriesIndex;