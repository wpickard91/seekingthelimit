import React from 'react'

var CategoryCreate = React.createClass({
    getInitialState() {
        return {
            name: ''
        }
    },
    handleSubmit(e) {
        e.preventDefault();
        $.ajax({
            url: '/categories/categoriesApi/v0/create',
            data: {name: this.state.name},
            dataType: 'json',
            method: 'POST',
            success: function(data) {
                //TODO Show some sort of confirmation here
                if (data.status == 'success') {
                    this.setState({
                        name: ''
                    });
                }
            }.bind(this),
            error: function(xhr, status, err) {
                console.log('CategoryCreate::handleSubmit, error occured. ', xhr, ' ', status, ', ', err);
            }
        });
    },
    handleNameChange(e) {
        this.setState({name: e.target.value})
    },
    render() {
        return (
            <div className='categoryCreate'>
                <p>Create a new Category</p>
                <form onSubmit={this.handleSubmit} className="form">
                    <div className="form-group">
                        <label htmlFor="name" className="control-label">Name</label>
                        <input
                            type="text"
                            name="name"
                            className="form-control"
                            onChange={this.handleNameChange}
                            value={this.state.name}
                        />
                    </div>

                    <div className="form-group">
                        <input
                            type="submit"
                            className="btn btn-default"
                            value="Submit"
                        />
                    </div>
                </form>
            </div>
        )
    }
});

export default CategoryCreate;