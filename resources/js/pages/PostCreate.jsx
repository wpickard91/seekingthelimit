import React from 'react'
import CategoryPicker from '../components/Categories/CategoryPicker'
import KeywordCreator from '../components/Keywords/KeywordCreator'
import TagList from '../components/Keywords/TagList'

import PostForm from '../components/Posts/PostForm'
import ImageInput from '../components/Images/ImageInput'
import EditGallery from '../components/Images/EditGallery'


import PostController from '../Controllers/PostController'
import ImageController from '../Controllers/ImageController'
import KeywordController from '../Controllers/KeywordController'

var PostCreate = React.createClass({
    getInitialState: function() {
        console.log(this.props);
        return {
            post_id: this.props.initialPostId || 0,
            title: this.props.initialTitle || '',
            description: this.props.initialDescription || '',
            body: this.props.initialBody|| '',
            images: this.props.initialImages || [],
            categoryId: this.props.initialCategoryId || 0,
            keywords: this.props.initialKeywords || [],
            uploadedImages: this.props.uploadedImages || []
        }
    },
    handleTitleChange: function(e) {
        this.setState({title: e.target.value})
    },
    handleDescriptionChange: function(e) {
        this.setState({description: e.target.value})
    },
    handleBodyChange: function(e) {
        this.setState({body: e.target.value})
    },
    handleImagesChange: function(e) {
        var files = e.target.files;
        this.setState({images: files});
    },
    handleCategoryIdChange: function(category_id) {
        console.log(category_id);
        this.setState({
            categoryId: category_id
        });
    },
    handleKeywordChange: function(keyword) {
        this.setState({
            keywords: this.state.keywords.concat([keyword])
        });
    },
    handleSubmit: function(e) {
        e.preventDefault();
        var self = this,
            postData = {
                post_id: this.state.post_id,
                title: this.state.title,
                description: this.state.description,
                body: this.state.body
            },
            data = {
                images: this.state.images,
                keywords: this.state.keywords,
                category_id: this.state.categoryId
            },
            createMethod = PostController.postCreate;

        // in order to treat this as the edit page or the create page,
        // just check to see if there is a post id
        if (this.state.post_id) createMethod = PostController.update;
        console.log('this.state.post_id: ', this.state.post_id);
        // first upload and create the post
        createMethod(postData, function(post) {
            self.setState({post_id: post.id});

            // upload all the images
            ImageController.uploadFileList(data.images, function(images) {
                // now create post images
                self.setState({
                    uploadedImages: images,
                    images: []
                });

                PostController.assignImagesToPost(images, post);
            });

            // also handle categories and keywords
            if(data.category_id) {
                PostController.assignCategoryToPost(data.category_id, post);
            }

            if(data.keywords.length) {
                var keyword_ids = data.keywords.map(function(keyword){
                    return keyword.id;
                });
                PostController.putKeywords(post.id, keyword_ids);
            }
        });

    },
    render: function() {
        var infoMessage = "Create a new post";
        if (this.state.post_id) {
            infoMessage = "Edit post " + this.state.post_id;
        }
        return (
            <section id="PostCreate" className="container-fluid">
                <h3>{infoMessage}</h3>
                <PostForm
                    handleSubmit={this.handleSubmit}
                    handleTitleChange={this.handleTitleChange}
                    handleDescriptionChange={this.handleDescriptionChange}
                    handleBodyChange={this.handleBodyChange}
                >

                    <div className="form-group">
                        <ImageInput
                            name="images[]"
                            onChange={this.handleImagesChange}
                            multiple="multiple"
                        />
                        <EditGallery images={this.state.uploadedImages} />
                    </div>

                    <div className="form-group">
                        <label htmlFor="category_id" className="control-label">Pick a category</label>
                        <CategoryPicker
                            updateParent= {this.handleCategoryIdChange}
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="keywords" className="control-label">Name some keywords</label>
                        <input type="hidden" name="keywords" value={this.state.keywords.map(function(tag){return tag.name})} />
                        <TagList tags={this.state.keywords} />
                        <KeywordCreator afterHarvest={this.handleKeywordChange}/>
                    </div>

                    <div className="form-group">
                        <input type="submit" className="btn btn-block btn-default" />
                    </div>
                </PostForm>
            </section>
        )
    }
});

export default PostCreate;