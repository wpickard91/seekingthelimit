import React from 'react'
import PostController from '../../Controllers/PostController'
import PostForm from '../../components/Posts/PostForm'
import ImageController from '../../Controllers/ImageController'

var CreatePost = React.createClass({
    getInitialState () {
        return {
            redirectRequirement: 0,
            stepsCompleted: 0
        }
    },
    setRedirectRequirement(i) {
        this.setState({
            redirectRequirement: i
        })
    },
    setStepsCompleted(i) {
        this.setState({
            stepsCompleted: i
        })
    },
    incrementAndAskForRedirect() {
        this.setStepsCompleted(this.state.stepsCompleted + 1);
        console.log('ask for redirect: ', this.state.stepsCompleted, this.state.redirectRequirement);
        return this.state.stepsCompleted == this.state.redirectRequirement;
    },
    incrementAndRedirectIfReady(url) {
        if(this.incrementAndAskForRedirect()) {
            window.location = url;
        }
    },
    handleSubmit(data) {
        var self = this,
            postData = {
                post_id: this.props.params.id,
                title: data.title,
                description: data.description,
                body: data.body,
                main_image_id: data.main_image_id
            },
            fileList = data.fileList,
            keywords = data.keywords,
            category_id = data.category_id;

        // set up the redirectRequirement value
        // init to 1 because we will always call create
        var r = 1;
        if (category_id) r++;
        if (fileList.length > 0) r+=2; //increment two because we will always uploadFileList and assiggnImages
        if (keywords.length) r++;
        this.setRedirectRequirement(r);

        PostController.create(postData, function(post) {
            self.incrementAndAskForRedirect();
            ImageController.uploadFileList(fileList, function(images) {
                self.incrementAndAskForRedirect();
                PostController.assignImagesToPost(images, post, function(post) {
                    self.setState({
                        images: post.images
                    });
                    self.incrementAndRedirectIfReady('/admin/posts/edit/' + post.id);
                });
            });

            if(category_id) {
                // update the category
                PostController.assignCategoryToPost(category_id, post, function() {
                    self.incrementAndRedirectIfReady('/admin/posts/edit/' + post.id);
                });
            }


            if(keywords.length) {
                var keyword_ids = keywords.map(function(kw){
                    return kw.id
                });
                PostController.putKeywords(post.id, keyword_ids, function(){
                    self.incrementAndRedirectIfReady('/admin/posts/edit/' + post.id);
                });
            }
        });
    },
    render() {
        return (
            <section id="EditPost" className="container-fluid">
                <PostForm
                    submit={this.handleSubmit}
                    />
            </section>
        )
    }
});

export default CreatePost;