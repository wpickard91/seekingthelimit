import React from 'react'
import StoryForm from '../../../components/Posts/Stories/StoryForm'

var CreateStory = React.createClass({
    render() {
        return (
            <StoryForm />
        )
    }
});

export default CreateStory;