import React from 'react'
import PostController from '../../Controllers/PostController'
import PostForm from '../../components/Posts/PostForm'
import ImageController from '../../Controllers/ImageController'

var EditPost = React.createClass({
    getInitialState() {
        return {
            post: {}
        }
    },
    handleSubmit(data) {
        var self = this,
            postData = {
                post_id: this.props.params.id,
                title: data.title,
                description: data.description,
                body: data.body,
                main_image_id: data.main_image_id
            },
            fileList = data.fileList,
            keywords = data.keywords,
            category_id = data.category_id;

        console.log('EditPost::handleSubmit, data: ' ,data, ' post: ', self.state.post);
        PostController.update(postData, function(post) {
            ImageController.uploadFileList(fileList, function(images) {
                PostController.assignImagesToPost(images, post, function(post) {
                    self.setState({
                        images: post.images
                    });
                });
            });

            if(post.category_id != category_id) {
                // update the category
                PostController.assignCategoryToPost(category_id, post, function() {});
            }


            if(keywords.length) {
                var keyword_ids = keywords.map(function(kw){
                    return kw.id
                });
                PostController.putKeywords(post.id, keyword_ids, function(){

                });
            }
        });
    },
    render() {
        return (
            <section id="EditPost" className="container-fluid">
                <PostForm
                    submit={this.handleSubmit}
                    sourceId={this.props.params.id}
                />
            </section>
        )
    }
});

export default EditPost;