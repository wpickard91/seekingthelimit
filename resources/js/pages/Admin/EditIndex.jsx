import React from 'react'
import PostController from '../../Controllers/PostController'
import {Link} from 'react-router'

var EditIndex = React.createClass({
    getInitialState() {
        return {posts: []}
    },
    componentWillMount() {
        var self = this;
        PostController.getPostsIndex(100, function(posts) {
            self.setState({posts: posts});
        });
    },
    render() {
        return (
            <section id="EditIndex" className="container-fluid">
                <ul>
                    {
                        this.state.posts.map(function (post) {
                            var href = "/admin/posts/edit/" + post.id;
                            return (
                                <li key={post.id}><Link to={href}>{post.title}</Link></li>
                            )
                        })
                    }
                </ul>
                { this.props.children }
            </section>
        )
    }
});

export default EditIndex;