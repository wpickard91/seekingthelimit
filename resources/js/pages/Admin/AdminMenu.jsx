import React from 'react'
import {Link} from 'react-router'
import ReactTabs from 'react-tabs'
var Tab = ReactTabs.Tab,
    Tabs = ReactTabs.Tabs,
    TabList = ReactTabs.TabList,
    TabPanel = ReactTabs.TabPanel;

var AdminMenu = React.createClass({
    render () {
        return (
            <ul className="nav nav-pills nav-stacked">
                <li><Link to="/admin/posts/create">Create Post</Link></li>
                <li><Link to="/admin/posts/edit">Edit Posts</Link></li>
                <li><Link to="/admin/categories/create">Create Category</Link></li>
                <li><Link to="/admin/stories/create">Create Story</Link></li>
                <li><Link to="/admin/stories/edit">Edit Stories</Link></li>
            </ul>
        );
    }
});

export default AdminMenu;