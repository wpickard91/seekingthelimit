import React from 'react'
import ReactDOM from 'react-dom'
import PostController from '../Controllers/PostController'

var About = React.createClass({
    getInitialState() {
        return {
            job: jobs[0]
        }
    },
    componentWillMount() {
        this.intervals = [];
    },
    setInterval() {
        this.intervals.push(setInterval.apply(null, arguments));
    },
    componentWillUnmount() {
        this.intervals.forEach(clearInterval);
    },
    componentDidMount() {
        // Get the components DOM node
        var elem = ReactDOM.findDOMNode(this);
        // Set the opacity of the element to 0
        elem.style.opacity = 0;
        window.requestAnimationFrame(function() {
            // Now set a transition on the opacity
            elem.style.transition = "opacity 1000ms";
            // and set the opacity to 1
            elem.style.opacity = 1;
        });

        this.setInterval(this.nextJob, 4000);
    },
    nextJob() {
        var i = jobs.indexOf(this.state.job);
        if (i >= 0) {
            if (i + 1 >= jobs.length) i = 0;
            else i = i + 1;

            var self = this;

            $("#job").animate({opacity: 0}, function() {
                self.setState({job: jobs[i]});
                $("#job").animate({opacity: 1})
            });
        }
    },
    render() {
        return (
            <section>
                <div  className="text-center">
                    <div style={style.imgContainer} className="material">
                        <img style={style.img} src="/images/me1000.jpg" />
                    </div>
                    <h3 style={style.h3}>I'm Will</h3>
                    <h4 style={style.h4}>I am <span id="job" style={style.span}>{this.state.job}</span></h4>
                </div>

                <div className="container-fluid material">
                    <div style={style.body} className="col-md-12">
                        <p>Thanks for checking out my site.</p>
                        <p>I generally post pictures from trips that I do. But occasionally I'll write a tutorial that hopefully you will find helpful.</p>
                        <p>I like to hike, backpack, mountain bike, climb, and solve problems. If you have questions about a trip that I post here, a tutorial that did not teach, or anything else: please feel free to contact me.</p>
                        <p>For those who came here to discover my technical skill set... look no further!</p>
                        <h3>Languages (in order of skill)</h3>
                        <ul>
                            <li>Brainfuck*</li>
                            <li>PHP</li>
                            <li>JavaScript</li>
                            <li>Scala</li>
                            <li>Python</li>
                            <li>C++</li>
                            <li>Java</li>
                            <li>C</li>
                        </ul>
                        <h3>Tools (in order of skill) | There are so many tools I have used, I am only going to outline the primary/major ones</h3>
                        <ul>
                            <li>MySQL</li>
                            <li>Laravel (Everything you can't see on this site is powered by Laravel! Wicked neat!</li>
                            <li>React (Everything you see on this site is a react component! That's pretty neat!)</li>
                            <li>jQuery</li>
                            <li>Django</li>
                            <li>Elasticsearch</li>
                            <li>Selenium</li>
                            <li>Jest</li>
                        </ul>
                        <p>Generally speaking, modern day business problems are relatively trivial to implement. Not everyone is developing self-driving cars or <a href="http://www.bbc.com/future/story/20130516-big-bets-on-quantum-computers">quantum fighter jets</a>.
                            That is not to say attention to an algorithm's complexity can be dismissed. However, frameworks and tool sets have allowed us to view problems from a very high layer of abstraction.
                            This has allowed us as developers to solve complicated problems faster than ever before. Because of this relative ease of problem solving, a new skill for developers to master has emerged.
                            The ability for developers to organize an application logically  and write code that is reusable is more important than ever before. The modern software developer should be just that: a developer.
                            Analogous to an architect's ability to design classically beautiful structures, a software developer's code is not gauged by its cleverness or complexity but by its structure, organization, and beauty.
                            Or maybe that's all bullshit. Just one man's opinion.</p>

                        <p><small>* I don't actually know Brainfuck. Sorry to all of you Brainfuck developers out there that thought I could be a part of your team.</small></p>
                    </div>
                </div>
            </section>
        )
    }
});

const style = {
    h1: {
        fontWeight: 300,
        letterSpacing: "20px",
        fontSize: "60px"
    },
    imgContainer: {
        width: "300px",
        height: "300px",
        borderRadius: "100%",
        overflow: "hidden",
        position: "relative",
        margin: "auto"
    },
    img: {
        position: "absolute",
        top: 0,
        left: 0,
        height: "100%"
    },
    h3: {

    },
    h4: {
        marginBottom: "50px"
    },
    body: {
        padding: "25px",
        lineHeight: "2",
        fontSize: "18px"
    }
};

const jobs = [
    "a full stack developer",
    "an adventurer",
    "a world renowned shower rapper",
    "another guy who just wants a motorcycle",
    "a dog lover",
    "a cat not-really-hate-but-I-wouldn't-say-I-like-them-er",
    "known to eat broccoli occasionally",
    "a functional programmer",
    "a mountain climber",
    "absolutely terrible at twister"
];

export default About;