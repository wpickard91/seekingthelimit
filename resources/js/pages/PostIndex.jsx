import React from 'react'
import PostController from '../Controllers/PostController'
import KeywordController from '../Controllers/KeywordController'
import FilterablePostList from '../components/Posts/FilterablePostList'
import TagList from '../components/Keywords/TagList'
import Tag from '../components/Keywords/Tag'
import CategoryPicker from '../components/Categories/CategoryPicker.jsx'
import TextInput from '../components/Forms/TextInput'
import PaginateBar from '../components/Paginate/PaginateBar'
import EntityCache from '../Util/EntityCache'
import Array from '../Util/Array'
import RadioButton from 'material-ui/lib/radio-button'
import RadioButtonGroup from 'material-ui/lib/radio-button-group'

const PC = new PostController();
const KC = new KeywordController();

var PostIndex = React.createClass({
    getInitialState: function() {
        return {
            take: 10,
            start: 0,
            end: 10,
            paginating: false,
            posts: [],
            keywords: [],
            categories: [],
            clickedKeywords: [],
            filterText: "",
            sortDirection: "desc",
            sortField: "updated_at"
        }
    },
    readUrl() {
        var s = window.location.search.replace('?', ''),
            o = {};

        s = s.split('&');

        for (var i = 0; i < s.length; i++) {
            if (s[i]) {
                var p = s[i].split('=');
                o[p[0]] = JSON.parse(p[1])
            }
        }

        return o;
    },
    writeUrl(o = {}) {
        var s = "?";
        for (var i in o) {
            s += i + "=" + o[i] + "&"
        }
        s = s.substring(0, s.length - 1);
        console.log(s);
        //window.location.search = s;
    },
    getPosts() {
        var url = this.readUrl(),
            take = url.hasOwnProperty('take') ? url['take'] : 10,
            start = url.hasOwnProperty('start') ? url['start'] : 0,
            end = url.hasOwnProperty('end') ? url['end'] : 10;

        this.paginate(take, start, end);
    },
    paginate(take, start, end) {
        var self = this;

        PC.getTeasers(function(teasers) {
            console.log("teasers: ", teasers);
            self.setPosts(teasers.skip(start));
        });
    },
    setPaginateSettings(paginate, take, start, end) {
        this.setState({
            paginating: paginate,
            take: take,
            start: start,
            end: end
        });

        var url = this.readUrl();
        url['take'] = take;
        url['start'] = start;
        url['end'] = end;
        this.writeUrl(url);

        if (paginate) {
            this.paginate(take, start, end);
        }
    },
    componentWillMount: function(){
        var self = this;

        this.getPosts();

        KC.getAllReferences(function(keywords) {
            console.log('keywords: ', keywords);
            self.setKeywords(keywords);
        });

        this.installEventListeners();
    },
    componentWillUnmount: function() {
        this.removeEventListeners();
    },
    installEventListeners: function() {
       /* window.addEventListener("scroll", function(e) {
            var teasers = document.getElementsByClassName('teaser');
            if (teasers.length > 0) {
                var t = teasers[teasers.length - 1],
                    y = t.getBoundingClientRect().top + t.clientHeight;

                if (window.innerHeight + window.scrollY > y) {
                    console.log("paginate");
                    if (!this.state.paginating) {
                        this.setPaginateSettings(10, this.state.start + 10, this.state.end + 10);
                    }
                }
            }
        }.bind(this));*/
    },
    removeEventListeners: function() {
       // window.removeEventListener("scroll");
    },
    setPosts: function(posts)   {
        this.setState({
            posts: posts
        });
    },
    setKeywords: function(keywords) {
        this.setState({
            keywords: keywords
        });
    },
    handleTagClick: function(tag) {
        // only add to clicked keywords if they have not been clicked yet
        for (var i = 0; i < this.state.clickedKeywords.length; i++) {
            if (this.state.clickedKeywords[i].id == tag.id) {
                return;
            }
        }

        var t = {
            id: tag.id,
            name: tag.name + "  x"
        };
        this.setState({
            clickedKeywords: this.state.clickedKeywords.concat([t])
        })
    },
    handleTagUnclick: function(tag) {
        for(var i = 0; i < this.state.clickedKeywords.length; i++) {
            if (this.state.clickedKeywords[i].id == tag.id) {
                this.state.clickedKeywords.splice(i, 1);
                this.setState({
                    clickedKeywords: this.state.clickedKeywords
                });
                return;
            }
        }
    },
    handleCategorySelection: function(cat) {
        if (cat == "Select a category") {
            this.setState({categories: []})
        }
        else {
            this.setState({
                categories: [cat]
            })
        }
    },
    handleFilterTextChange: function(e) {
        this.setState({
            filterText: e.target.value
        });
    },
    handlePaginateClick: function() {
        this.setPaginateSettings(true, this.state.take + 10, this.state.start, this.state.end + 10);
    },
    handleSortDirectionChange(e, direction) {
        this.setSortSettings(this.state.sortField, direction)
    },
    handleSortFieldChange: function(e, field){
        this.setSortSettings(field, this.state.sortDirection)
    },
    setSortSettings: function(field, direction) {
        this.setState({
            sortField: field,
            sortDirection: direction
        })
    },
    render() {
        var paginateStart = 10,
            paginateEnd = 50,
            increment = 10;
        if (paginateStart < this.state.start - 20) {
            paginateStart = this.state.start- 20;
            paginateEnd = paginateStart + 50;
        }

        return (
            <section className="postIndex container-fluid page">
                <div className="filterControls col-lg-2 col-md-4 col-sm-12">
                    <div className="filterItem">
                        <p className="elegant">Search</p>
                        <TextInput name="q" onChange={this.handleFilterTextChange} className="form-control search" />
                    </div>

                    <div className="filterItem">
                        <p className="elegant">Filter by keyword</p>
                        <TagList tags={this.state.keywords}
                            onTagClick={this.handleTagClick}
                         />
                    </div>

                    <div className="filterItem">
                        <p className="elegant">Fitler by category</p>
                        <div className="">
                            <CategoryPicker updateParent={this.handleCategorySelection} />
                        </div>
                    </div>

                    <div className="filterItem">
                        <p className="elegant">Sort <br/>direction</p>
                        <RadioButtonGroup name="sortDirection"
                                          defaultSelected="desc"
                                          onChange={this.handleSortDirectionChange}
                            >
                            <RadioButton value="asc" label="ASC" />
                            <RadioButton value="desc" label="DESC" />
                        </RadioButtonGroup>
                        <p className="elegant">Sort by</p>
                        <RadioButtonGroup name="sortField"
                                          defaultSelected="date"
                                          onChange={this.handleSortFieldChange}
                            >
                            <RadioButton value="date" label="Date" />
                            <RadioButton value="title" label="Title" />
                        </RadioButtonGroup>
                    </div>
                </div>
                <div className="col-lg-10 col-md-8 col-sm-12">
                    {this.props.children}
                    <TagList tags={this.state.clickedKeywords}
                             onTagClick={this.handleTagUnclick}
                    />
                    <FilterablePostList
                        posts={this.state.posts}
                        categories={this.state.categories}
                        keywords={this.state.clickedKeywords}
                        filterText={this.state.filterText}
                        limit={this.state.take}
                        sortField={this.state.sortField}
                        sortDirection={this.state.sortDirection}
                        postsPerRow={4}
                    />
                    <div className="col-sm-12">
                        <a className="btn btn-block btn-primary" onClick={this.handlePaginateClick}>More</a>
                    </div>
                </div>
            </section>
        )
    }
});

export default PostIndex;