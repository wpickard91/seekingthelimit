import React from 'react'
import Form from '../components/Forms/Form'
import TextInput from '../components/Forms/TextInput'

var Contact = React.createClass({
    getInitialState() {
        return {
            name: "",
            email: "",
            body: ""
        }
    },

    sayThanks() {

    },

    handleNameChange(e) {
        this.setState({
            name: e.target.value
        });
    },

    handleEmailChange(e) {
        this.setState({
            email: e.target.value
        });
    },

    handleBodyChange(e) {
        this.setState({
            body: e.target.value
        });
    },

    handleSubmit() {

    },

    render: function() {
        return (
            <section id="contactPage" className="page container">
                <h1>Contact</h1>

            </section>
        )
    }
});

const styles = {

};

export default Contact;