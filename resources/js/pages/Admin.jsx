import React from 'react'
import AdminMenu from './Admin/AdminMenu'

var Admin = React.createClass({
    render() {
        return (
            <div className="admin page">
                <h1>Admin Home</h1>
                <div className="container-fluid">
                    <div className="col-sm-12 col-md-4">
                        <AdminMenu />
                    </div>
                    <div className="col-sm-12 col-md-8">
                        { this.props.children }
                    </div>
                </div>
            </div>
        )
    }
});

export default Admin;