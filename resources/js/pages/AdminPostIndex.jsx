import React from 'react'

var AdminPostIndex = React.createClass({
    render() {
        return (
            <div className="container-fluid">
                <p>Admin Post Index</p>
                { this.props.children }
            </div>
        )
    }
});

export default AdminPostIndex;