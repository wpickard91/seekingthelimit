console.log("EntityCache");

import Date from './Date'

class EntityCache
{
    static installCache() {
        window.WP_CACHE = new EntityCache();
        return window.WP_CACHE;
    }

    static retrieveCache() {
        return window.WP_CACHE;
    }

    constructor() {
        this.cache = {};
        this.entityLifeSpans = {};
    }

    get(key) {
        return this.cache[key] ? this.cache[key].value : null;
    }

    getEntity(key, id) {
        if (this.has(key)) {
            for (var i = 0; i < this.get(key).length; i++) {
                if (this.get(key)[i].id == id) return this.get(key)[i];
            }
        }

        return null;
    }
    getOrElse(key, orElse) {
        if (this.has(key)) return this.get(key);
        else return orElse;
    }

    has(key) {
        return this.get(key) != null;
    }


    hasEntity(key, id) {
        return this.getEntity(key, id) != null;
    }

    put(key, value) {
        this.cache[key] = {
            lastUpdate: Date.now,
            value: value
        }
    }

    putEntity(key, entity) {
        var list = this.getOrElse(key, []);
        list.push(entity);
        this.put(key, list)
    }

    setLifeSpan(key, seconds) {
        this.entityLifeSpans[key] = seconds;
    }

    shouldUpdate(key) {
        if (this.has(key)) {
            var elapsed = Date.now - this.get(key).lastUpdate;
            console.log("elapsed for ", key , " is ", elapsed);
            if (this.entityLifeSpans[key] && this.entityLifeSpans < elapsed) {
                return false;
            }
        }

        return true;
    }
}

export default EntityCache;