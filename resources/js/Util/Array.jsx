Array.prototype.intersection = function(a) {
    var c = [];

    for (var i = 0; i < this.length; i++) {
        for (var j = 0; j < a.length; j++) {
            if (this[i] == a[j]) {
                c.push(this[i]);
                break;
            }
        }
    }

    return c;
};

Array.prototype.take = function(i) {
    i = Math.min(this.length, i);
    var a = [];
    for (var j = 0; j < i; j++) {
        a.push(this[j])
    }
    return a;
};

Array.prototype.skip = function(i) {
    i = Math.max(0, i);
    var a = [];
    for (i; i < this.length; i++) {
        a.push(this[i])
    }
    return a;
};

Array.prototype.sum = function() {
    var s = 0;
    for (var i = 0; i < this.length; i++) {
        if (!isNaN(this[i])) s += this[i];
    }
    return s;
};

Array.prototype.mean = function() {
    return this.sum() / this.length;
};

Array.prototype.variance = function() {
    var m = this.mean();
    return this.map(x => Math.pow(x - m, 2)).mean();
};

Array.prototype.stdDeviation = function() {
    return Math.sqrt(this.variance());
};