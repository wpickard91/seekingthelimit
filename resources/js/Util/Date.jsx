Date.prototype.getFullMonth = function() {
    return [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    ][this.getMonth()];
};

Date.prototype.getShortMonth = function() {
    return this.getFullMonth().substring(0,3);
};

export default Date;