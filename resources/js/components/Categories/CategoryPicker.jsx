import React from 'react'
import CategoryController from '../../Controllers/CategoryController'

var CategoryPicker = React.createClass({
    getInitialState() {
        return {
            categories: [],
        }
    },
    getDefaultProps() {
        return {
            name: 'category_id',
            updateParent: function(){}
        }
    },
    componentWillMount() {
        var self = this;
        CategoryController.getAllCategories(self.setCategories);
    },
    setCategories(categories) {
        this.setState({
            categories: categories
        })
    },
    handleCategoryClick(e) {
        this.setState({
            category_id: e.target.value
        });
        this.props.updateParent(e.target.value);
    },
    render() {
        var self = this;
        return (
            <select
                onChange={self.handleCategoryClick}
                className='categoryPicker form-control'
                name={this.props.name}
                value={this.props.category_id}
            >
                <option onClick={self.handleCategoryClick}>Select a category</option>
                {
                    this.state.categories.map(function(category) {
                        return (
                            <option key={category.id}
                                    onClick={self.handleCategoryClick}
                                    value={category.id}>{category.name}
                            </option>
                        )
                    })
                }
            </select>
        )
    }
});

export default CategoryPicker;
