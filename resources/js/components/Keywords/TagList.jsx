import React from 'react'
import Tag from './Tag'
import Array from '../../Util/Array'

var TagList = React.createClass({
    getDefaultProps() {
        return {
            tags: [],
            onTagClick: function(){}
        }
    },
    calculateFontSize(tags = []) {
        var baseFontSize = 14;

        if (tags.length > 0 && tags[0].hasOwnProperty('keyword_references')) {
            var stdDeviation =  tags.map(x => x.keyword_references.length).stdDeviation(),
                mean =          tags.map(x => x.keyword_references.length).mean();

            tags.map(x => {
                var l = x.keyword_references.length,
                    d = (l + stdDeviation) / mean;

                x['fontSize'] = baseFontSize * d;
            })
        }

        return tags;
    },
    render() {
        var self = this,
            tags = this.calculateFontSize(this.props.tags);

        return (
            <div className='tagList material'>
                {
                    tags.map(function(tag, i){
                        var id = tag.hasOwnProperty('id') ? tag.id : '',
                            s = {
                                fontSize: tag.hasOwnProperty('fontSize') ? tag.fontSize : 'inherit'
                            };
                        return(
                            <Tag style={s} onClick={self.props.onTagClick} id={id} key={id} name={tag.name} />
                        )
                    })
                }

            </div>
        )
    }
});

export default TagList;