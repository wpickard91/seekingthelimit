import React from 'react'
import AutoComplete from '../Support/AutoComplete'
import Tag from './Tag'
import KeywordController from '../../Controllers/KeywordController'

var KeywordCreator = React.createClass({
    getInitialState() {
        return {
            currentInput: '',
            keywords: []
        }
    },
    getDefaultProps() {
        return {
            afterHarvest: function(keyword){},
            updateParent: function(keywords){}
        }
    },
    updateKeywords(event) {
        var value = event.target.value;

        if ((value.substring(value.length - 1, value.length)) == ' ') {
           this.harvestKeyword(value);
        } else {
            this.setState({
                currentInput: value
            })
        }
    },
    harvestKeyword(keyword) {
        var self = this;

        this.setState({
            keywords: this.state.keywords.concat([keyword]),
            currentInput: ''
        });

        KeywordController.createKeyword(keyword, function(keyword) {
            self.props.afterHarvest(keyword)
        });
        this.props.updateParent(this.state.keywords);
    },
    render() {
        return (
            <div className='keywordCreator'>
                <input type='text' onChange={this.updateKeywords} value={this.state.currentInput}/>
                <AutoComplete />
            </div>
        )
    }
});

export default KeywordCreator;