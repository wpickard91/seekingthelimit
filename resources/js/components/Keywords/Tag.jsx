import React from 'react'

var Tag = React.createClass({
    getDefaultProps() {
        return {
            onClick: function(){}
        }
    },
    handleClick() {
        this.props.onClick(this.props);
    },
    render() {
        var s = style;
        for (var i in this.props.style) s[i] = this.props.style[i];

        return (
            <div style={s} onClick={this.handleClick} key={this.props.id} className='tag'>
                <span className='name'>{this.props.name}</span>
            </div>
        )
    }
});

const style = {
    cursor: "pointer"
};

export default Tag;