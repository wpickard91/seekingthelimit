import React from 'react'
import TopNav from './menus/TopNav'
import {Link} from 'react-router'

var App = React.createClass({
    render: function() {
        console.log("App: ", this);
        return (
            <div>
                <TopNav />
                <section id='app' className='container-fluid'>
                    {this.props.children}
                </section>
                <footer className="material footer container-fluid" style={styles.footer}>
                    <div style={styles.footerItem} className="col-md-4 text-center">
                        <Link style={styles.footerLink} to="/home">Home</Link>
                    </div>
                    <div style={styles.footerItem} className="col-md-4 text-center">
                        <Link style={styles.footerLink} to="/posts">Posts</Link>
                    </div>
                    <div style={styles.footerItem} className="col-md-4 text-center">
                        <Link style={styles.footerLink} to="/contact">Contact</Link>
                    </div>
                </footer>
            </div>
        )
    }
});

const styles = {
    footer: {
        marginTop: "75px",
    },
    footerItem: {
        borderRight: "1px solid #e6e6e6",
        height: "75px",
        verticalAlign: "middle",
        lineHeight: "75px"
    },
    footerLink: {
        color: "#333",
        fontSize: "22px",
        fontWeight: 100
    }
};

export default App;