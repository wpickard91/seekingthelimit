import React from 'react'
import ReactDOM from 'react-dom'
import Loader from './Loader'
import PostMaster from './PostMaster'

var ContentContainer = React.createClass({
    getInitialState: function ()
    {
        return {
            data: {},
            loading: true
        }
    },

    loadContentFromServer: function()
    {
        console.log('loadContentFromServer, url: ' , this.props.url);
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            method: 'GET',
            cache: true,
            success: function(data) {
                this.setState({
                    data: data,
                    loading: false
                })
            }.bind(this),
            error: function(xhr, status, err) {
                console.log(this.props.url, ' had error, xhr: ' , xhr, ' status: ' , status, ' err: ' , err);
            }.bind(this)
        });
    },

    componentDidMount: function()
    {
        this.loadContentFromServer();
    },

    render: function()
    {
        console.log('ContentContainer props: ' , this.props);
        var self = this;
        return (
            <div className="contentContainer">
                <Loader loading={this.state.loading} />
                { self.props.renderContent(self.state.data) }
            </div>
        )
    }
});


export default ContentContainer;