import React from 'react'

var RadioButton = React.createClass({
    render() {
        return (
            <input type="radio"
                   className={this.props.className}
                   name={this.props.name}
                   placeholder={this.props.placeholder}
                   onClick={this.props.onClick}
                   onFocus={this.props.onFocus}
                   onChange={this.props.onChange}
            />
        )
    }
});

export default RadioButton;