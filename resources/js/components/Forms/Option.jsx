import React from 'react'

var Option = React.createClass({
    render() {
        return (
            <option
                value={this.props.value}
                style={this.props.style}
                className={this.props.className}
             >
                {this.props.children}
            </option>
        )
    }
});

export default Option;
