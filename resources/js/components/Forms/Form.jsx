import React from 'react'

var Form = React.createClass({
    render() {
        return (
            <form className={this.props.className}
                  action={this.props.action}
                  method={this.props.method}
                  encType={this.props.enctype}
                  onSubmit={this.props.onSubmit}
            >
                {this.props.children}
            </form>
        )
    }
});

export default Form;