import React from 'react'
import TextInput from './TextInput'
import Option from './Option'

var Select = React.createClass({
    getInitialState() {
        return {
            filterText: ''
        }
    },

    render() {
        return (
            <select style={this.props.style}
                    className={this.props.className}
                    onChange={this.props.onChange}
                    >
                {this.props.children}
            </select>
        )
    }
});

export default Select;
