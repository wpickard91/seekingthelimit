import React from 'react'
import Select from './Select'
import Option from './Option'
import TextInput from './TextInput'
import FormGroup from './FormGroup'

var FilterableSelect = React.createClass({
    getInitialState() {
        return {
            filterText: ''
        }
    },

    render() {
        return {
            filterText: ''
        }
    },

    handleFilterTextChange(e) {
        this.setState({
            filterText: e.target.value
        })
    },

    render() {
        var options = [],
            self = this;

        this.props.children.map(function(option) {
            if (option.props.children.toLowerCase().indexOf(self.state.filterText.toLowerCase()) >= 0) {
                options.push(option);
            }
        });

        return (
            <div className="filterableSelect">
                <FormGroup>
                    <TextInput placeHolder="Filter..."
                               className="form-control search-field"
                               onChange={this.handleFilterTextChange} />
                </FormGroup>
                <FormGroup>
                    <Select className="form-control" onChange={this.props.onChange}>
                        {options}
                    </Select>
                </FormGroup>
            </div>
        )
    }
});

export default FilterableSelect;