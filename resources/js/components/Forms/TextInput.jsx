import React from 'react'

var TextInput = React.createClass({
    render() {
        return (
            <input type="text"
                   className={this.props.className}
                   name={this.props.name}
                   placeholder={this.props.placeholder}
                   onClick={this.props.onClick}
                   onFocus={this.props.onFocus}
                   onChange={this.props.onChange}
            />
        )
    }
});

export default TextInput;