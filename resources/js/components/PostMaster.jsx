import React from 'react'
import PostList from './Posts/PostList'
import Post from '../components/Posts/Post'

var PostMaster = React.createClass({
    getInitialProps: function ()
    {
        return {
            posts: []
        }
    },

    render: function()
    {
        var count = this.props.posts.length;
        var nodes = <div></div>;
        console.log('PostMaster render');
        // if the count is > 1 then we want to show a post list
        if (count > 1) {
            nodes = <PostList posts={this.props.posts} />
        } else if (count == 1) {
            // if count is 1 then we want to show one post
            var post = this.props.posts[0];
            nodes = <Post
                post_id = {post.id}
                title = {post.title}
                description = {post.description}
                body = {post.body}
                images = {post.images}
                main_image = {post.mainImage}
                />
        }

        return (
            <div className="postMaster">
                {nodes}
            </div>
        )
    }
});

export default PostMaster
