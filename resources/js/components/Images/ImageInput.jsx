import React from 'react'

var ImageInput = React.createClass({
    render() {
        return (
            <input
                type="file"
                onChange={this.props.onChange}
                multiple
                className = "imageInput"
             />
        )
    }
});

export default ImageInput;