import React from 'react';
import ImagePanel from './ImagePanel';
import ImageStage from './ImageStage'

var Gallery = React.createClass({
    getDefaultProps() {
        return {
            images: [],
            thumbnails: []
        }
    },
    getInitialState() {
        return {
            stageOpen: false,
            primaryImageIndex: -1
        }
    },

    componentWillMount() {
        var self = this;
        // Add an event listener for when escape button is pressed
        // to close the imagestage if it is open
        window.addEventListener('keydown', function(e) {
             if (e.keyCode == 27) {
                 self.setState({
                     stageOpen: false,
                     primaryImageIndex: -1
                 })
             } else if(event.keyCode == 37) {
                self.handlePrevPress();
             } else if (event.keyCode == 39)  {
                self.handleNextPress();
             }
        });
    },
    componentWillUnmount() {
        window.removeEventListener('keydown');
    },
    changeToImage(imageIndex) {
        this.setState({
            primaryImageIndex: imageIndex
        })
    },
    handleImagePanelClick (imageIndex) {
        console.log("handleImagePanelClick: ", this.props.images[imageIndex], this.props.thumbnails[imageIndex]);
        this.setState({
            primaryImageIndex: imageIndex,
            stageOpen: true
        })
    },
    handleNextPress() {
        var next = (this.state.primaryImageIndex + 1 >= this.props.images.length )
            ? 0 : this.state.primaryImageIndex + 1;
        this.setState({
            primaryImageIndex:  next
        });
    },
    handlePrevPress() {
        var prev = (this.state.primaryImageIndex - 1 >= 0)
            ? this.state.primaryImageIndex - 1 : this.props.images.length - 1;
        this.setState({
            primaryImageIndex: prev
        });
    },
    resolveImageIndex(imageIndex) {
        // Create default image data in case we can't resolve
        var image = {
            alt: '',
            title: '',
            relative_path: '',
            caption: '',
            postId: 0,
            imageId: 0
        };

        if (0 <= imageIndex && imageIndex < this.props.images.length) {
            image = this.props.images[imageIndex];
        }

        return image;
    },

    render () {
        var primaryImage = this.resolveImageIndex(this.state.primaryImageIndex),
            panelImages = this.props.thumbnails;

        if (panelImages.length == 0) {
            panelImages = this.props.images;
        }

        return (
            <div className="gallery">
                <div className="row">
                    <ImagePanel
                        images={this.props.thumbnails}
                        handleImageClick={this.handleImagePanelClick}
                    />
                </div>
                <ImageStage
                    images={this.props.images}
                    open={this.state.stageOpen}
                    primaryImage={primaryImage}
                />
            </div>
        )
    }
});

export default Gallery;