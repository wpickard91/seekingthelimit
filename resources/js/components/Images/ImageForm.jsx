import React from 'react'
import FormGroup from '../Forms/FormGroup'

var ImageForm = React.createClass({
    onChange(e) {
        this.props.onChange(
            this.props.id,
            this.refs.titleInput.value,
            this.refs.altInput.value,
            this.refs.captionInput.value,
            this.refs.mainImageInput.checked
        )
    },
    submit(e) {
        this.props.handleSubmit(
            this.props.id,
            this.refs.titleInput.value,
            this.refs.altInput.value,
            this.refs.captionInput.value,
            this.refs.mainImageInput.checked
        );
    },
    delete() {
        this.props.handleDelete(this.props.id);
    },
    render() {
        return (
            <div className={"imageForm form-horizontal container material " + this.props.className}>
                <FormGroup>
                    <label className="control-label col-sm-3">Title</label>
                    <div className="col-sm-9">
                        <input type="text"
                               value={this.props.title}
                               ref="titleInput"
                               className="form-control"
                               onChange={this.onChange} />
                    </div>
                </FormGroup>

                <FormGroup>
                    <label className="control-label col-sm-3">Alt</label>
                    <div className="col-sm-9">
                        <input type="text"
                               ref="altInput"
                               className="form-control"
                               value={this.props.alt}
                               onChange={this.onChange} />
                    </div>
                </FormGroup>

                <FormGroup>
                    <label className="control-label col-sm-3">Caption</label>
                    <div className="col-sm-9">
                        <input type="text"
                               ref="captionInput"
                               value={this.props.caption}
                               className="form-control"
                               onChange={this.onChange}
                            />
                    </div>
                </FormGroup>

                <FormGroup>
                    <label className="control-label col-sm-3">Make Primary Image</label>
                    <div className="col-sm-9">
                        <input type="checkbox"
                               ref="mainImageInput"
                               checked={this.props.isMain ? 'checked' : ''}
                               onChange={this.onChange}
                        />
                    </div>
                </FormGroup>

                <FormGroup>
                    <a className="btn btn-block btn-default" onClick={this.submit}>
                        Submit
                    </a>
                </FormGroup>

                <FormGroup>
                    <a className="btn btn-block btn-danger" onClick={this.delete}>
                        Delete
                    </a>
                </FormGroup>
            </div>
        )
    }
});

export default ImageForm;