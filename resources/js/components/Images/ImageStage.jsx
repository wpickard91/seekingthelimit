import React from 'react'
import StageImage from './StageImage'

var ImageStage = React.createClass({
    getDefaultProps() {
        return {
            primaryImage: {
                alt: '',
                title: '',
                relative_path: '',
                caption: '',
                postId: 0,
                imageId: 0
            },
            className: "",
        }
    },
    render() {
        // Hide the stage if it is not open
        var className = "imageStage " + (this.props.open ? "" : "hidden");
        return (
            <div className={className}>
                <StageImage className={this.props.className} image={this.props.primaryImage} />
                { this.props.children }
            </div>
        );
    }
});

export default ImageStage;
