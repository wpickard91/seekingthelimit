import React from 'react'
import ImagePanel from './ImagePanel'
import ImageStage from './ImageStage'
import ImageForm from './ImageForm'
import ImageController from '../../Controllers/ImageController'

var EditGallery = React.createClass({
    getInitialState() {
        return {
            stageOpen: false,
            primaryImageIndex: -1,
            imageData: {},
        }
    },
    componentWillMount() {
        var self = this;
        // Add an event listener for when escape button is pressed
        // to close the imagestage if it is open
        window.addEventListener('keydown', function(e) {
            if (e.keyCode == 27) {
                self.setState({
                    stageOpen: false,
                    primaryImageIndex: -1
                })
            } else if(event.keyCode == 37) {
                self.handlePrevPress();
            } else if (event.keyCode == 39)  {
                self.handleNextPress();
            }
        });
    },
    componentWillUnmount() {
        window.removeEventListener('keydown');
    },
    changeToImage(imageIndex) {
        this.setState({
            primaryImageIndex: imageIndex
        })
    },
    handleImagePanelClick (imageIndex) {
        this.setState({
            primaryImageIndex: imageIndex,
            stageOpen: true
        })
    },
    handleNextPress() {
        var next = (this.state.primaryImageIndex + 1 >= this.props.images.length )
            ? 0 : this.state.primaryImageIndex + 1;
        this.setState({
            primaryImageIndex:  next
        });
    },
    handlePrevPress() {
        var prev = (this.state.primaryImageIndex - 1 >= 0)
            ? this.state.primaryImageIndex - 1 : this.props.images.length - 1;
        this.setState({
            primaryImageIndex: prev
        });
    },
    resolveImageIndex(imageIndex) {
        // Create default image data in case we can't resolve
        var image = {
            alt: '',
            title: '',
            relative_path: '',
            caption: '',
            postId: 0,
            imageId: 0
        };

        if (0 <= imageIndex && imageIndex < this.props.images.length) {
            image = this.props.images[imageIndex];
        }

        return image;
    },
    setMainImage(image_id, isMain) {
        if(isMain) {
            var data = this.state.imageData;
            for(var imageId in data) {
                data[imageId][3] = false;
            }
            data[imageId][3] = isMain;
            this.props.setMainImage(imageId);
            this.setState({imageData: data});
        }
    },
    handleFormChange(image_id, title, alt, caption, isMain) {
        var data = this.state.imageData;
        data[image_id] = [title, alt, caption, isMain];
        this.setState({imageData: data});
        this.setMainImage(image_id, isMain);
    },
    handleSubmit(image_id, title, alt, caption, isMain) {
        var data = {
            image_id: image_id,
            title: title,
            alt: alt,
            caption: caption,
            isMain: isMain
        };
        this.setMainImage(image_id, isMain);
        ImageController.updateImage(data, function(updatedImage) {

        });
    },
    handleDelete(image_id) {
        this.props.handleDelete(image_id);
    },
    render() {
        var primaryImage = this.resolveImageIndex(this.state.primaryImageIndex),
            id = primaryImage.id,
            title = this.state.imageData[id] ? this.state.imageData[id][0] : primaryImage.title,
            alt = this.state.imageData[id] ? this.state.imageData[id][1] : primaryImage.alt,
            caption = this.state.imageData[id] ? this.state.imageData[id][2]: primaryImage.caption,
            isMain = this.state.imageData[id] ? this.state.imageData[id][3] : this.props.mainImageId == primaryImage.id;
        return (
            <div className="editGallery">
                <ImagePanel
                    images={this.props.images}
                    handleImageClick={this.handleImagePanelClick}
                />
                <ImageStage className="col-md-9 col-sm-12"
                            primaryImage={primaryImage}
                            open={this.state.stageOpen}
                    >
                    <ImageForm
                        className="col-md-3 col-sm-12"
                        handleSubmit={this.handleSubmit}
                        handleDelete={this.handleDelete}
                        onChange={this.handleFormChange}
                        id={id}
                        key={id}
                        title={title}
                        alt={alt}
                        caption={caption}
                        isMain={isMain}
                    />
                </ImageStage>
            </div>
        )
    }
});

export default EditGallery;