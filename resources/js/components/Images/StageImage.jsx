import React from 'react'
import Image from './Image'

var StageImage = React.createClass({
    getInitialProps () {
        return {
            image: {
                id: 0,
                relative_path: '',
                title: '',
                caption: '',
                alt: '',
                post_id: 0
            },
            className: ""
        }
    },
    render() {

        return (
            <div className={"stageImageContainer " + this.props.className}>
                <Image
                    relative_path={this.props.image.relative_path}
                    title={this.props.image.title}
                    caption={this.props.image.caption}
                    id={this.props.image.id}
                    alt={this.props.image.alt}
                    post_id={this.props.image.post_id}
                    className="stageImage"
                />
                <p className="caption">{this.props.image.caption}</p>
            </div>
        )
    }
});

export default StageImage;