import React from 'react';

var Image = React.createClass({
    getDefaultProps () {
        return {
            className: 'img-responsive',
            alt: '',
            title: '',
            relative_path: '',
            caption: '',
            postId: 0,
            imageId: 0
        }
    },
    render () {
        return (
            <img
                className={this.props.className}
                alt={this.props.alt}
                title={this.props.title}
                src={this.props.relative_path}
                />
        )
    }
});

export default Image;