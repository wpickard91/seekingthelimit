import React from 'react';
import Image from './Image'
import ActionContainer from '../Support/ActionContainer'

var ImagePanel = React.createClass({
    getDefaultProps() {
        return {
            handleImageClick: function(e, i){}
        }
    },
    render () {
        var self = this;
        var images = this.props.images.map(function(image, i) {

            var imageComponent = <Image
                imageId = {image.id}
                relative_path = {image.relative_path}
                title = {image.title}
                alt = {image.alt}
                caption={image.caption}
                postId ={image.post_id}
                className='panelImage'
            />;

            return (
                <ActionContainer
                    key = {image.id}
                    children = {imageComponent}
                    onClick = {self.props.handleImageClick.bind(null, i)}
                />
            )
        });

        return (
            <div className="imagePanel">
                { images }
            </div>
        )
    }
});

export default ImagePanel;