import React from 'react'
import PostController from '../../Controllers/PostController'
import ImageController from '../../Controllers/ImageController'
import Gallery from '../Images/Gallery'
import Date from '../../Util/Date'
import EntityCache from '../../Util/EntityCache'
import {Link} from 'react-router'

var Post = React.createClass({
    getInitialState() {
        return {
            post: {}
        }
    },
    setPost(post) {
        post.created_at = new Date(post.created_at);
        post.updated_at = new Date(post.updated_at);
        this.setState({
            post: post
        });
    },
    getPost(postId) {
        var cache = EntityCache.retrieveCache(),
            self = this;
        if (cache.hasEntity("posts", postId)) {
            this.setPost(cache.getEntity("posts", postId))
        } else {
            PostController.find(postId, function(post) {
                cache.putEntity("posts", post);
                self.setPost(post);
            });
        }
    },
    formatDate(date) {
        if (!date) return "";
        return date.getFullMonth() + " " + date.getDate() + ", " + date.getFullYear()
    },
    componentWillMount() {
        if (this.props.params.id) {
            this.getPost(this.props.params.id)
        }
    },
    componentWillReceiveProps(nextProps) {
        if (this.props.params.id != nextProps.params.id) {
            this.getPost(nextProps.params.id);
        }
    },
    render() {
        var images = this.state.post.images || [],
            thumbnails = this.state.post.thumbnails || [];

        if (thumbnails.length <= 0) {
            // check to see if the images have a thumbnail property
            // if they do then we need to reference the thumbnail's image
            thumbnails = images.map(x => {
                if (x.thumbnail && x.thumbnail.image) return x.thumbnail.image;
                else return x;
            });
        }


        // Scroll to the top of the window because user might have clicked link at bottom
        window.scrollTo(0, 0);

        return (
            <div>
                <Link to="/posts" style={{marginBottom: "20px"}} className="btn btn-block btn-primary">Back</Link>

                <div className="post page material container-fluid">
                    <div className="row">
                        <h1 className="text-center title">{this.state.post.title}</h1>
                        <p className="text-center date">Posted on {this.formatDate(this.state.post.updated_at)}</p>
                    </div>
                    <div style={{marginBottom: "25px"}} className="row">
                        <div className="col-sm-12">
                            <p className="bs-callout description">{this.state.post.description}</p>
                        </div>
                    </div>
                    <div style={{paddingTop: "25px", paddingBottom: "25px", borderBottom: "1px solid #eee", borderTop: "1px solid #eee"}} className="row">
                        <div className="col-sm-12">
                            <Gallery images={images} thumbnails={thumbnails}/>
                        </div>
                    </div>
                    <div style={{marginTop: "25px"}} className="body">
                        {this.state.post.body}
                    </div>
                </div>
            </div>
        )
    }
});

export default Post;