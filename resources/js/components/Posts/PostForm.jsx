import React from 'react'
import CategoryPicker from '../Categories/CategoryPicker'
import FormGroup from '../Forms/FormGroup'
import PostController from '../../Controllers/PostController'
import ImageInput from '../Images/ImageInput'
import EditGallery from '../Images/EditGallery'
import TagList from '../Keywords/TagList'
import KeywordCreator from '../Keywords/KeywordCreator.jsx'

var PostForm = React.createClass({
    getInitialState() {
        return {
            id: 0,
            title: '',
            description: '',
            body: '',
            images: [],
            category_id: 0,
            keywords: [],
            fileList: []
        }
    },
    getInitialProps() {
        return {
            submit: function(){},
            sourceId: false,
        }
    },
    componentDidMount() {
        // if we have the source prop then we need to load the post from the server
        var self = this;
        if (this.props.sourceId) {
            PostController.find(this.props.sourceId, function(post) {
                self.setState({
                    id: post.id,
                    title: post.title,
                    description: post.description,
                    body: post.body,
                    images: post.images,
                    category_id: post.category_id,
                    keywords: post.keywords,
                    main_image_id: post.main_image_id
                });
            });
        }
    },
    handleMainImageChange(image_id) {
        this.setState({
            main_image_id: image_id
        });
    },
    handleTitleChange(e) {
        this.setState({
            title: e.target.value
        });
    },
    handleDescriptionChange(e) {
        this.setState({
            description: e.target.value
        });
    },
    handleBodyChange(e) {
        this.setState({
            body: e.target.value
        });
    },
    handleCategoryIdChange(category_id) {
        this.setState({
            category_id: category_id
        });
    },
    handleFileListChange(e) {
        this.setState({
            fileList: e.target.files
        });
    },
    handleImageDelete(image_id) {
        var self = this;
        PostController.removeImageFromPost(image_id, this.state.id, function(){
            // Because this image has been deleted, let's update the state without it
            var images = self.state.images,
                tempImages = [];

            for (var i = 0; i < images.length; i++) {
                console.log(images[i].id , ' vs ', image_id);
                if(images[i].id != image_id) {
                    tempImages.push(images[i]);
                    console.log('test: ', tempImages);
                }
            }

            console.log(tempImages);
            self.setState({
                images: tempImages
            })
        });
    },
    handleKeywordsChange(keyword) {
        // if it is already a keyword then do not include it
        for (var i = 0; i < this.state.keywords.length; i++) {
            if (this.state.keywords[i].name == keyword.name) {
                return
            }
        }

        var keywords = this.state.keywords.concat([keyword]);
        this.setState({
            keywords: keywords
        });
    },
    handleSubmit(e) {
        e.preventDefault();
        var data = {
            title: this.state.title,
            description: this.state.description,
            body: this.state.body,
            images: this.state.images,
            category_id: this.state.category_id,
            keywords: this.state.keywords,
            fileList: this.state.fileList,
            main_image_id: this.state.main_image_id
        };

        this.props.submit(data);
    },
    render() {
        return (
            <form className="form" onSubmit={this.handleSubmit}>

                <div className="form-group">
                    <label htmlFor="title" className="control-label">Title</label>
                    <input
                        name="title"
                        onChange={this.handleTitleChange}
                        type="text"
                        className="form-control"
                        value={this.state.title}
                        />
                </div>

                <div className="form-group">
                    <label className="control-label" htmlFor="description">Description</label>
                    <input
                        name="description"
                        type="text"
                        className="form-control"
                        value={this.state.description}
                        onChange={this.handleDescriptionChange}
                        />
                </div>

                <div className="form-group">
                    <label htmlFor="body" className="control-label">Body</label>
                    <textarea
                        rows="20"
                        className="form-control"
                        name="body"
                        value={this.state.body}
                        onChange={this.handleBodyChange}>
                    </textarea>
                </div>

                <FormGroup>
                    <CategoryPicker
                        category_id={this.state.category_id}
                        updateParent={this.handleCategoryIdChange}
                    />
                </FormGroup>

                <FormGroup>
                    <ImageInput onChange={this.handleFileListChange} />
                </FormGroup>

                <FormGroup>
                    <EditGallery
                        images={this.state.images}
                        mainImageId={this.state.main_image_id}
                        setMainImage={this.handleMainImageChange}
                        handleDelete ={this.handleImageDelete}
                    />
                </FormGroup>

                <FormGroup>
                    <TagList tags={this.state.keywords} />
                    <KeywordCreator afterHarvest={this.handleKeywordsChange} />
                </FormGroup>

                <FormGroup>
                    <input type="submit" className="btn btn-block" value="Submit" />
                </FormGroup>

                { this.props.children }
            </form>
        )
    }
});

export default PostForm;