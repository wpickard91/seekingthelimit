import React from 'react'
import PostController from '../../../Controllers/PostController'
import StoryController from '../../../Controllers/StoryController'
import Form from '../../Forms/Form'
import FormGroup from '../../Forms/FormGroup'
import TextInput from '../../Forms/TextInput'
import FilterableSelect from '../../Forms/FilterableSelect'
import Option from '../../Forms/Option'

var StoryForm = React.createClass({
    getInitialState() {
        return {
            title: '',
            chapters: [],
            posts: []
        }
    },

    resolvePostId(postId) {
        for (var i = 0; i < this.state.posts.length; i++) {
            if (this.state.posts[i].id == postId) return this.state.posts[i]
        }
        return null;
    },

    chapterExists(postId) {
        for(var i = 0; i < this.state.chapters.length; i++) {
            if (this.state.chapters[i].id == postId) return true;
        }
        return false;
    },

    handleTitleChange(e) {

    },

    handleSelectedPost(e) {
        var selectedPostId = e.target.value,
            post = this.resolvePostId(selectedPostId);
        this.appendPost(post);

    },

    appendPost(post) {
          this.setState({
              chapters: this.state.chapters.concat([post])
          });
    },

    handleSubmit() {

    },

    componentWillMount() {
        var self = this;
        PostController.getPostsIndex(1000, function(posts) {
             self.setState({
                 posts: posts
             })
        });
    },

    render() {
        var options = [],
            chapters = [],
            self = this;

        this.state.posts.map(function(post) {
            if (!self.chapterExists(post.id)) {
                options.push(<Option value={post.id}>{post.title}</Option>);
            }
        });

        for(var i = 0; i < this.state.chapters.length; i++) {
            chapters.push(
                <div className="row chapter post material">
                    <div className="col-sm-6 col-md-3 text-center">
                        <h2>{i}</h2>
                    </div>
                    <div className="col-sm-6 col-md-9">
                        <p className="title">{this.state.chapters[i].title}</p>
                        <p className="description">{this.state.chapters[i].description}</p>
                    </div>
                </div>
            )
        }

        return (
            <Form className="form story">
                <FormGroup>
                    <FormGroup>
                        <TextInput name="title"
                                   placeholder="Give the story a title"
                                   onChange={this.handleTitleChange}
                        />
                    </FormGroup>
                    <FormGroup>
                        <label className="control-label">Add a new post</label>
                        <FilterableSelect onChange={this.handleSelectedPost}>
                            {options}
                        </FilterableSelect>
                    </FormGroup>
                </FormGroup>

                <FormGroup>
                    { chapters }
                </FormGroup>
            </Form>
        )
    }
});

export default StoryForm;