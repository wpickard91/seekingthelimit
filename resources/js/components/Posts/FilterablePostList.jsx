import React from 'react'
import PostList from './PostList'
import Array from '../../Util/Array'

var FilterablePostList = React.createClass({
    getInitialProps() {
        return {
            keywords: [],
            posts: [],
            categories: [],
            filterText: "",
            postsPerRow: 4,
            limit: 10,
            sortField: "",
            sortDirection: "desc"
        }
    },
    sortEntities(entities, field, direction) {
        return entities.sort((a, b) => {
            if (a.hasOwnProperty(field) && b.hasOwnProperty(field)) {
                if (a[field] < b[field]) {
                    return direction == "desc" ? 1 : -1;
                } else if (a[field] > b[field]) {
                    return direction == "desc" ? -1 : 1;
                }
                else {
                    return 0;
                }
            }
        })
    },
    preformFilter() {
        var posts = [],
            keyword_ids = this.props.keywords.map(function(k){ return k.id }),
            category_ids = this.props.categories.map(function(c){ return c}),
            filterText = this.props.filterText.trim();

        // filter out posts based on criteria
        this.props.posts.forEach(function(post) {
            var isValid = true;

            // first filter based on keywords
            // show posts if they contain references to each keyword in the keyword array
            if(keyword_ids.length > 0 && post.hasOwnProperty('keyword_references')) {
                var kw_ref_ids = post.keyword_references.map(function(r) { return r.keyword_id });

                // we only want the post if it contains all keywords present
                if (keyword_ids.intersection(kw_ref_ids).length != keyword_ids.length) {
                    isValid = false;
                }
            }

            // now handle the category requirements
            // once again, the post only meets criteria if it contains all categories
            if (isValid && category_ids.length > 0 && post.hasOwnProperty('category_id')) {
                if (category_ids.intersection([post.category_id]).length != category_ids.length) {
                    isValid = false;
                }
            }

            // now handle the fitler text
            if (isValid && filterText.length > 0) {
                if (post.title.toLowerCase().indexOf(filterText.toLowerCase()) < 0
                    && post.description.toLowerCase().indexOf(filterText.toLowerCase()) < 0) {
                    isValid = false;
                }
            }

            if (isValid) {
                posts.push(post);
            }
        });

        return this.sortEntities(posts.take(this.props.limit), this.props.sortField, this.props.sortDirection);
    },
    render() {
        return (
            <div className='filterablePostList'>
                <PostList posts={this.preformFilter()}  postsPerRow={this.props.postsPerRow} />
            </div>
        )
    }
});

export default FilterablePostList;