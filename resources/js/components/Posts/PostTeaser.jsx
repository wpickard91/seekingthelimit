import React from 'react'
import { Link } from 'react-router'
import Date from '../../Util/Date';

var PostTeaser = React.createClass({
    getDefaultProps: function() {
        return {
            handleClick: function(post, post_id, e){}
        }
    },

    render() {
        var href = "/posts/" + this.props.post.id,
            category = this.props.post.category ? this.props.post.category.name : "",
            date = new Date(this.props.post.updated_at),
            imgSrc = this.props.post.main_image ? this.props.post.main_image.relative_path : '';

        return (
            <div className="col-md-6 col-sm-12" key={this.props.post.id}>
                <div className="post teaser material">
                    <div style={style.circleContainer} className="col-sm-4">
                        <div style={style.circle}>
                            <img style={style.img} src={imgSrc} />
                        </div>
                    </div>
                    <div className="col-sm-8">
                        <p className={"elegant category " + category}>{category}</p>
                        <p className="title">
                            <Link to={href}>{this.props.post.title}</Link>
                        </p>
                        <p style={style.date}>Posted on {date.getFullMonth() + ", " + date.getFullYear()}</p>
                    </div>
                </div>
            </div>
        )

    }
});

const style = {
    img: {
        height: "100px",
        width: "100px",
        borderRadius: "100%"
    },
    circleContainer: {
        height: "150px"
    },
    circle: {
        position: "absolute",
        height: "100px",
        width: "100px",
        overflow: "hidden",
        borderRadius: "100%",
        top: "25px",
        left: "40px"
    },
    date: {
        fontSize: "12px",
    },
    description: {
        fontSize: "12px",
        color: "#e6e6e6"
    },
    title: {
        color: "#2980b9"
    }
};

export default PostTeaser