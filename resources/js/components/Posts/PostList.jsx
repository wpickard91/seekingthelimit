import React from 'react'
import PostTeaser from './PostTeaser'

var PostList = React.createClass({
    getInitialProps() {
        return {
            postsPerRow: 3
        }
    },

    render: function()
    {
        var self = this,
            width = 12 / this.props.postsPerRow;

        var teaserNodes = this.props.posts.map(function(post, i) {
            return (
                <PostTeaser
                    key={i}
                    width={width}
                    post={post}
                    />
            )
        });

        var rows = [];
        for (var i = 0; i < teaserNodes.length; i++) {
            var row = [];
            for (var j = 0; j < this.props.postsPerRow; j++) {
                if (i < teaserNodes.length) {
                    row.push(teaserNodes[i]);

                    // only increment if the outer loop is not going to do it for us
                    if (j + 1 < this.props.postsPerRow) {
                        i++;
                    }
                }
            }
            rows.push(row);
            row = [];
        }

        return (
            <div className="postList container-fluid">
                {
                    rows.map(function(r,i) {
                        return (
                            <div key={i} className="row">{r}</div>
                        )
                    })
                }
            </div>
        )
    }
});

export default PostList