import React from 'react'

class Loader extends React.Component
{
    render()
    {
        if (this.props.loading) {
            return (
                <p>Loading</p>
            )
        }
        return (
            <p></p>
        )
    }
}

export default Loader;