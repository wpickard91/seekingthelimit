import React from 'react'

var PaginateNumber = React.createClass({
    click() {
        this.props.onClick(this.props.count);
    },
    render() {
        return (
            <span style={this.props.style}
                  key={this.props.count}
                  className={this.props.className}
                  onClick={this.click}>
                {this.props.count}
            </span>
        )
    }
});

export default PaginateNumber;