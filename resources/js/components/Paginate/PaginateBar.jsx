import React from 'react'
import PaginateNumber from './PaginateNumber'
import PaginateDirection from './PaginateDirection'

var PaginateBar = React.createClass({
    getDefaultProps() {

    },

    render() {
        var n = [];
        for (var i = this.props.start; i < this.props.end; i += this.props.increment) {
            n.push (
                <PaginateNumber selected={this.props.selected == i}
                                className={this.props.numberClass}
                                style={style.number}
                                key={i}
                                count={i}
                                onClick={this.props.onClick} />
            )
        }

        return (
            <div style={style.bar}>
                {n}
            </div>
        )
    }
});

const style = {
    bar: {
        textAlign: "center",
    },
    number: {
        textAlign: "center",
        padding: "10px",
        margin: "0 10px",
        cursor: "pointer"
    }
};

export default PaginateBar;