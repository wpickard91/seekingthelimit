import React from 'react'

var ActionContainer = React.createClass({
    getDefaultProps() {
        return {
            children: [],
            onClick: function(){console.log('ActionContainer default click')}
        }
    },
    render() {
        return (
            <div className="actionContainer" onClick={this.props.onClick}>
                {this.props.children}
            </div>
        )
    }
});

export default ActionContainer;