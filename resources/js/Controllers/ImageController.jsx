class ImageController
{
    static getPostImages(postId, callback)
    {
        $.ajax({
            url: '/posts/postsApi/v0/post/' + postId + "/images",
            method: "GET",
            dataType: "json",
            success: function(data) {
                if (data.status == 'success') {
                    callback(data.images);
                }
            },
            error: function(xhr, status, err) {
                console.log(xhr, status. err);
            }
        });
    }

    static getThumbnailsForPost(postId, callback)
    {
        $.ajax({
            url: '/posts/postsApi/v0/post/' + postId + "/thumbnails",
            method: "GET",
            "dataType": "json",
            success: function(data) {
                if (data.status == 'success') {
                    callback(data.images);
                }
            },
            error: function(xhr, status, err) {
                console.log(xhr, status, err);
            }
        })
    }

    static uploadFileList(fileList, callback)
    {
        var formData = new FormData();
        for (var i = 0; i < fileList.length; i++) {
            formData.append('images[]', fileList[i], fileList[i].name);
            console.log(fileList[i], ' : ', formData);
        }

        console.log('formData: ', formData);

        var ajax = $.ajax({
            url: '/images/imageApi/v0/create',
            dataType: 'json',
            method: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log(data);
                if (data.status == 'success') {
                    callback(data.images);
                } else {
                    console.log('ImageController::uploadFileList, success did not succeed? data:' , data);
                }
            },
            error: function(xhr, status, err) {
                console.log('ImageController::uploadFileList error! xhr: ', xhr, ' status: ', status, ', err: ', err);
            }
        });
    }

    static updateImage(data, callback)
    {
        $.ajax({
            url: '/images/imageApi/v0/image/' + data.image_id,
            method: 'PUT',
            data: {
                id: data.image_id,
                title: data.title,
                caption: data.caption,
                alt: data.alt
            },
            dataType: 'json',
            success: function(data) {
                if (data.status == 'success') {
                    callback(data.image);
                }
            },
            error: function(xhr, status, err) {
                console.log('ImageController::updateImage xhr: ', xhr, ', ', status, ', ', err);
            }
        });
    }

    static deleteImage(image_id)
    {
        $.ajax({
            url: '/images/imageApi/v0/image/' + image_id,
            method: 'DELETE'
        });
    }
}

export default ImageController;
