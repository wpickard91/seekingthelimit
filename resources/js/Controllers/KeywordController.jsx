import BaseApiController from './BaseController'

const fnc = function(){};

class KeywordController extends BaseApiController
{
    constructor() {
        super();
        this.baseUrl = '/keywords/keywordsApi/v0';
    }

    getAllReferences(callback=fnc){
        var url = this.baseUrl + '/keywords/references';
        this.get(url, {}, function(response) {
            callback(response.keywords);
        });
    }

    createKeyword(name, callback)
    {
        $.ajax({
            url: '/keywords/keywordsApi/v0/keyword/create',
            method: 'POST',
            dataType: 'json',
            data: {
                name: name
            },
            success: function(data) {
                if (data.status == 'success') {
                    callback(data.keyword);
                }
            },
            error: function(xhr, status, err){
                console.log('KeywordController::createKeyword error!', xhr, status, err);
            }
        });
    }

    editKeyword(keyword_id, name, callback)
    {
        $.ajax({
            url: '/keywords/keywordsApi/v0/keyword/' + keyword_id,
            method: 'PUT',
            dataType: 'json',
            data: {
                name: name
            },
            success: function(data) {
                if (data.status == 'success') {
                    callback(data.keyword);
                }
            },
            error: function(xhr, status, err) {
                console.log('KeywordController::editKeyword error!', xhr, status, err);
            }
        })
    }

    deleteKeyword(keyword_id, callback)
    {
        $.ajax({
            url: '/keywords/keywordsApi/v0/keyword/' + keyword_id,
            method: 'DELETE',
            success: function() {
                callback();
            },
            error: function(xhr, status, err) {
                console.log('KeywordController::deleteKeyword error!', xhr, status, err);
            }
        });
    }

    getKeywordByName(name, callback)
    {
        $.ajax({
            url: '/keywords/keywordsApi/v0/keyword?name=' + name,
            method: 'GET',
            dataType: 'json',
            success: function(data) {
                if (data.status == 'success') {
                    callback(data.keyword);
                }
            },
            error: function(xhr, status, err) {
                console.log('KeywordController::getKeywordByName error!', xhr, status, err);
            }
        })
    }

    getKeywordsForPost(post_id, callback)
    {
        $.ajax({
            url: '/keywords/keywordsApi/v0/keywords?post_id=' + post_id,
            method: 'GET',
            dataType: 'json',
            success: function(data) {
                if (data.status == 'success') {
                    callback(data.keywords);
                }
            },
            error: function(xhr, status, err) {
                console.log('KeywordController::getKeywordsForPost error!', xhr, status, err);
            }
        })
    }

    getKeywords(callback)
    {
        $.ajax({
            url: '/keywords/keywordsApi/v0/',
            method: 'GET',
            dataType: 'json',
            success: function(data) {
                if (data.status == 'success') {
                    callback(data.keywords);
                }
            },
            error: function(xhr, status, err) {
                console.log('KeywordController::getKeywords error!', xhr, status, err);
            }
        })
    }
}

export default KeywordController;