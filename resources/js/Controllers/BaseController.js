const defaultFunction = function(){};

class BaseApiController
{
    get(url, params = {}, callback = defaultFunction) {
        console.log("BaseApiController::get:url: " , url);
        $.ajax({
            url: url,
            method: "GET",
            dataType: params.hasOwnProperty("json") ? params['json'] : 'json',
            cache: params.hasOwnProperty("cache") ? params['cache'] : true,
            success: callback,
            error: this.handleError
        })
    }

    handleError(xhr, status, err) {
        console.log(xhr, status, err);
    }
}

export default BaseApiController;