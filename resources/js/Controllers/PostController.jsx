import BaseApiController from './BaseController'
import EntityCache from '../Util/EntityCache'

class PostController extends BaseApiController
{
    constructor() {
        super();
        this.baseUrl = '/posts/postsApi/v0';
    }

    paginate(take=10, start=0, end=10, callback=function(){}) {
        var url = "/posts/postsApi/v0?take=" + take + "&start=" + start;
        var params = {
            cache: true,
            dataType: "json"
        };

        this.get(url, params, function(data) { callback(data.posts) });
    }

    getTeasers(callback) {
        var cache = EntityCache.retrieveCache();

        if (cache.has('teasers')) {
            callback(cache.get('teasers'));
        }

        this.get(this.baseUrl + '/postTeasers', {}, function(teasers) {
            cache.put('teasers', teasers);
            callback(teasers);
        });
    }

    static getPostsIndex(take=10, callback = function(){}) {
        var url = "/posts/postsApi/v0?take=" + take;

        $.ajax({
            url: url,
            cache: true,
            method: 'GET',
            dataType: 'json',
            success: function(data) {
                if (data.status == 'success') {
                    callback(data.posts);
                }
            },
            error: function(xhr, status, err) {
                console.log('PostController::getPostsIndex, there was an error! ', xhr, ', ', status, ', err');
            }
        });
    }

    static find(postId, callback = function(){}) {
        $.ajax({
            url: '/posts/postsApi/v0/post/' + postId,
            method: 'GET',
            cache: true,
            dataType: 'json',
            success: function(data) {
                if (data.status == 'success') {
                    callback(data.post);
                }
            },
            error (xhr, status, err) {
                console.log('PostController::find, there was an error! ', xhr, ', ', status, ', ', err);
            }
        });
    }

    static create(data, callback = function(){}) {
        $.ajax({
            url: '/posts/postsApi/v0/create',
            method: 'POST',
            dataType: 'json',
            data: data,
            success: function(data) {
                if(data.status == 'success') {
                    console.log('PostController::postCreate success, data: ' , data);
                    callback(data.post);
                }
            },
            error: function(xhr, status, err) {
                console.log('PostController::postCreate error occured, ', xhr, ', ', status, ', ', err);
            }
        });
    }

    static update(data, callback = function(){}) {
        $.ajax({
            url: '/posts/postsApi/v0/post/' + data.post_id,
            method: 'PUT',
            data: data,
            dataType: 'json',
            success: function(data) {
                if (data.status == 'success') {
                    callback(data.post)
                }
            },
            error: function(xhr, status, err) {
                console.log('PostController::update error!', xhr, status, err);
            }
        });
    }

    static assignImagesToPost(images, post, callback = function(){})
    {
        var imageIds = images.map(function(image) {
            return image.id;
        }),
            postId = post.id;

        $.ajax({
            url: '/posts/postsApi/v0/post/' + postId + '/images',
            method: 'PUT',
            dataType: 'json',
            data: {
                images: imageIds,
                postId: postId
            },
            success: function(data) {
                if (data.status == 'success') {
                    callback(data.post);
                }
            },
            error: function(xhr, status, err) {
                console.log('PostController::assignImagesToPost, error!', xhr, status, err);
            }
        });
    }

    static removeImageFromPost(image_id, post_id, callback = function(){})
    {
        $.ajax({
            url: '/posts/postsApi/v0/post/' + post_id + '/images/' + image_id,
            method: 'DELETE',
            success: function() {
                callback()
            },
            error: function(xhr, status, err) {
                console.log('PostController::removeImageFromPost, ', xhr, status, err);
            }
        })
    }

    static putKeywords(post_id, keyword_ids, callback = function(){})
    {
        $.ajax({
            url: '/posts/postsApi/v0/post/' + post_id + '/keywords',
            method: 'PUT',
            dataType: 'json',
            data: {
                'keyword_ids' : keyword_ids
            },
            success: function(data) {
                if (data.status == 'success') {
                    callback(data.post);
                }
            },
            error: function(xhr, status, err) {
                console.log('PostController::putKeywords error!' , xhr, status, err);
            }
        });
    }

    static assignCategoryToPost(categoryId, post, callback = function(){})
    {
        $.ajax({
            url: '/posts/postsApi/v0/post/' + post.id + '/category',
            method: 'PUT',
            dataType: 'json',
            data: {
                'category_id' : categoryId
            },
            success: function(data) {
                if (data.status == 'success') {
                    callback(data.post);
                }
            },
            error: function(xhr, status, err) {
                console.log('PostController::AssignCategoryToPost error!' , xhr, status, err);
            }
        });
    }
}

export default PostController;
