class CategoryController
{
    static getAllCategories(callback)
    {
        $.ajax({
            url : '/categories/categoriesApi/v0',
            dataType: 'json',
            method: 'GET',
            success: function(data) {
                if (data.status == 'success') {
                    callback(data.categories);
                }
            },
            error : function(xhr, status, err) {
                console.log('CategoryController::getAllCategories, error, ' , xhr, ', ', status, ', ', err)
            }
        });
    }
}

export default CategoryController;