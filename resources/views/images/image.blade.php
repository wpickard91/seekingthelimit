@if($use_thumbnail && $image->thumbnail)
    <p>Thumbnail</p>
    <img
            src="{{$image->thumbnail->image->relative_path}}"
            class="{{isset($class) ? $class : ''}}"
            alt="{{$image->alt}}"
            title="{{$image->title}}"
    />
@else
    <p>regular</p>
    <img
            src="{{$image->relative_path}}"
            class="{{isset($class) ? $class : ''}}"
            alt="{{$image->alt}}"
            title="{{$image->title}}"
    />
@endif