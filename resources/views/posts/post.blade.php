@extends('layouts.master')

@section('content')

    <div class="page post container" id="post-{{$post->id}}">
        @if(Auth::user() && Auth::user()->is_admin)
            <div class="bs-callout bs-callout-info">
                <p>Hey! you're an admin!</p>
                <p><a href="/admin/posts/edit/{{$post->id}}">Click here to edit</a></p>
            </div>
            <hr />
        @endif
        <div class="row">
            <div class="col-sm-12 image-container">
                @if($post->main_image_id)
                    <img src="{{$post->mainImage->relative_path}}" />
                @endif
            </div>
            <div class="col-sm-12">
                <h1 class="title">{{$post->title}}</h1>
                <p>{{ $post->created_at->toFormattedDateString() }}</p>
                <p>By: <a href="/users/{{$post->user->id}}">{{ $post->user->username }}</a></p>
                <h4 class="description">{{$post->description}}</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="body">
                    {!! $post->body !!}
                </div>
            </div>
        </div>
    </div>

@endsection