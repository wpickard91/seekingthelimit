
@if($include_trumbo)
    @include('util.trumbo')
    @yield('trumbo')
@endif

<div class="form-group">
    <label for="{{$title_name}}" class="control-label">Title</label>
    <input type="text" name="{{$title_name}}" value="{{$title_value}}" class="form-control" />
</div>

<div class="form-group">
    <label for="{{$description_name}}" class="control-label">Description</label>
    <input name="{{$description_name}}" value="{{$description_value}}" type="text" class="form-control" />
</div>

<div class="form-group">
    <label for="{{$body_name}}" class="control-label">Body</label>
    <div class="body" name="{{$body_name}}" value="{{$body_value}}" class="form-control"></div>
</div>