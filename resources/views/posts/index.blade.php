@extends('layouts.master')

@section('content')
   <section id="postsIndex" class="page container">
      <ul>
         @foreach ($posts as $post)
               <li>
                  @if($post->main_image_id)
                     <img style="max-height: 50px" src="{{$post->mainImage->relative_path}}" />
                  @endif
                  <a href="/posts/{{$post->id}}">{{$post->title}}</a>
               </li>
         @endforeach
      </ul>
   </section>

@endsection