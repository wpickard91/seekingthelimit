@extends('layouts.master')

@include('util.trumbo')
@yield('trumbo')

@section('content')
<section id="createArticle" class="container page">
    <h1>Create Post</h1>
    <form class="form" action="/admin/posts/create" method="POST">
        <div class="form-group">
            <label for="title" class="control-label">Title</label>
            <input type="text" name="title" class="form-control" />
        </div>

        <div class="form-group">
            <label for="description" class="control-label">Description</label>
            <input name="description" type="text" class="form-control" />
        </div>

        <div class="form-group">
            <label for="body" class="control-label">Body</label>
            <textarea name="body" class="body form-control"></textarea>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-default btn-block" value="Submit" />
            {{ csrf_field() }}
        </div>
    </form>
</section>

@endsection