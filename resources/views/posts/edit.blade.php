@extends('layouts.master')

@include('util.trumbo')
@yield('trumbo')

@section('content')
    <section id="createArticle" class="container page">
        <h1>Edit <span>{{$post->title}}</span></h1>
        <p>Created by {{$post->user->username}}</p>
        <form class="form" action="/admin/posts/edit/{{$post->id}}" method="POST" enctype="multipart/form-data">
            @if($post->mainImage)
                @if($post->mainImage->thumbnail)
                    <img src="{{$post->mainImage->thumbnail->image->relative_path}}" />
                    <p>Thumbnail id: {{$post->mainImage->thumbnail->id}}</p>
                @else
                    <img src="{{$post->mainImage->relative_path}}" style="max-width:800px"/>
                @endif
            @endif

            <p>main_image_id: {{$post->main_image_id ? $post->main_image_id : 'null'}}</p>
            <div class="form-group">
                <label for="published" class="control-label">Publish</label>
                @if ($post->published)
                    <input type="checkbox" name="published" checked/>
                @else
                    <input type="checkbox" name="published" />
                @endif
            </div>

            <div class="form-group">
                <label for="main_image" class="control-label">Main image</label>
                <input type="file" name="main_image" />
            </div>
            <div class="form-group">
                <label for="title" class="control-label">Title</label>
                <input type="text" name="title" class="form-control" value="{{$post->title}}"/>
            </div>

            <div class="form-group">
                <label for="description" class="control-label">Description</label>
                <input name="description" type="text" class="form-control" value="{{$post->description}}"/>
            </div>

            <div class="form-group">
                <label for="body" class="control-label">Body</label>
                <div name="body" rows="20" class="body form-control">{!! $post->body !!}</div>
            </div>

            <div class="form-group">
                <label for="categories" class="control-label">Select Categories</label>
                @foreach($categories as $category)
                    <div>
                        <label class="control-label">{{$category->name}}</label>
                        @if($post->isInCategory($category))
                            <input type="checkbox" name="category-{{$category->id}}" checked />
                        @else
                            <input type="checkbox" name="category-{{$category->id}}" />
                        @endif
                    </div>
                @endforeach
            </div>

            <div class="form-group">
                <a target="_blank" class="btn btn-secondary btn-block" href="/posts/{{$post->id}}">Preview (will open new tab)</a>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-primary btn-block" value="Submit" />
                {{ csrf_field() }}
            </div>
        </form>

        <form id="deletePost" action="/admin/posts/delete/{{$post->id}}" method="POST">
            <div class="form-group">
                <input type="submit" value="Delete" class="btn btn-danger" />
            </div>
        </form>
    </section>
@endsection
