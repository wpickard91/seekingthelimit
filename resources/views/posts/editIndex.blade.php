@extends('layouts.master')
@section('content')

    <section>
        <h1>Edit Posts</h1>
        <div>
            @foreach($posts as $post)
                <p><a href="/admin/posts/edit/{{$post->id}}">{{ $post->title }}</a></p>
            @endforeach
        </div>
    </section>

@endsection