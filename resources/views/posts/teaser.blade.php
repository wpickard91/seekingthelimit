<div class="post teaser">
    <div class="image-container">
        @if($post->main_image_id)
            {!! view('images.image', ['image' => $post->mainImage, 'use_thumbnail' => $use_thumbnail]) !!}
        @endif
    </div>
    <div class="info">
        <p class="title"><strong><a href="/posts/{{$post->id}}">{{$post->title}}</a></strong></p>
        <p>
            <small>
                <a href="/users/{{$post->user_id}}">{{$post->user->username}}</a> | {{$post->created_at->toFormattedDateString()}}
            </small>
        </p>
    </div>
</div>
