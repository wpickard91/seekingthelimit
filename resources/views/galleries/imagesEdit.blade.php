<table>
    <thead>
    <tr>
        <th>Image</th>
        <th>Path</th>
        <th>Make main image</th>
        <th>Caption</th>
        <th>Alt</th>
        <th>Title</th>
        <th>Delete</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($images as $image)
        <tr>
            <td>
                {!! view('images.image', ['image' => $image, 'use_thumbnail' => true]) !!}
            </td>
            <td>
                <span>{{$image->absolute_path}}</span>
            </td>
            <td>
                @if($main_image_id== $image->id)
                    <input type="radio" name="main_image_id" value="{{$image->id}}" checked />
                @else
                    <input type="radio" name="main_image_id" value="{{$image->id}}" />
                @endif
            </td>
            <td>
                <input type="text" name="caption={{$image->id}}" class="form-control" value="{{$image->caption}}"/>
            </td>
            <td>
                <input type="text" name="alt={{$image->id}}" class="form-control" value="{{$image->alt}}"/>
            </td>
            <td>
                <input type="text" name="title={{$image->id}}" class="form-control" value="{{$image->title}}"/>
            </td>
            <td>
                <input type="checkbox" name="delete_image={{$image->id}}" />
            </td>
        </tr>
    @endforeach
    </tbody>
</table>