@extends('layouts.master')

@section('content')
   <section id="galleriesIndex" class="page container">
      <h1>Galleries</h1>{{--
      <ul>
         @foreach ($galleries as $gallery)
               <li>
                  <a href="/galleries/{{$gallery->id}}">{{$gallery->title}}</a>
               </li>
         @endforeach
      </ul>--}}
       {!! view('util.entityCalendar', ['entities' => $grouped_galleries, 'render' => function($gallery) {
        return "<a href='/galleries/{$gallery->id}'>{$gallery->title}</a>";
       }]) !!}
   </section>

@endsection