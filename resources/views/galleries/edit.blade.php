@section('scripts')
    <script>
        function updatePage(gallery) {
            //todo
            window.location.reload();
        }

        $('#form').on('submit', function(e) {
            e.stopPropagation();
            e.preventDefault();
            var data = new FormData($('#form')[0]);

            $('body').append($('<div style="position:fixed;background:rgba(255,255,255,0.5);top:0;left:0;height:100%;width:100%"></div>'));

            $.ajax({
                url: '/admin/galleries/api/edit/{{$gallery->id}}',
                method: 'POST',
                cache: false,
                dataType: 'json',
                data: data,
                processData: false,
                contentType: false,
                success: function(response) {
                    updatePage(response.gallery);
                },
                error: function(jqhxr, textstatus, error) {
                    console.log('error: ', jqhxr, textstatus, error);
                }
            })
        });
    </script>
@endsection

@extends('layouts.master')
@section('content')
    <section id="createArticle" class="container page">
        <h1>Edit gallery <span class="gallery title">{{$gallery->title}}</span></h1>
        <a href="/galleries/{{$gallery->id}}">View</a>
        <form id="form" class="form" action="/admin/galleries/edit/{{$gallery->id}}" method="POST" enctype="multipart/form-data">
            <div class="row">
                <div class="col-sm-12 image-container">
                    @if($gallery->mainImage)
                        {!! view('images.image', ['image' => $gallery->mainImage, 'use_thumbnail' => true]) !!}
                    @endif
                    <p>main_image_id: {{$gallery->main_image_id ? $gallery->main_image_id : 'null'}}</p>
                </div>
            </div>

            <div class="form-group">
                <label for="published" class="control-label">Published</label>
                @if($gallery->published)
                    <input type="checkbox" name="published" checked />
                @else
                    <input type="checkbox" name="published" />
                @endif
            </div>

            <div class="form-group">
                <label for="title" class="control-label">Title</label>
                <input type="text" name="title" class="form-control" value="{{$gallery->title}}"/>
            </div>

            <div class="form-group">
                <label for="description" class="control-label">Description</label>
                <input name="description" type="text" class="form-control" value="{{$gallery->description}}"/>
            </div>

            <div class="form-group">
                <label for="body" class="control-label">Upload some more images</label>
                <input id="images" type="file" name="images[]" multiple />
                {!! view('galleries.imagesEdit', ['main_image_id' => $gallery->main_image_id, 'images' => $gallery->images]) !!}
            </div>
            <div class="form-group">
                <a href="/galleries/{{$gallery->id}}" target="_blank" class="btn btn-block btn-primary">Preview (will open in new tab)</a>
                <input type="submit" class="btn btn-default btn-block" value="Submit" />
                {{ csrf_field() }}
            </div>
        </form>


        <form id="deleteGallery" action="/admin/galleries/delete/{{$gallery->id}}" method="POST">
            <div class="form-group">
                <input type="submit" value="Delete" class="btn btn-danger" />
            </div>
        </form>
    </section>
@endsection