@extends('layouts.master')

@section('content')
   <section id="galleriesIndex" class="page container">
      <h1>Admin Galleries</h1>
       <p><a href="/admin/galleries/create">Create a gallery</a></p>
      <ul>
         @foreach ($galleries as $gallery)
               <li>
                  <a href="/admin/galleries/edit/{{$gallery->id}}">{{$gallery->title}}</a>
               </li>
         @endforeach
      </ul>
   </section>

@endsection