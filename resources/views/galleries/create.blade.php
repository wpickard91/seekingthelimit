@extends('layouts.master')
@section('content')
<section id="galleryCreate" class="container page">
    <h1>Galleries Create</h1>
    <form class="form" action="/admin/galleries/create" method="POST" enctype="multipart/form-data">
        <div class="form-group">
            <label for="title" class="control-label">Title</label>
            <input type="text" name="title" class="form-control" />
        </div>

        <div class="form-group">
            <label for="description" class="control-label">Description</label>
            <input name="description" type="text" class="form-control" />
        </div>

        <div class="form-group">
            <label for="images[]" class="control-label">Pics</label>
            <input type="file" name="images" multiple />
        </div>
        <div class="form-group">
            {{ csrf_field() }}
            <input type="submit" class="btn btn-default btn-block" value="Submit" />
        </div>
    </form>
</section>
@endsection