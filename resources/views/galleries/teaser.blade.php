<div class="gallery teaser">
    <div class="image-container">
        @if($gallery->main_image_id)
            {!! view('images.image', ['image' => $gallery->mainImage, 'use_thumbnail' => $use_thumbnail]) !!}
        @else
            <p>no image</p>
        @endif
    </div>
    <div class="info">
        <p class="title"><strong>   <a href="/galleries/{{$gallery->id}}">{{$gallery->title}}</a></strong></p>
        <p><small><a href="/users/{{$gallery->user->id}}">{{$gallery->user->username}}</a> | {{$gallery->created_at->toFormattedDateString()}}</small></p>
    </div>
</div>
