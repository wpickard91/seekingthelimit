@section('scripts')
    <script>
        @if(!$isMobile)
            function positionContent($content) {
                $content.css({'margin-top': window.innerHeight * (2/3)});
                $content.find('img').css({'max-height': window.innerHeight});
            }

            positionContent($('.banner + .content'));
            $(window).resize(function() { positionContent($('.banner + .content'))});
        @endif
    </script>
@endsection

@extends('layouts.master')

@section('content')

    <section class="container-fluid page gallery">
        <div class="row">
            <div class="banner image-container">
                @if($gallery->main_image_id)
                  {{--<img src="{{$gallery->mainImage->relative_path}}" />--}}
                    {!! view('images.image', ['image' => $gallery->mainImage, 'use_thumbnail' => $isMobile]) !!}
                @endif
            </div>

            <div class="container content">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="title">{{$gallery->title}}</h1>
                        <p>
                            <span class="author">By: <a href="/users/{{$gallery->user->id}}">{{$gallery->user->username}}</a></span>,
                            <span class="date">{{$gallery->created_at->toFormattedDateString()}}</span>
                        </p>

                        <h4 class="description">{{$gallery->description}}</h4>
                    </div>
                </div>

                @foreach($gallery->images as $image)
                    <div class="row">
                        <div class="col-sm-12 image-container">
                            <a href="{{$image->relative_path}}">
                                {!!  view('images.image', ['image' => $image, 'use_thumbnail' => $isMobile, 'class' => 'img-responsive']) !!}
                            </a>
                            @if(strlen($image->caption) > 0)
                                <p class="bs-callout bs-callout-caption caption">{{$image->caption}}</p>
                            @endif
                        </div>
                    </div>
                @endforeach

            </div>
        </div>


    </section>

@endsection