@extends('layouts.master')
@section('content')
    <section class="container page">
        <form method="POST" action="/users/edit/{{$user->id}}" enctype="multipart/form-data">
            <p>Hi {{$user->username}}</p>
            <div class="form-group">
                @if($user->image)
                    <img src="{{$user->image->relative_path}}" />
                    <p>id: {{$user->image->id}}</p>
                @endif
                <input type="file" name="image" />
            </div>

            <div class="form-group">
                <label for="first_name" class="control-label">First name</label>
                <input type="text" name="first_name" class="form-control" value="{{$user->first_name}}"/>
            </div>

            <div class="form-group">
                <label for="last_name" class="control-label">Last name</label>
                <input type="text" name="last_name" class="form-control" value="{{$user->last_name}}"/>
            </div>

            <div class="form-group">
                <label for="bio" class="control-label">Bio</label>
                <input type="text" name="bio" class="form-control" value="{{$user->bio}}"/>
            </div>

            <div class="form-group">
                <label for="current_location" class="control-label">Current Location</label>
                <input type="text" name="current_location" class="form-control" value="{{$user->current_location}}"/>
            </div>

            <div class="form-group">
                <a class="btn btn-block  btn-secondary"  href="/users/{{$user->id}}">See it how others see it</a>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-primary btn-block" value="Submit" />
                {{csrf_field()}}
            </div>
        </form>
    </section>
@endsection