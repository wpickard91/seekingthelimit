@extends('layouts.master')
@section('content')
<section id="createUser" class="container page">

    @if(session('created_user'))
        <p>Created {{ session('created_user')->username }}<p>
    @endif

    <form class="form" action="/users/create" method="POST">
        <div class="form-group">
            <label for="username" class="control-label">Username</label>
            <input type="text" name="username" class="form-control" />
        </div>

        <div class="form-group">
            <label for="password" class="control-label">Password</label>
            <input type="password" name="password" class="form-control" />
        </div>

        @if($active_user && $active_user->is_admin)
            <div class="form-group">
                <label for="is_admin" class="control-label">Make user admin</label>
                <input type="checkbox" name="is_admin" class="checkbox" />
            </div>
        @endif
        <div class="form-group">
            <input type="submit" class="btn btn-primary btn-block" value="Submit" />
            {{ csrf_field() }}
        </div>
    </form>
</section>
@endsection