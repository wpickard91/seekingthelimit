@extends('layouts.master')
@section('content')
<section class="container page">
    @if(Auth::user() && Auth::user()->id == $user->id)
        <div class="bs-callout bs-callout-info">
            <p>Hey! this is you!</p>
            <p><a href="/users/edit/{{$user->id}}">Click here to edit this page</a></p>
        </div>
        <hr />
    @endif

    @if($user->image)
        <img src="{{$user->image->relative_path}}" />
    @endif
    <p>{{$user->username}}</p>
    <p>{{$user->first_name}} {{$user->last_name}}</p>
    <p>{{$user->current_location}}</p>
    <p class="bs-callout bs-callout-caption">{{$user->bio}}</p>
</section>
@endsection