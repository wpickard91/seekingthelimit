@extends('layouts.master')
@section('content')
    <section class="container page">
        <h1>Users</h1>
        <ul>
            @foreach ($users as $user)
                <li>
                    @if($user->image)
                        <img src="{{$user->image->relative_path}}" />
                    @endif
                    <a href="/users/edit/{{$user->id}}">{{$user->username}}</a>
                </li>
            @endforeach
        </ul>
    </section>
@endsection
