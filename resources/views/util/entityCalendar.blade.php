@foreach($entities as $month => $entities_in_month)
    <h4>{{$month}}</h4>
    <ul>
        @foreach($entities_in_month as $entity)
            <li>{!! $render($entity) !!}</li>
        @endforeach
    </ul>
@endforeach