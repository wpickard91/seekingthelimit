@section('scripts')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.1.0.min.js"><\/script>')</script>
    <script src="/js/trumbowyg.min.js"></script>
    <script src="/js/trumbowyg.upload.js"></script>
    <script>
        $.trumbowyg.svgPath = '/js/icons.svg';
        $(".body").trumbowyg({
            btns: [
                ['viewHTML'],
                ['undo', 'redo'],
                ['formatting'],
                'btnGrp-design',
                ['link'],
                ['image'],
                'btnGrp-justify',
                'btnGrp-lists',
                ['foreColor', 'backColor'],
                ['preformatted'],
                ['horizontalRule'],
                ['fullscreen']
            ],
            btnsDef: {
                image: {
                    dropdown: ['insertImage', 'upload'],
                    ico: 'insertImage'
                }
            },
            plugins: {
                upload: {
                    serverPath: '/api/images/upload',
                    fileFieldName: 'image',
                    headers: {
                        'Authorization': 'Client-ID 9e57cb1c4791cea'
                    },
                    urlPropertyName: 'data.link',
                    success: function(response, $trumbo, $modal, uploadedFile) {
                        console.log('success!', response);
                        console.log('trumbo: ', $trumbo);
                        console.log('modal: ', $modal);
                        console.log('uploadedFile: ', uploadedFile);
                        $trumbo.execCmd('insertImage', response.image.relative_path);
                        setTimeout(function() {
                            $trumbo.closeModal();
                        }, 250);
                        $trumbo.$c.trigger('tbwuploadsuccess', [$trumbo, response, response.image.relative_path]);
                    }
                }
            }
        });
    </script>
@endsection

@section('styles')
    <link type="text/css" rel="stylesheet" href="/css/trumbowyg.min.css" />
@endsection
