<label class="control-label">Specs</label>
<div class="specs-input"></div>
<script>
    function createSpec(label, body) {
        // what the user sees
        var display = document.createElement('tr');
        display.className = 'spec';
        var labelDisplay = document.createElement('td');
        var bodyDisplay = document.createElement('td');
        labelDisplay.appendChild(document.createTextNode(label));
        bodyDisplay.appendChild(document.createTextNode(body));
        display.appendChild(labelDisplay);
        display.appendChild(bodyDisplay);

        // what the backend will get
        var hiddenDetails = document.getElementsByClassName('hidden-spec');
        var hiddenElementID = hiddenDetails.length;
        var hiddenElement = document.createElement('INPUT');
        hiddenElement.type = 'hidden';
        hiddenElement.className = 'hidden-spec';
        hiddenElement.name = 'gear_spec-' + hiddenElementID;
        hiddenElement.value = 'label=' + label + '&body=' + body;

        // when clicked, kill this group
        var killButton = document.createElement('td');
        killButton.appendChild(document.createTextNode('X'));
        killButton.addEventListener('click', function() {
            display.parentNode.removeChild(display);
        });
        display.appendChild(killButton);

        display.appendChild(hiddenElement);

        return display;
    }

    function collectInputAndRenderSpec(addTo, labelInputElement, bodyInputElement) {
        var detail = createSpec(labelInputElement.value, bodyInputElement.value);

        addTo.appendChild(detail);

        labelInputElement.value = '';
        bodyInputElement.value = '';
    }

    function createSpecAddButton() {
        var button = document.createElement('A');
        var text = document.createTextNode('Add');
        button.addEventListener('click', function() {
            var labelInputElements = document.getElementsByClassName('spec-label-input');
            var bodyInputElements = document.getElementsByClassName('spec-body-input');
            var displayArea = document.getElementsByClassName('specs-display-area');

            if (labelInputElements.length > 0 && bodyInputElements.length > 0 && displayArea.length > 0) {
                var tbody = displayArea[0].getElementsByTagName("TBODY");
                collectInputAndRenderSpec(tbody[0], labelInputElements[0], bodyInputElements[0]);
            }
        });

        button.appendChild(text);
        return button;
    }

    function createSpecInputFormGroup() {
        var labelInput = document.createElement('INPUT');
        var bodyInput = document.createElement('INPUT');
        var group = document.createElement('DIV');
        var labelLabel = document.createElement('LABEL');
        var bodyLabel = document.createElement("LABEL");

        group.className = 'form-group';
        labelInput.className = ['form-control', 'spec-label-input'].join(' ');
        bodyInput.className = ['form-control', 'spec-body-input'].join(' ');

        labelLabel.className = 'control-label';
        labelLabel.appendChild(document.createTextNode('Label'));
        bodyLabel.className = 'control-label';
        bodyLabel.appendChild(document.createTextNode('Body'));

        group.appendChild(labelLabel);
        group.appendChild(labelInput);
        group.appendChild(bodyLabel);
        group.appendChild(bodyInput);

        return group;
    }

    function createSpecDisplayArea() {
        var table = document.createElement('TABLE');
        var thead =  document.createElement("THEAD");
        var tbody = document.createElement("TBODY");
        var theadRow = document.createElement("TR");
        var label = document.createElement("TH");
        label.appendChild(document.createTextNode("Label"));
        var body = document.createElement("TH");
        body.appendChild(document.createTextNode("Body"));
        var kill = document.createElement("TH")
        kill.appendChild(document.createTextNode("Remove"));

        table.className = 'specs-display-area';
        theadRow.appendChild(label);
        theadRow.appendChild(body);
        theadRow.appendChild(kill);
        thead.appendChild(theadRow);
        table.appendChild(thead);
        table.appendChild(tbody);

        return table;
    }

    function installSpecsInput(parentDiv) {
        console.log('install for : ', parentDiv);
        var addButton = createSpecAddButton();
        var inputGroup = createSpecInputFormGroup();
        var area = createSpecDisplayArea();

        parentDiv.appendChild(addButton);
        parentDiv.appendChild(inputGroup);
        parentDiv.appendChild(area);
    }

    (function(){
        var specsInputs = document.getElementsByClassName('specs-input');
        for (var i = 0; i < specsInputs.length; i++) {
            installSpecsInput(specsInputs[i]);
        }
    })();
</script>