@extends('layouts.master')
@section('content')
    <div class="page container">
        <h1>Create Gear Review</h1>
        <form class="form" action="/admin/reviews/create" method="POST" enctype="multipart/form-data">
        <h3>Gear Post</h3>

        <div class="form-group">
            <label for="image" class="control-label">Images</label>
            <input type="file" name="images" multiple/>
        </div>

        <p class="description">Summarize the gear</p>
        {!! view('posts.inputs')->with([
            'title_name' => 'gear_post_title',
            'title_value' => '',
            'description_name' => 'gear_post_description',
            'description_value' => '',
            'body_name' => 'gear_post_body',
            'body_value' => '',
            'include_trumbo' => true
        ]) !!}

        <hr />

        <h3>Review</h3>
        <p class="description">Review the gear</p>

        <div class="form-group">
            <label for="rating" class="control-label">Give it a rating (0-100)</label>
            <input name="rating" class="form-control" type="number" min="0" max="100" />
        </div>

        {!! view('posts.inputs')->with([
            'title_name' => 'gear_review_title',
            'title_value' => '',
            'description_name' => 'gear_review_description',
            'description_value' => '',
            'body_name' => 'gear_review_body',
            'body_value' => '',
            'include_trumbo' => false
        ]) !!}

        <hr />

        @include('gearreviews.detailsinput')
        <hr />

        @include('gearreviews.specsinput')
        <hr />

    </form>

    </div>
@endsection