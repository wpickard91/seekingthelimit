<label class="control-label">Add some details</label>
<div class="details-input"></div>
<script>
    function createDetail(text) {
        console.log('create detail: ', text);
        var wrapper = document.createElement('DIV');
        wrapper.className = 'detail';

        // what the user sees
        var display = document.createElement('LI');
        display.appendChild(document.createTextNode(text));

        // what the backend will get
        var hiddenDetails = document.getElementsByClassName('hidden-detail');
        var hiddenElementID = hiddenDetails.length;
        var hiddenElement = document.createElement('INPUT');
        hiddenElement.type = 'hidden';
        hiddenElement.className = 'hidden-detail';
        hiddenElement.name = 'gear_detail-' + hiddenElementID;
        hiddenElement.value = text;

        // when clicked, kill this group
        var killButton = document.createElement('SPAN');
        killButton.style['padding-left'] = '15px';
        killButton.appendChild(document.createTextNode('X'));
        killButton.addEventListener('click', function() {
            wrapper.parentNode.removeChild(wrapper);
        });
        display.appendChild(killButton);

        wrapper.appendChild(display);
        wrapper.appendChild(hiddenElement);

        return wrapper;
    }

    function collectInputAndRender(addTo, inputElement) {
        var detail = createDetail(inputElement.value);

        addTo.appendChild(detail);

        inputElement.value = '';
    }

    function createAddButton() {
        var button = document.createElement('A');
        var text = document.createTextNode('Add');
        button.addEventListener('click', function() {
            var inputElements = document.getElementsByClassName('detail-input');
            var displayArea = document.getElementsByClassName('details-display-area');

            if (inputElements.length > 0 && displayArea.length > 0) {
                collectInputAndRender(displayArea[0], inputElements[0]);
            }
        });

        button.appendChild(text);
        return button;
    }

    function createInputFormGroup() {
        var input = document.createElement('INPUT');
        var group = document.createElement('DIV');
        var label = document.createElement('LABEL');

        group.className = 'form-group';
        input.className = ['form-control', 'detail-input'].join(' ');
        label.className = 'control-label';
        label.appendChild(document.createTextNode('Body'));

        group.appendChild(label);
        group.appendChild(input);

        return group;
    }

    function createDisplayArea() {
        var area = document.createElement('UL');
        area.className = 'details-display-area';
        return area;
    }

    function installDetailsInput(parentDiv) {
        console.log('install for : ', parentDiv);
        var addButton = createAddButton();
        var inputGroup = createInputFormGroup();
        var area = createDisplayArea();

        parentDiv.appendChild(addButton);
        parentDiv.appendChild(inputGroup);
        parentDiv.appendChild(area);
    }

    (function(){
        var detailsInputs = document.getElementsByClassName('details-input');
        console.log(detailsInputs);
        for (var i = 0; i < detailsInputs.length; i++) {
            installDetailsInput(detailsInputs[i]);
        }
    })();
</script>