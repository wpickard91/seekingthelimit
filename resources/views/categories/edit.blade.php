@extends('layouts.master')
@section('content')
    <section class="page container">
        <h1>Edit Category {{$category->name}}</h1>
        <form method="POST" enctype="multipart/form-data" action="/admin/categories/edit/{{$category->id}}">
            <div class="form-group">
                @if($category->image)
                    <img src="{{$category->image->relative_path}}" style="max-width: 300px" />
                @endif
                <label for="image" class="control-label">Image</label>
                <input name="image" type="file" />
            </div>
            <div class="form-group">
                <label for="name" class="control-label">Name</label>
                <input name="name" class="form-control" type="text" value="{{$category->name}}"/>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit"/>
                {{ csrf_field() }}
            </div>
        </form>


        <form id="deleteCategory" action="/admin/categories/delete/{{$category->id}}" method="POST">
            <div class="form-group">
                <input type="submit" value="Delete" class="btn btn-danger" />
            </div>
        </form>
    </section>
@endsection