@extends('layouts.master')
@section('content')
    <section class="page container">
        <h1>Create Category</h1>
        <form method="POST" enctype="multipart/form-data" action="/admin/categories/create">
            <div class="form-group">
                <label for="name" class="control-label">Name</label>
                <input name="name" class="form-control" type="text" />
            </div>
            <div class="form-group">
                <label for="image" class="control-label">Image</label>
                <input name="image" type="file" />
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit"/>
                {{ csrf_field() }}
            </div>
        </form>
    </section>
@endsection