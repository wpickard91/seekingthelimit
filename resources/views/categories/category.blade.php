@extends('layouts.master')
@section('content')
    <section class="container page">
        <div class="row">
            <div class="col-md-12">
                <div class="category teaser banner">
                    <div class="image-container">
                        <p class="title">{{$category->name}}</p>
                        <img src="{{$category->image->relative_path}}" />
                        <span class="film" />
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="row">
            @foreach($category->posts as $post)
                <a href="/posts/{{$post->id}}">
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        {!! view('posts.teaser')->with('post', $post) !!}
                    </div>
                </a>
            @endforeach
        </div>
    </section>
@endsection