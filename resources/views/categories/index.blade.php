@extends('layouts.master')
@section('content')
    <section class="container page">
        <ul>
            @foreach($categories as $category)
                <div class="row">
                    <div class="col-sm-12">
                        <a href="/categories/{{$category->name}}">
                            {!! view('categories.teaser')->with('category', $category) !!}
                        </a>
                    </div>
                </div>
            @endforeach
        </ul>
    </section>
@endsection