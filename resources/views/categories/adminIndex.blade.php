@extends('layouts.master')
@section('content')
    <section class="page content container">
        <h1>Admin Categories</h1>
        <p><a href="/admin/categories/create">Create Category</a></p>
        <p><strong>Categories:</strong></p>
        @if (count($categories) == 0)
            <p>No categories</p>
        @endif
        <ul>
            @foreach($categories as $category)
                <li>
                    @if($category->image)
                        <img src="{{$category->image->relative_path}}" style="max-width:50px" />
                    @endif
                    <a href="/admin/categories/edit/{{$category->id}}">{{ $category->name }}</a>
                </li>
            @endforeach
        </ul>
    </section>
@endsection