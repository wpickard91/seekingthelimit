{{--
<div class="teaser category" style="background: url('{{$category->image->relative_path}}')">

    <a href="/categories/{{$category->name}}">{{$category->name}}</a>
</div>--}}

<div class="teaser category">
    <p class="title"><strong>{{$category->name}}</strong></p>

    <div class="image-container">
        {!! view('images.image', ['image' => $category->image, 'use_thumbnail' => $use_thumbnail]) !!}
        <span class="film"></span>
    </div>
</div>