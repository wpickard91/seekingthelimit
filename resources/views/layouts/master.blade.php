<!DOCTYPE html>
<html>
<head>
    <title>{{ @isset($pageTitle) ? $pageTitle : 'Seeking the Limit' }}</title>
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />

    @include ('partials.styles')
    @yield ('styles')
</head>
<body>
    @include('partials.navs.top')

    <section id="contentContainer" class="content">
        @yield('content')
    </section>
    <footer>
        {{Session::get("isMobile", false) ? "mobile" : "desktop"}}
        <p>&copy All Rights Reserved | SeekingTheLimit.com</p>
    </footer>
</body>

@include('partials.scripts')


@yield('scripts')
</html>