@extends('layouts.master')
@section('styles')
    <style>
        .group {
            padding: 15px
        }
    </style>
@endsection
@section('content')
    <section class="container page">
        <h1>admin home</h1>

        <div class="group">
            <p><a href="/admin/posts/create">Create Post</a></p>
            <p><a href="/admin/posts/">Edit Posts</a></p>
        </div>

        <div class="group">
            <p><a href="/admin/galleries/create">Create Gallery</a></p>
            <p><a href="/admin/galleries/">Edit Galleries</a></p>
        </div>

        <div class="group">
            <p><a href="/admin/categories/create">Create Category</a></p>
            <p><a href="/admin/categories/">Edit Categories</a></p>
        </div>

        <div class="group">
            <p><a href="/users/create">Create User</a></p>
            <p><a href="/admin/users/">Edit Users</a></p>
        </div>
    </section>
@endsection