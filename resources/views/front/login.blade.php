@extends('layouts.master')
@section('content')

    <section class="container page">
        @if(session('message'))
            <p>{{session('message')}}</p>
        @endif
        <h1>Login</h1>
        <form id="login" action="/login" method="POST">
            <input name="username" type="text" class="form-control"/>
            <input name="password" type="password" class="form-control" />
            <input type="submit" value="submit" class="form-control"/>
            {{ csrf_field() }}
        </form>
    </section>

@endsection