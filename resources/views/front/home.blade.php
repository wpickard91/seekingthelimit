@extends('layouts.master')

@section('content')

    <section class="container-fluid page">
        @if(session('message'))
            <p>{{session('message')}}</p>
        @endif
            <div class="row">
                <div class="col-sm-12 col-md-3">
                    <h3 class="col-sm-12">Recent <a class="post" href="/posts">Posts</a></h3>
                    @foreach($recent_posts as $post)
                        <div class="col-sm-12">
                            <a href="/posts/{{$post->id}}">
                              {!! view('posts.teaser')->with(['post' => $post, 'use_thumbnail' => $isMobile]) !!}
                            </a>x
                        </div>
                    @endforeach
                </div>

                <div class="col-sm-12 col-md-6">
                    @foreach($categories as $category)
                        <a href="/categories/{{$category->name}}">
                            {!! view('categories.teaser')->with(['category' => $category, 'use_thumbnail' => $isMobile]) !!}
                        </a>
                    @endforeach
                </div>

                <div class="col-sm-12 col-md-3">
                    <h3 class="col-sm-12">Recent <a class="gallery" href="/galleries">Galleries</a></h3>
                    @foreach($recent_galleries as $gallery)
                        <div class="col-sm-12">
                            <a href="/galleries/{{$gallery->id}}">
                                {!! view('galleries.teaser')->with(['gallery' => $gallery, 'use_thumbnail' => $isMobile]) !!}
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>


            <div style="width:50%">

            </div>

    </section>
@endsection