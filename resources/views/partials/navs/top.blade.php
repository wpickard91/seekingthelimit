<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menustuff" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/home">Seeking the Limit</a>
        </div>
        <div class="collapse navbar-collapse" id="menustuff">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="/categories">Categories</a>
                </li>
                @if (Auth::user())
                    <li>
                        <a href="/logout">Logout</a>
                    </li>
                    <li>
                        <a href="/users/edit/{{Auth::user()->id}}">Profile</a>
                    </li>
                    @if(Auth::user()->is_admin)
                        <li>
                            <a href="/admin">Admin Home</a>
                        </li>
                    @endif
                @else
                    <li>
                        <a href="/users/create">Sign up</a>
                    </li>
                    <li>
                        <a href="/login">Login</a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>