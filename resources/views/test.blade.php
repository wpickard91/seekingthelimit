<!-- index.html -->
<!DOCTYPE html>
<html>
<head>
    <style>
        circle {
            stroke: #655;
            fill: yellowgreen;
        }

        svg {
            transform: rotate(-90deg);
            background: yellowgreen;
            border-radius: 50%;
        }
    </style>
</head>
<body>
    <figure>
        <figcaption>
            Wocaption
        </figcaption>
        <svg width="100" height="100" class="chart">
            <circle id="pie" r="19" cx="50" cy="50"></circle>
        </svg>
        <button id="toggle">toggle</button>
        <p>percent: </p>
        <input id="percent" type="text" />
        <button id="newPercent">New Percent</button>
    </figure>
    <script>
        var circle = document.getElementById('pie');
        console.log(circle);
        function setCircleStrokeDashArray(circle, l, r) {
            circle.style.strokeDasharray = l + " " + r;
            return circle;
        }

        var r = 50;
        var total = Math.PI * 2 * r;
        var last = 0;
        var stop = true;

        circle.setAttribute('r', r);
        circle.style.strokeWidth = r * 2;

        console.log("r: ", r);
        console.log("total: ", total);

        var auto = () => setInterval(function() {
            if (!stop) {
                var percent = getPercent();
                var currentPercent = parseFloat(circle.style.strokeDasharray);
                newPercent(circle, percent + currentPercent, total);
            }
        }, 50);

        var isAuto = false;
        document.getElementById("toggle").addEventListener("click", function() {
            if (!isAuto) {
                isAuto = true;
                auto();
            }
            console.log("toggle");
            stop = !stop;
        });

        function newPercent(circle, percent, total) {
            var p = Math.min((percent / 100) * total, 100);
            setCircleStrokeDashArray(circle, p, total);
        }

        function getPercent() {
            return parseFloat(document.getElementById('percent').value)
        }

        document.getElementById('newPercent').addEventListener('click', function() {
            newPercent(circle, getPercent(), total);
        });
    </script>
</body>
</html>