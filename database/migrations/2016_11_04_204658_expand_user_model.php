<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExpandUserModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('image_id')->unsigned()->nullable();
            $table->string('bio');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('current_location');
            $table->foreign('image_id')->references('id')->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('bio');
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('current_location');
            $table->dropForeign('users_image_id_foreign');
            $table->dropColumn('image_id');
        });
    }
}
