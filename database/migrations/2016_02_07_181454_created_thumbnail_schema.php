<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatedThumbnailSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("thumbnails", function(Blueprint $table) {
            $table->increments("id");
            $table->integer("image_id")->unsigned();
            $table->integer("original_image_id")->unsigned();
            $table->timestamps();
            $table->foreign("image_id")->references("id")->on("images");
            $table->foreign("original_image_id")->references("id")->on("images");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("thumbnails");
    }
}
