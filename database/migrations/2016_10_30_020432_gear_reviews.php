<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GearReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gear_reviews', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('gear_post_id')->unsigned();
            $table->integer('review_post_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('rating')->default(0);
            $table->timestamps();
            $table->foreign('gear_post_id')->references('id')->on('posts');
            $table->foreign('review_post_id')->references('id')->on('posts');
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('gear_review_images', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('gear_review_id')->unsigned();
            $table->integer('image_id')->unsigned();
            $table->timestamps();

            $table->foreign('gear_review_id')->references('id')->on('galleries');
            $table->foreign('image_id')->references('id')->on('images');
        });

        Schema::create('gear_details', function(Blueprint $table) {
            $table->increments('id');
            $table->text('body');
            $table->integer('gear_review_id')->unsigned();
            $table->foreign('gear_review_id')->references('id')->on('gear_reviews');
            $table->timestamps();
        });

        Schema::create('gear_specs', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('gear_review_id')->unsigned();
            $table->string('label');
            $table->string('body');
            $table->foreign('gear_review_id')->references('id')->on('gear_reviews');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gear_specs');
        Schema::drop('gear_details');
        /*Schema::table('gear_review_images', function(Blueprint $table) {
            $table->dropForeign('image_id');
        });*/
        Schema::drop('gear_review_images');
        Schema::drop('gear_reviews');
    }
}
