<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserAndPublishedToGalleries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('galleries', function(Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->boolean('published')->default(false);
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('galleries', function(Blueprint $table) {
            $table->dropForeign('galleries_user_id_foreign');
            $table->dropColumn('user_id');
            $table->dropColumn('published');
        });
    }
}
