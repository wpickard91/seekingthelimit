<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Posts;

use Illuminate\Database\Eloquent\Collection;
use WP\Posts\Interfaces\PostRepositoryInterface;
use WP\Posts\Entities\Post;

class PostRepository implements PostRepositoryInterface
{
    public function find($id)
    {
        return Post::find($id);
    }

    public function all()
    {
        /*return Post::with('images')
            ->get();*/

        return Post::all();
    }


    /**
     * @param int $take
     * @param int $start
     * @param int $end
     * @return Collection
     */
    public function paginate($take=10, $start=0, $end=10)
    {
        if ($take <= 0) {
            return Collection::make([]);
        }

        return Post::query()
            ->skip($start)
            ->limit($take)
            ->get();
    }

    public function recent($count) {
        return Post::select('*')
            ->where(['published' => true])
            ->orderBy('created_at', 'desc')
            ->limit($count)
            ->get();
    }
}