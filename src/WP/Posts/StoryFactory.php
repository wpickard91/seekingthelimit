<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Posts;

use Illuminate\Database\Eloquent\Collection;
use WP\Posts\Entities\Chapter;
use WP\Posts\Entities\Story;
use WP\Posts\Interfaces\PostRepositoryInterface;
use WP\Posts\Interfaces\StoryFactoryInterface;
use WP\Posts\Interfaces\StoryRepositoryInterface;

class StoryFactory implements StoryFactoryInterface
{
    /**
     * @var PostRepositoryInterface
     */
    protected $postRepository;

    /**
     * @var StoryRepositoryInterface
     */
    protected $storyRepository;

    public function __construct(
        PostRepositoryInterface $postRepo,
        StoryRepositoryInterface $storyRepo
    ) {
        $this->postRepository = $postRepo;
        $this->storyRepository = $storyRepo;
    }

    /**
     * @param $story_id
     * @param $post_id
     * @param $prev_id
     * @param $next_id
     * @return Chapter
     */
    public function createChapter($story_id, $post_id, $prev_id, $next_id)
    {
        return Chapter::create([
            'story_id' => $story_id,
            'post_id' => $post_id,
            'prev_id' => $prev_id,
            'next_id' => $next_id
        ]);
    }

    /**
     * @param array $post_ids
     * @return Story
     */
    public function create($post_ids = [])
    {
        $story = Story::create();

        for($i = 0; $i < count($post_ids); $i++) {
            $chapter = Chapter::create([
                'story_id' => $story->id,
                'post_id' => $post_ids[$i]
            ]);

            if ($i > 0) {
                $chapter->prev_id = $post_ids[$i - 1]->id;
                $post_ids[$i - 1]->next_id = $chapter->id;

                $chapter->save();
                $post_ids[$i - 1]->save();
            }
        }

        return $story;
    }

    public function delete($story_id)
    {
        $this->storyRepository->findStory($story_id)->delete();
    }

    public function assignNextChapter($this_chapter_id, $next_chapter_id)
    {
        $this->storyRepository->findChapter($this_chapter_id)->next_id = $next_chapter_id;
    }

    public function assignPrevChapter($this_chapter_id, $prev_chapter_id)
    {
        $this->storyRepository->findChapter($this_chapter_id)->prev_id = $prev_chapter_id;
    }
}