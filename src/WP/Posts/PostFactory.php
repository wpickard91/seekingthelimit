<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Posts;

use Illuminate\Database\Eloquent\Collection;
use WP\Images\Entities\PostImage;
use WP\Images\Interfaces\ImageFactoryInterface;
use WP\Images\Interfaces\ImageRepositoryInterface;
use WP\Keywords\Entities\Keyword;
use WP\Keywords\Interfaces\KeywordFactoryInterface;
use WP\Posts\Interfaces\PostFactoryInterface;
use WP\Posts\Entities\Post;
use WP\Images\Entities\Image;
use WP\Categories\Entities\Category;

class PostFactory implements  PostFactoryInterface
{
    /*
     * @var ImageFactoryInterface
     */
    protected $imageFactory;

    /**
     * @var ImageRepositoryInterface
     */
    protected $imageRepository;

    /**
     * @var KeywordFactoryInterface
     */
    protected $keywordFactory;

    public function __construct(
        ImageFactoryInterface $imageFactory/*
        ImageRepositoryInterface $imageRepository,
        KeywordFactoryInterface $keywordFactory*/
    ) {
        $this->imageFactory = $imageFactory;/*
        $this->imageRepository = $imageRepository;
        $this->keywordFactory = $keywordFactory;*/
    }

    public function createAssetDirectory(Post $post) {
        $images_dir = public_path() . '/images';
        if (!file_exists($images_dir)) {
            if(mkdir($images_dir)) {
                $this->createAssetDirectory($post);
            } else {
                return false;
            }
        }

        $posts_dir =  $images_dir . '/posts';
        if (!file_exists($posts_dir)) {
            if(mkdir($posts_dir)) {
                $this->createAssetDirectory($post);
            } else {
                return false;
            }
        }

        $this_post_dir = $posts_dir . '/' . $post->id;
        if (!file_exists($this_post_dir)) {
            if(mkdir($this_post_dir)) {
                return true;
            } else {
                return false;
            }
        }

        return true;
    }

    public function create($title, $description, $body, $user_id, $main_image_id = null) {
        $post = Post::create([
            'title' => $title,
            'description' => $description,
            'body' => $body,
            'user_id' => $user_id,
            'main_image_id' => $main_image_id
        ]);

        $this->createAssetDirectory($post);

        return $post;
    }

    public function setMainImage(Post $post, Image $image) {
        $post->main_image_id = $image->id;
        $post->save();

        if ($this->createAssetDirectory($post)) {
            $absolute_asset_dir = public_path() . '/images/posts/' . $post->id;
            $this->imageFactory->move($image, $absolute_asset_dir);
        }

        return $post;
    }

    public function edit(Post $post, $title, $description, $body, $published = false) {
        $post->title = $title;
        $post->description = $description;
        $post->body = $body;
        $post->published = $published;
        $post->save();
        return $post;
    }

    public function removeImage(Post $post, Image $image) {
        /**
         * @var PostImage
         */
        $postImage = $this->imageRepository->findPostImage($post->id, $image->id);
        $postImage->delete();
    }

}