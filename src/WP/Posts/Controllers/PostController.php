<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Posts\Controllers;

use Illuminate\Auth\Guard;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use WP\Categories\Entities\PostCategory;
use WP\Categories\Interfaces\CategoryFactoryInterface;
use WP\Categories\Interfaces\CategoryRepositoryInterface;
use WP\Common\LaravelUtil;
use WP\Http\Controllers\BaseController;
use WP\Images\Entities\Image;
use WP\Images\Interfaces\ImageFactoryInterface;
use WP\Images\Interfaces\ThumbnailFactoryInterface;
use WP\Posts\Interfaces\PostFactoryInterface;
use WP\Posts\Entities\Post;
use WP\Posts\Interfaces\PostRepositoryInterface;
use Illuminate\Http\Request;
use WP\Categories\Entities\Category;

class PostController extends BaseController
{
    /**
     * @var  PostRepositoryInterface
     */
    protected $postRepository;

    /**
     * @var PostFactoryInterface
     */
    protected $postFactory;

    /**
     * @var ImageFactoryInterface
     */
    protected $imageFactory;

    /**
     * @var ThumbnailFactoryInterface
     */
    protected $thumbnailFactory;

    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * @var CategoryFactoryInterface
     */
    protected $categoryFactory;

    /**
     * @var Guard
     */
    protected $guard;

    public function __construct (
        PostRepositoryInterface $postRepository,
        PostFactoryInterface $postFactory,
        ImageFactoryInterface $imageFactory,
        ThumbnailFactoryInterface $thumbnailFactoryInterface,
        CategoryRepositoryInterface $categoryRepo,
        CategoryFactoryInterface $categoryFactory,
        Guard $guard
    ) {
        $this->postRepository = $postRepository;
        $this->postFactory = $postFactory;
        $this->imageFactory = $imageFactory;
        $this->thumbnailFactory = $thumbnailFactoryInterface;
        $this->categoryRepository = $categoryRepo;
        $this->categoryFactory = $categoryFactory;
        $this->guard = $guard;
    }

    /**
     * return the page showing the posts
     */
    public function getIndex()
    {
        $posts = $this->postRepository->all();

        $posts->load('mainImage');

        return view('posts.index', compact('posts'));
    }

    public function getPost($postId){
        $post = $this->postRepository->find($postId);

        //$post->load('mainImage', 'images');

        return view('posts.post', compact('post'));
    }

    /**
     * Show the form for creating a new post
     */
    public function getCreate()
    {
        return view('posts.create');
    }

    public function getEditIndex(Request $request) {
        $posts = $this->postRepository->all();
        return view('posts.editIndex', compact('posts'));
    }

    public function getEdit(Request $request, $post_id)
    {
        $categories = $this->categoryRepository->all();
        $post = $this->postRepository->find($post_id);
        return view('posts.edit')->with(compact('categories', 'post'));
    }

    /**
     * @param Post $post The post to make the route for
     * @return string
     */
    private function makeEditRoute(Post $post) {
        return '/admin/posts/edit/' . $post->id;
    }

    /**
     * @param Request $request
     * @return Image|null
     */
    public function makeMainImage(Request $request) {
        $main_image = null;

        if ($request->hasFile('main_image')) {
            $main_image = $request->file('main_image');
            $filename = $main_image->getClientOriginalName();
            $main_image = $this->imageFactory->create($main_image, $filename);
        }

        return $main_image;
    }

    public function handlePostCategoriesRequest(Post $post, Request $request) {
        $currentPostCategories = $post->categories;
        $requestCategories = array_keys(LaravelUtil::inputWithKeysLike($request, 'category-'));

        // Pull apart the strings formatted like category-{category_id} and add this post to them
        $newPostCategories = array_filter(array_map(function($requestCategory) use ($post) {
            $category_id = intval(explode("-", $requestCategory)[1]);
            $category = $this->categoryRepository->find($category_id);

            if ($category) {
                return $this->categoryFactory->addPostToCategory($post, $category);
            } else {
                return null;
            }
        }, $requestCategories), function($x) { return $x !== null;});

        // now handle members of this post's current categories that are not present in the new post categories.
        // any member that appears in the latter and not in the former needs to be deleted
        $toDelete = $currentPostCategories->filter(function($postCategory) use ($newPostCategories) {
            /**
             * @var PostCategory $newPostCategory
             * @var Category $postCategory
             */
            foreach ($newPostCategories as $newPostCategory) {
                if ($newPostCategory->category_id == $postCategory->id) {
                    return false;
                }
            }

            return true;
        });

        // Now delete them
        foreach ($toDelete as $doomed) {
            $this->categoryFactory->removePostFromCategory($post, $doomed);
        }

       return $newPostCategories;
    }

    /**
     * Create a new post
     * @param Request $request
     */
    public function postCreate(Request $request)
    {
        // We will trust that the middleware is ensuring that only
        // priveleged users are hitting

        // @var Request
       // $request = app('request');
        $title = $request->input('title');
        $description = $request->input('description');
        $body = $request->input('body');
        $user_id = $this->guard->user()->id;

        $main_image = $this->makeMainImage($request);
        $main_image_id = $main_image !== null ? $main_image->id : null;

        if ($main_image) {
            $this->thumbnailFactory->create($main_image, 300);
        }

        $post = $this->postFactory->create(
            $title,
            $description,
            $body,
            $user_id,
            $main_image_id
        );

        return redirect($this->makeEditRoute($post))->with('post', $post);
    }

    public function postEdit(Request $request, $post_id) {
        /**
         * @var Post
         */
        $post = $this->postRepository->find($post_id);

        if ($post && !empty($request->input())) {
            $title = $request->input('title');
            $description = $request->input('description');
            $body = $request->input('body');
            $published = $request->has('published');
            $main_image = $this->makeMainImage($request);
            $postCategories = $this->handlePostCategoriesRequest($post, $request);
            //$main_image_id = $main_image !== null ? $main_image->id : null;

            if ($main_image) {
                $this->postFactory->setMainImage($post, $main_image);
                $this->thumbnailFactory->create($main_image, 300);
            }

            $post = $this->postFactory->edit($post, $title, $description, $body, $published);

            return redirect($this->makeEditRoute($post))->with('post', $post);
        } else {
            return redirect()->back();
        }
    }

    public function postDelete(Request $request, $post_id) {
        $this->postRepository->find($post_id)->delete();
        return redirect('/admin/posts');
    }
}