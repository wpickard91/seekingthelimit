<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Posts;

use WP\Http\Interfaces\RouteMapper;
use Illuminate\Routing\Router;
use WP\Images\Controllers\ImageApiController;
use WP\Posts\Controllers\PostController;
use WP\Posts\Controllers\PostApiController;

class PostRouteDirectory implements RouteMapper
{
    public function map (Router $router)
    {
        $router->group(
            ['prefix' => '/posts'],
            function (Router $router) {
                $this->mapPublicRoutes($router);
            }
        );

        $router->group(
            [
                'prefix' => '/admin/posts',
            //    'middleware' => ['auth','admin']
            ],
            function (Router $router) {
                $this->mapAdminRoutes($router);
            }
        );

        $router->group(
            ['prefix' => '/posts'],
            function (Router $router) {
                $this->mapApiRoutes($router);
            }
        );

        $router->group(
            ['prefix' => '/admin/stories'],
            function(Router $router) {
                $this->mapStoryRoutes($router);
            }
        );
    }

    protected function mapPublicRoutes (Router $router)
    {
        $router->get('/', [
            'as' => 'posts.index',
            'uses' => PostController::class . '@getIndex'
        ]);

        $router->get('/{id}', [
            'uses' => PostController::class . '@getPost'
        ])->where('id', '[0-9]+');
    }

    protected function mapAdminRoutes (Router $router)
    {
        $router->get('/create', [
            'as' => 'posts.create',
            'uses' => PostController::class .'@getCreate'
        ]);

        $router->post('/create', [
            'as' => 'posts.postCreate',
            'uses' => PostController::class . '@postCreate'
        ]);

        $router->get('/', PostController::class . '@getEditIndex');

        $router->get('/edit/{id}', [
            'as' => 'posts.edit',
            'uses' => PostController::class . '@getEdit'
        ]);

        $router->post('/edit/{id}', [
            'as' => 'posts.postEdit',
            'uses' => PostController::class . '@postEdit'
        ]);

        $router->post('/delete/{id}', [
            'as' => 'posts.delete',
            'uses' => PostController::class . '@postDelete'
        ]);
    }

    protected function mapApiRoutes (Router $router)
    {
        $router->group([
            'prefix' => '/postsApi/v0'
        ], function (Router $router) {
            $router->get('/', PostApiController::class . '@paginate');
            $router->get('/post/{postId}', PostApiController::class . '@getSingle')
                ->where('postId', '[0-9]+');

            $router->get('/count', PostApiController::class . '@getCount');
            $router->get('/invertedIndex', PostApiController::class . '@getInvertedIndex');
            $router->get('/postTeasers', PostApiController::class . '@getPostTeasers');

            $router->post('/create', PostApiController::class . '@postCreate');

            $router->put('/post/{post_id}', PostApiController::class . '@edit')
                ->where('post_id', '[0-9]+');

            $router->get('/post/{post_id}/images', ImageApiController::class . '@getImagesForPost')
                ->where('post_id', '[0-9]+');
            $router->get('/post/{post_id}/thumbnails', ImageApiController::class . '@getThumbnailsForPost')
                ->where('post_id', '[0-9]+');

            $router->put('/post/{post_id}/images', [
                'uses' => PostApiController::class . '@putImages'
            ])->where('post_id', '[0-9]+');

            $router->put('/post/{post_id}/images/main/{image_id}', [
                'uses' => PostApiController::class . '@putMainImage'
            ])->where('post_id', '[0-9]+')
                ->where('image_id', '[0-9]+');

            $router->delete('/post/{post_id}/images/{image_id}', PostApiController::class . '@deleteImage')
                ->where('post_id', '[0-9]+')
                ->where('image_id', '[0-9]+');

            $router->put('/post/{post_id}/category', [
                'uses' => PostApiController::class . '@putCategory'
            ])->where('post_id', '[0-9]+');

            $router->put('/post/{post_id}/keywords', PostApiController::class . '@putKeywords')
                ->where('post_id', '[0-9]+');
        });
    }

    public function mapStoryRoutes(Router $router)
    {
        $router->get('/create', PostController::class . '@getCreate');
    }
}