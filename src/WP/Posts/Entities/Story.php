<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Posts\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use WP\Posts\Entities\Chapter;
use WP\Posts\Entities\Post;

/**
 * Class Story
 * @package WP\Posts\Entities
 * Represents a story. Has many chapters which are 1 to 1 with posts
 *
 * @property Integer $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Story extends Model
{
    protected $table = 'stories';

    public function chapters()
    {
        return $this->hasMany(Chapter::class);
    }

    public function posts()
    {
        return $this->hasManyThrough(Post::class, Chapter::class);
    }

    public function getChapterSequence()
    {
        return array_map(function(Chapter $chapter) {
            return $chapter->id;
        }, $this->chapters->toArray());
    }
}