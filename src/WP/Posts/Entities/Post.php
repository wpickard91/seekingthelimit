<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Posts\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
//use WP\Categories\Entities\Category;
use Illuminate\Support\Collection;
use WP\Categories\Entities\Category;
use WP\Images\Entities\Image;
use WP\Users\Entities\User;

/*use WP\Images\Entities\PostImage;
use WP\Images\Entities\Thumbnail;
use WP\Keywords\Entities\Keyword;
use WP\Keywords\Entities\KeywordReference;*/

/**
 * Class Post
 * @package WP\Posts
 *
 * @property String $title
 * @property String $description
 * @property String $body
 * @property Integer $id
 * @property Integer $user_id
 * @property Integer $main_image_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Post extends Model
{

    protected $table = 'posts';

    protected $fillable = ['title', 'description', 'body', 'main_image_id', 'user_id'];

 /*   public function images()
    {
        return $this->belongsToMany(Image::class, 'postImages', 'post_id', 'image_id');
    }*/

    public function mainImage()
    {
        return $this->hasOne(Image::class, 'id', 'main_image_id');
    }

    public function categories() {
        return $this->belongsToMany(Category::class, 'post_categories', 'post_id', 'category_id');
    }

    public function isInCategory(Category $category) {
        /**
         * @var Collection
         */
        $myCategories = $this->categories;

        return $myCategories->contains(function($key, $myCategory) use ($category) {
            return $category->id == $myCategory->id;
        });
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
/*
    public function keywordReferences()
    {
        return $this->hasMany(KeywordReference::class);
    }

    public function keywords()
    {
        return $this->belongsToMany(Keyword::class, 'keywordReferences', 'post_id', 'keyword_id');
    }*/

/*    public function category()
    {
        return $this->belongsTo(Category::class);
    }*/
}