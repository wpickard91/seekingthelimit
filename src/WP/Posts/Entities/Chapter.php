<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Posts\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use WP\Posts\Entities\Story;
use WP\Posts\Entities\Post;

/**
 * Class Chapter
 * @package WP\Posts\Entities
 * Represents a chapter in a story
 *
 * @property Integer $id
 * @property Integer $story_id
 * @property Integer $post_id
 * @property Integer $next_id
 * @property Integer $prev_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Chapter extends Model
{
    protected $fillable = ['story_id', 'post_id', 'next_id', 'prev_id'];

    protected $table = 'chapters';

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function story()
    {
        return $this->belongsTo(Story::class);
    }

    public function next()
    {
        return $this->belongsTo(Chapter::class, 'next_id');
    }

    public function prev()
    {
        return $this->belongsTo(Chapter::class, 'prev_id');
    }
}