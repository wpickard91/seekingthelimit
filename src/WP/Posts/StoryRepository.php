<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Posts;

use WP\Posts\Entities\Post;
use Illuminate\Database\Eloquent\Collection;
use WP\Posts\Entities\Story;
use WP\Posts\Entities\Chapter;
use WP\Posts\Interfaces\StoryRepositoryInterface;

class StoryRepository implements StoryRepositoryInterface
{
    /**
     * @param $story_id
     * @return Collection
     */
    public function findChaptersForStory($story_id)
    {
        $this->findStory($story_id)
            ->chapters;
    }

    /**
     * @param $chapter_id
     * @return Story
     */
    public function findStoryForChapter($chapter_id)
    {
        return $this->findStory($this->findChapter($chapter_id));
    }

    /**
     * @param $story_id
     * @return Story
     */
    public function findStory($story_id)
    {
        return Story::find($story_id);
    }

    /**
     * @param $chapter_id
     * @return Chapter
     */
    public function findChapter($chapter_id)
    {
        return Chapter::find($chapter_id);
    }

    /**
     * @return Collection
     */
    public function all()
    {
        return Story::all();
    }
}