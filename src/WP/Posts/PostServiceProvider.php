<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Posts;

use Illuminate\Support\ServiceProvider;
use WP\Posts\Interfaces\PostRepositoryInterface;
use WP\Posts\Interfaces\PostFactoryInterface;
use WP\Posts\Entities\Post;
use Illuminate\Support\Facades\Artisan;
use WP\Console\Commands\GenerateInvertedIndex;

class PostServiceProvider extends ServiceProvider
{

    public function boot()
    {
        /*Post::saved(function (Post $post) {
            Artisan::call('posts:generate:index');
            Artisan::call('posts:generate:teasers');
            Artisan::call('posts:generate:thumbnails', [
                'width' => 300,
                'postId' => $post->id
            ]);
        });*/
    }

    public function register()
    {
        $this->app->bind(
            PostRepositoryInterface::class,
            PostRepository::class
        );

        $this->app->bind(
            PostFactoryInterface::class,
            PostFactory::class
        );
    }
}