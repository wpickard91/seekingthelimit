<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Posts\Interfaces;

use Illuminate\Support\Collection;
use WP\Posts\Entities\Chapter;
use WP\Posts\Entities\Post;
use WP\Posts\Entities\Story;

interface StoryRepositoryInterface
{
    /**
     * @param $story_id
     * @return Collection
     */
    public function findChaptersForStory($story_id);

    /**
     * @param $chapter_id
     * @return Story
     */
    public function findStoryForChapter($chapter_id);

    /**
     * @param $story_id
     * @return Story
     */
    public function findStory($story_id);

    /**
     * @param $chapter_id
     * @return Chapter
     */
    public function findChapter($chapter_id);

    /**
     * @return Collection
     */
    public function all();
}