<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Posts\Interfaces;

use WP\Categories\Entities\Category;
use WP\Keywords\Entities\Keyword;
use WP\Posts\Entities\Post;
use WP\Images\Entities\Image;
/**
 * Interface PostFactoryInterface
 *
 * Define class for creating/manipulating posts
 *
 * @package WP\Posts\Interfaces
 */
interface PostFactoryInterface
{
    public function create($title, $description, $body, $user_id, $main_image_id = null);

    public function edit(Post $post, $title, $description, $body, $published);

    public function setMainImage(Post $post, Image $image);

    public function removeImage(Post $post, Image $image);
}