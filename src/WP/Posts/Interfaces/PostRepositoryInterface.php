<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Posts\Interfaces;

use Illuminate\Database\Eloquent\Collection;

interface PostRepositoryInterface
{
    public function all();

    /**
     * @param Integer $id
     * @return mixed
     */
    public function find($id);

    /**
     * @return Collection
     */
    public function paginate($take, $start, $end);

    /**
     * @param Integer $count
     * @return Collection
     */
    public function recent($count);
}