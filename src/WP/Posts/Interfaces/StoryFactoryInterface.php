<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Posts\Interfaces;

use WP\Posts\Entities\Chapter;
use WP\Posts\Entities\Post;
use WP\Posts\Entities\Story;

interface StoryFactoryInterface
{

    /**
     * @param $story_id
     * @param $post_id
     * @param $prev_id
     * @param $next_id
     * @return Chapter
     */
    public function createChapter($story_id, $post_id, $prev_id, $next_id);

    /**
     * @param array $post_ids
     * @return Story
     */
    public function create($post_ids = []);

    public function delete($story_id);

    public function assignNextChapter($this_chapter_id, $next_chapter_id);

    public function assignPrevChapter($this_chapter_id, $prev_chapter_id);
}