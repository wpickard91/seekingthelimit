<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Keywords\Controllers;

use Illuminate\Http\Request;
use WP\Http\Controllers\BaseApiController;
use WP\Keywords\Interfaces\KeywordFactoryInterface;
use WP\Keywords\Interfaces\KeywordRepositoryInterface;
use WP\Posts\Interfaces\PostRepositoryInterface;

class KeywordApiController extends BaseApiController
{
    protected $plural = 'keywords';

    protected $singular = 'keyword';

    /**
     * @var KeywordRepositoryInterface
     */
    protected $keywordRepository;

    /**
     * @var KeywordFactoryInterface
     */
    protected $keywordFactory;

    /**
     * @var PostRepositoryInterface
     */
    protected $postRepository;

    public function __construct(
        Request $request,
        KeywordRepositoryInterface $keywordRepository,
        KeywordFactoryInterface $keywordFactory,
        PostRepositoryInterface $postRepository
    ) {
        parent::__construct($request);
        $this->keywordRepository = $keywordRepository;
        $this->keywordFactory = $keywordFactory;
        $this->postRepository = $postRepository;
    }

    public function getIndex()
    {
        return $this->collection(
            $this->keywordRepository->all()
        );
    }

    /**
     * return keywords coupled with the number of occurrences they have
     */
    public function getReferences()
    {
        return $this->collection(
            $this->keywordRepository->allReferences()
        );
    }

    public function getKeyword($keyword_id)
    {
        return $this->single(
            $this->keywordRepository->find($keyword_id)
        );
    }

    public function getKeywordByName($name)
    {
        return $this->single(
            $this->keywordRepository->findByName($name)
        );
    }

    public function getKeywordsForPost($postId)
    {
        return $this->collection(
            $this->keywordRepository->forPost($postId)
        );
    }

    public function postCreate()
    {
        $name = $this->request->input('name');
        return $this->single(
            $this->keywordFactory->create($name)
        );
    }

    public function putKeyword($keyword_id)
    {
        $keyword = $this->keywordRepository->find($keyword_id);
        $keyword = $this->keywordFactory->edit($keyword, $this->request->input('name'));
        return $this->single($keyword);
    }

    public function putReference($keyword_id, $post_id)
    {
        $this->keywordFactory->createReference(
            $this->keywordRepository->find($keyword_id),
            $this->postRepository->find($post_id)
        );
    }

    public function deleteKeyword($keyword_id)
    {
        $keyword = $this->keywordRepository->find($keyword_id);
        $this->keywordFactory->delete($keyword);
    }
}