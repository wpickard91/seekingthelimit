<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Keywords;

use WP\Keywords\Entities\Keyword;
use WP\Keywords\Entities\KeywordReference;
use WP\Keywords\Interfaces\KeywordFactoryInterface;
use WP\Posts\Entities\Post;

class KeywordFactory implements KeywordFactoryInterface
{
    public function create($name)
    {
        return Keyword::firstOrCreate(
            ['name' => $name]
        );
    }

    public function edit(Keyword $keyword, $name)
    {
        $keyword->name = $name;
        $keyword->save();
        return $keyword;
    }

    public function createReference(Keyword $keyword, Post $post)
    {
        KeywordReference::firstOrCreate([
            'keyword_id' => $keyword->id,
            'post_id' => $post->id
        ]);
    }

    public function delete(Keyword $keyword)
    {
        $keyword->delete();
    }
}