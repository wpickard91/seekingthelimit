<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Keywords;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Routing\Router;
use WP\Http\Interfaces\RouteMapper;
use WP\Keywords\Controllers\KeywordApiController;

class KeywordsRouteDirectory implements RouteMapper
{
    public function map(Router $router)
    {
        $router->group(
            ['prefix' => '/keywords'],
            function(Router $router) {
                $this->mapApiRoutes($router);
            }
        );
    }

    public function mapApiRoutes(Router $router)
    {
        $router->group(
            ['prefix' => '/keywordsApi/v0'],
            function(Router $router) {
                $router->get('/', KeywordApiController::class . '@getIndex');

                $router->get('/keywords/references', KeywordApiController::class . '@getReferences');

                $router->get('/keyword/{id}', KeywordApiController::class . '@getKeyword')
                    ->where('id', '[0-9]+');

                $router->get('/keyword?name={name}', KeywordApiController::class . '@getKeywordByName');

                $router->post('/keyword/create', KeywordApiController::class . '@postCreate');

                $router->put('/keyword/{id}', KeywordApiController::class . '@putKeyword')
                    ->where('id', '[0-9]+');

                $router->delete('/keyword/{id}', KeywordApiController::class . '@deleteKeyword')
                    ->where('id', '[0-9]+');

                $router->get('/keywords?post_id={postid}', KeywordApiController::class . '@getKeywordsForPost')
                    ->where('postid', '[0-9]+');

                $router->put('/keyword/{keyword_id}/reference/{post_id}', KeywordApiController::class . '@putReference')
                    ->where('keyword_id', '[0-9]+')
                    ->where('post_id', '[0-9]+');
            }
        );
    }
}