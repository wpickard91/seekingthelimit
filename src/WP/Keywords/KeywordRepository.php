<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Keywords;

use WP\Keywords\Entities\Keyword;
use WP\Keywords\Entities\KeywordReference;
use WP\Keywords\Interfaces\KeywordRepositoryInterface;

class KeywordRepository implements KeywordRepositoryInterface
{
    public function all()
    {
        return Keyword::all();
    }

    public function find($keyword_id)
    {
        return Keyword::find($keyword_id);
    }

    public function findByName($keyword_name)
    {
        return Keyword::where('name', $keyword_name)
            ->first();
    }

    public function forPost($post_id)
    {
        return Keyword::whereHas(KeywordReference::class, function($query) use($post_id) {
            $query->where('post_id', $post_id);
        })->get();
    }

    public function allReferences()
    {
        return Keyword::with('keywordReferences')->get();
    }
}