<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Keywords;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use WP\Posts\PostRouteDirectory;
use WP\Users\UserRouteDirectory;
use WP\Keywords\Interfaces\KeywordRepositoryInterface;
use WP\Keywords\KeywordRepository;
use WP\Keywords\Interfaces\KeywordFactoryInterface;
use WP\Keywords\KeywordFactory;

class KeywordsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            KeywordRepositoryInterface::class,
            KeywordRepository::class
        );

        $this->app->bind(
            KeywordFactoryInterface::class,
            KeywordFactory::class
        );
    }
}