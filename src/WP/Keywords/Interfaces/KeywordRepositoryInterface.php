<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Keywords\Interfaces;

interface KeywordRepositoryInterface
{
    public function find($keyword_id);

    public function findByName($keyword_name);

    public function all();

    public function forPost($post_id);

    public function allReferences();
}