<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Keywords\Interfaces;

use WP\Keywords\Entities\Keyword;
use WP\Posts\Entities\Post;

interface KeywordFactoryInterface
{
    public function create($name);

    public function edit(Keyword $keyword, $name);

    public function delete(Keyword $keyword);

    public function createReference(Keyword $keyword, Post $post);
}