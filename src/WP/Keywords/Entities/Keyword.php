<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Keywords\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use WP\Keywords\Entities\KeywordReference;

/**
 * Class Keyword
 * @package WP\Keywords\Entities
 *
 * @property Integer $id
 * @property String $name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Keyword extends Model
{
    protected $table = 'keywords';

    protected $fillable = ['name'];

    public function keywordReferences()
    {
        return $this->hasMany(KeywordReference::class);
    }
}