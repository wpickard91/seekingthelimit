<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Keywords\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use WP\Keywords\Entities\Keyword;
use WP\Posts\Entities\Post;

/**
 * Class KeywordReference
 * @package WP\Keywords\Entities
 *
 * @property Integer $id
 * @property Integer $post_id
 * @property Integer $keyword_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class KeywordReference extends Model
{
    protected $table = 'keywordreferences';

    protected $fillable = ['post_id', 'keyword_id'];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function keyword()
    {
        return $this->belongsTo(Keyword::class);
    }
}