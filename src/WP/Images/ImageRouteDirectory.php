<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Images;

use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use WP\Http\Interfaces\RouteMapper;
use WP\Images\Controllers\ImageApiController;
use WP\Images\Entities\Image;

class ImageRouteDirectory implements RouteMapper
{
    public function map(Router $router)
    {
        $router->group(
            ['prefix' => '/api/images'],
            function (Router $router) {
                $router->post('/upload', ImageApiController::class . '@postUpload');
            }
        );
    }


    public function mapApiRoutes(Router $router)
    {
        $router->group([
            'prefix' => '/imageApi/v0'
        ], function(Router $router) {
            $router->get('/image/{image_id}', [
                'uses' => ImageApiController::class . '@getImage'
            ])->where('image_id', '[0-9]+');

            $router->post('/create', ImageApiController::class . '@postCreate');

            $router->put('/image/{image_id}', [
                'uses' => ImageApiController::class . '@putImage'
            ])->where('image_id', '[0-9]+');

            $router->delete('/image/{image_id}', ImageApiController::class . '@deleteImage')
                ->where('image_id', '[0-9]+');
        });
    }
}