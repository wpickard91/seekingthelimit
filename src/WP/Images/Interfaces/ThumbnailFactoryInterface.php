<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Images\Interfaces;

use Symfony\Component\HttpFoundation\File\File;
use WP\Images\Entities\Image;
use WP\Images\Entities\Thumbnail;
use WP\Posts\Entities\Post;

interface ThumbnailFactoryInterface
{
    /**
     * @param Image $image
     * @param $width
     * @return Thumbnail
     */
    public function create(Image $image, $width);

    public function delete(Thumbnail $image);
}