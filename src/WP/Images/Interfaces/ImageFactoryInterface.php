<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Images\Interfaces;

use Symfony\Component\HttpFoundation\File\File;
use WP\Images\Entities\Image;
use WP\Posts\Entities\Post;

interface ImageFactoryInterface
{
    public function create(File $file, $filename, $absolute_path = '', $alt = '');

    public function edit(Image $image, $caption, $title, $alt);

    public function move(Image $image, $parent_path);

    public function deleteImage(Image $image);

    public function getImageFileName(Image $image);
}