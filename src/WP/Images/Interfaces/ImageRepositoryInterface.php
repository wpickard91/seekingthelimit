<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Images\Interfaces;

interface ImageRepositoryInterface
{
    public function forPost($post_id);

    public function find($imageId);

    public function findPostImage($post_id, $image_id);
}