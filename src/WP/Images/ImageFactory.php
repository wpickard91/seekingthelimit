<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Images;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use WP\Images\Entities\Image;
use WP\Images\Entities\PostImage;
use WP\Images\Interfaces\ImageFactoryInterface;
use WP\Images\Interfaces\ThumbnailFactoryInterface;
use WP\Posts\Entities\Post;
use Symfony\Component\HttpFoundation\File\File;

class ImageFactory implements ImageFactoryInterface
{
   /* /**
     * @var ThumbnailFactoryInterface
     *
    protected $thumbnailFactory;

    public function __construct(
        ThumbnailFactoryInterface $thumbnailFactory
    ) {
        $this->thumbnailFactory = $thumbnailFactory;
    }*/

    function getImageFileName(Image $image) {
        $path = $image->absolute_path ? $image->absolute_path : $image->relative_path ?  $image->relative_path : '';

        $exploded = explode('/', $path);
        if ($path && end($exploded)) {
            return end($exploded);
        } else {
            return '';
        }
    }

    public function create (File $file, $filename, $absolutePath = '', $alt = '')
    {
        // Make sure we have assumed directory structure
        $this->ensureDirectories();

        $relativePath = '';
        $destinationPath = '';

        // if absolute path was passed in, use that, otherwise use tmp directory
        if ($absolutePath) {
            $destinationPath = $absolutePath;
            $relativePath = substr(
                    $absolutePath,
                    strpos($absolutePath, public_path()) . strlen(public_path())
                ) . "/" . $filename;
        } else {
            $destinationPath = public_path() . '/images/tmp';
            $relativePath = '/images/tmp/' . $filename;
        }

        $file->move($destinationPath, $filename);

        $image = Image::create([
            'absolute_path' => $destinationPath . '/' . $filename,
            'relative_path' => $relativePath,
            'alt' => $alt
        ]);

        return $image;
    }

    public function edit(Image $image, $caption, $title, $alt) {
        $image->title = $title;
        $image->caption = $caption;
        $image->alt = $alt;
        $image->save();
        return $image;
    }

    /**
     * @param Image $image - Image to move
     * @param $parent_path - The directory that this image will be moved to
     * @return Image
     */
    public function move(Image $image, $parent_path) {
        if (file_exists($parent_path)) {
            $filename = $this->getImageFileName($image);
            if ($filename) {
                $new_absolute_path = $parent_path . '/' . $filename;
                $new_relative_path = substr($new_absolute_path, strpos($new_absolute_path, public_path()) . strlen(public_path()));

                $file = new File($image->absolute_path, true);
                $file->move($parent_path, $filename);

                $image->absolute_path = $new_absolute_path;
                $image->relative_path = $new_relative_path;
                $image->save();
            }
        }

        return $image;
    }

    public function deleteImage(Image $image)
    {
        $image->delete();
    }

    private function ensureDirectories()
    {
        $publicPath = public_path();
        $imagesPath = $publicPath . '/images';
        $tmpPath = $imagesPath . '/tmp';
        $postsPath = $imagesPath . '/posts';

        if (!file_exists($tmpPath)) {
            mkdir($tmpPath);
        }

        if (!file_exists($postsPath)) {
            mkdir($postsPath);
        }
    }
}