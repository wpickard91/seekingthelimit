<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Images;

use WP\Images\Entities\PostImage;
use WP\Images\Interfaces\ImageRepositoryInterface;
use WP\Images\Entities\Image;

class ImageRepository implements ImageRepositoryInterface
{
    public function forPost($post_id)
    {
        return Image::where('post_id', $post_id);
    }

    public function find($imageId)
    {
        return Image::find($imageId);
    }

    public function findPostImage($post_id, $image_id)
    {
        return PostImage::where('post_id', $post_id)
            ->where('image_id', $image_id)
            ->first();
    }
}