<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Images\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use WP\Http\Controllers\BaseApiController;
use WP\Images\Interfaces\ImageFactoryInterface;
use WP\Images\Interfaces\ImageRepositoryInterface;
use WP\Posts\Interfaces\PostRepositoryInterface;

class ImageApiController extends BaseApiController
{
    /**
     * @var ImageFactoryInterface
     */
    protected $imageFactory;

    /**
     * @var ImageRepositoryInterface
     */
    protected $imageRepository;

    /**
     * @var PostRepositoryInterface
     */
    protected $postRepository;

    protected $plural = 'images';

    protected $singular = 'image';

    public function __construct(
        Request $request,
        ImageFactoryInterface $imageFactory,
        ImageRepositoryInterface $imageRepository,
        PostRepositoryInterface $postRepository
    ) {
        parent::__construct($request);
        $this->imageFactory = $imageFactory;
        $this->imageRepository = $imageRepository;
        $this->postRepository = $postRepository;;
    }

    public function postUpload(Request $request) {
        if ($request->hasFile('image')) {
            $uploadedFile = $request->file('image');
            $createdImage = $this->imageFactory->create($uploadedFile, $uploadedFile->getClientOriginalName());
            return $this->single($createdImage);
        }
    }

    public function getImage($imageId)
    {
        return $this->single(
            $this->imageRepository->find($imageId)
        );
    }


    public function postCreate()
    {
        $request = $this->request;
        $images = new Collection();
        if ($request->hasFile('images')) {
            $uploadedImages = $request->file('images');

            /**
             * @var UploadedFile $image
             */
            foreach($uploadedImages as $image) {
                $filename = $image->getClientOriginalName();
                $images->add($this->imageFactory->create($image, $filename));
            }
        }

        return $this->collection($images);
    }

    public function getImagesForPost($postId)
    {
        return $this->collection(
            $this->postRepository->find($postId)->images
        );
    }

    public function getThumbnailsForPost($postId)
    {
        // warning: this is shitty. need to either define a 'thumbnails' relation method on post
        // or update schema
        $thumbs = new Collection();

        $post = $this->postRepository->find($postId);
        $images = $post->images;

        foreach($images as $image) {
            if($image->thumbnail) {
                $thumbs->push($image->thumbnail->image);
            }
        }

        return $this->collection(
            $thumbs
        );
    }

    public function putImage($image_id)
    {
        $image = $this->imageRepository->find($image_id);

        $title = $this->request->input('title');
        $alt = $this->request->input('alt');
        $caption = $this->request->input('caption');

        $image->title = $title;
        $image->alt = $alt;
        $image->caption = $caption;

        $image->save();

        return $this->single($image);
    }

    public function deleteImage($image_id)
    {
        $this->imageFactory->deleteImage(
            $this->imageRepository->find($image_id)
        );
    }
}