<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Images;

use Illuminate\Support\ServiceProvider;
use WP\Images\Interfaces\ImageFactoryInterface;
use WP\Images\Interfaces\ImageRepositoryInterface;
use WP\Images\Interfaces\ThumbnailFactoryInterface;

class ImageServiceProvider extends ServiceProvider
{
    public function register() {
        $this->app->bind(
            ImageFactoryInterface::class,
            ImageFactory::class
        );

        $this->app->bind(
            ImageRepositoryInterface::class,
            ImageRepository::class
        );

        $this->app->bind(
            ThumbnailFactoryInterface::class,
            ThumbnailFactory::class
        );
    }
}