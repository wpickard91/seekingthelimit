<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Images\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use WP\Posts\Entities\Post;

/**
 * Class Image
 * @package WP\Images
 *
 * @property Integer $id
 * @property String $absolute_path
 * @property String $relative_path
 * @property String $alt
 * @property String $title
 * @property String $caption
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Image extends Model
{
    protected $fillable = ['absolute_path', 'relative_path', 'alt', 'title', 'caption', 'post_id'];

    public function thumbnail()
    {
        return $this->belongsTo(Thumbnail::class, 'id', 'original_image_id');
    }
}