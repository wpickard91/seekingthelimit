<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Images\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Thumbnail
 * @package WP\Images\Entities
 *
 * @property Integer $id
 * @property Integer $image_id
 * @property Integer $original_image_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Thumbnail extends Model
{
    protected $table = 'thumbnails';

    protected $fillable = ['image_id', 'original_image_id'];

    public function image()
    {
        return $this->hasOne(Image::class, 'id', 'image_id');
    }

    public function originalImage()
    {
        return $this->hasOne(Image::class, 'id', 'original_image_id');
    }
}