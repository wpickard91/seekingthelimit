<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Images\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class PostImage extends Model
{
    protected $table = 'postImages';

    protected $fillable = ['post_id', 'image_id'];

    protected $primaryKey = 'id';

}