<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Images;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use WP\Images\Entities\Image;
use WP\Images\Entities\PostImage;
use WP\Images\Entities\Thumbnail;
use WP\Images\Interfaces\ImageFactoryInterface;
use WP\Images\Interfaces\ThumbnailFactoryInterface;
use WP\Posts\Entities\Post;
use Symfony\Component\HttpFoundation\File\File;

class ThumbnailFactory implements ThumbnailFactoryInterface
{
    /**
     * @var ImageFactoryInterface
     */
    protected $imageFactory;

    public function __construct(
        ImageFactoryInterface $imageFactory
    ) {
        $this->imageFactory = $imageFactory;
    }

    /**
     * Create the thumbnail on disk and in the db
     * @param Image $image
     * @param $width
     * @return Thumbnail
     */
    public function create(Image $image, $width)
    {
        $srcPath = $image->absolute_path;
        $thumbnail = null;

        if (file_exists($srcPath)) {
            $srcImage = new File($srcPath);
            $absoluteDir = $srcImage->getPath();
            $srcFilename = $srcImage->getFilename();
            $absoluteDestination = $absoluteDir . '/thumb_' . $srcFilename;

            /**
             * @var Image
             */
            $createdImage = null;

            $mime = $srcImage->getMimeType();
            switch($mime) {
                case 'image/jpeg':
                    $createdImage = $this->createJpeg($srcImage, $absoluteDestination, $width);
                    break;
                default:
                    return null;
            }

            if ($createdImage) {
                $thumbnail = Thumbnail::create([
                    'original_image_id' => $image->id,
                    'image_id' => $createdImage->id
                ]);

                $createdImage->title = $image->title;
                $createdImage->alt = $image->alt;
                $createdImage->caption = $image->caption;
                $createdImage->save();
            }
        }

        // probably should throw exception if null
        return $thumbnail;
    }

    public function delete(Thumbnail $image)
    {
        $oImage = $image->originalImage;
        $dImage = $image->image;

        $image->delete();
        $oImage->delete();
        $dImage->delete();
    }

    /**
     * @param File $srcFile
     * @param string $dest
     * @param Integer $width
     * @return Image
     */
    protected function createJpeg(File $srcFile, $dest, $width)
    {
        // for loading large images
        $phpMemoryLimit = ini_get('memory_limit');

        $originalPath = $srcFile->getPath() . '/' . $srcFile->getFilename();

        $sourceImage = imagecreatefromjpeg($originalPath);

        $srcWidth = imagesx($sourceImage);
        $srcHeight = imagesy($sourceImage);

        $desiredHeight = floor($srcHeight * ($width / $srcWidth));

        $virtualImage = $this->copyAndResizeImage($sourceImage, $width, $desiredHeight);
        imagejpeg($virtualImage, $dest);
        $createdFile = new File($dest);
        $image = $this->imageFactory->create($createdFile, $createdFile->getFilename(), $createdFile->getPath());

        //restore memlimit and free memory
        ini_set('memory_limit', $phpMemoryLimit);
        unset($virtualImage);
        unset($sourceImage);

        return $image;
    }

    /**
     * @param $sourceImage
     * @param $desiredWidth
     * @param $desiredHeight
     */
    private function copyAndResizeImage($sourceImage, $desiredWidth, $desiredHeight)
    {
        $virtualImage = imagecreatetruecolor($desiredWidth, $desiredHeight);
        imagecopyresampled(
            $virtualImage,
            $sourceImage,
            0,
            0,
            0,
            0,
            $desiredWidth,
            $desiredHeight,
            imagesx($sourceImage),
            imagesy($sourceImage)
        );

        return $virtualImage;
    }
}