<?php

namespace WP\Common;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
class LaravelUtil
{
    public static function inputWithKeysLike(Request $request, $keys_like) {
        $input = $request->input();
        $keys = array_filter(array_keys($input), function($key) use($keys_like) {
            return strpos($key, $keys_like) !== false;
        });

        return array_intersect_key($input, array_flip($keys));
    }

    public static function groupEntitiesByDate(Collection $entities) {

    }
}