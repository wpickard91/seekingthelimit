<?php

namespace WP\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Input;
use Symfony\Component\Console\Input\InputArgument;
use WP\Keywords\Interfaces\KeywordFactoryInterface;
use WP\Support\Generators\LatinTextGenerator;

class GenerateKeywords extends Command
{
    protected $signature = 'keywords:generate {count}';

    protected $description = 'generate count keywords';

    protected $count = 0;

    protected $arguments = [
        ['count', InputArgument::REQUIRED, 'Number of keywords to generate']
    ];

    /**
     * @var KeywordFactoryInterface
     */
    protected $keywordFactory;

    public function __construct(
        KeywordFactoryInterface $keywordFactory
    ) {
        parent::__construct();

        $this->keywordFactory = $keywordFactory;
    }

    public function getArguments()
    {
        return $this->arguments;
    }

    public function handle()
    {
        $count = $this->argument('count');

        $this->info('Creating ' . $count . ' keywords');

        $latin = new LatinTextGenerator();

        for ($i = 0; $i < $count; $i++) {
            $keyword = $this->keywordFactory->create($latin->generateString(1));
        }

        $this->info('Done');
    }

}