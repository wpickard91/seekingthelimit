<?php

namespace WP\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpFoundation\File\File;
use WP\Categories\Entities\Category;
use WP\Categories\Interfaces\CategoryRepositoryInterface;
use WP\Images\Interfaces\ImageFactoryInterface;
use WP\Keywords\Interfaces\KeywordRepositoryInterface;
use WP\Posts\Interfaces\PostFactoryInterface;
use WP\Posts\Interfaces\PostRepositoryInterface;
use WP\Support\Generators\LatinTextGenerator;
use Illuminate\Support\Collection;
use WP\Posts\Entities\Post;

class GenerateInvertedIndex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'posts:generate:index {destination?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate inverted index of posts';
    /**
     * @var KeywordRepositoryInterface
     */
    protected $keywordRepository;

    /**
     * @var PostRepositoryInterface
     */
    protected $postRepository;

    protected $defaultDestination;

    protected $arguments = [
        ['destination', InputArgument::OPTIONAL, 'Absolute location of the index', false]
    ];

    /**
     * @param PostRepositoryInterface $postRepository
     * @param KeywordRepositoryInterface $keywordRepository
     */
    public function __construct(
        PostRepositoryInterface $postRepository,
        KeywordRepositoryInterface $keywordRepository
    ) {

        parent::__construct();

        $this->defaultDestination = base_path() . DIRECTORY_SEPARATOR . 'resources'. DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'invertedIndex.json';

        $this->postRepository = $postRepository;
        $this->keywordRepository = $keywordRepository;
    }

    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $destination = $this->argument('destination');
        if (!$destination) $destination = $this->defaultDestination;

        $this->info('Destination: ' . $destination);


        file_put_contents($destination, json_encode($this->createInvertedIndex()));
        $this->info('Done');
    }

    public function createInvertedIndex()
    {
        /** @var Collection $posts */
        $posts = $this->postRepository->all();

        $index = [];

        /**
         * @var Post $post
         */
        foreach ($posts as $post) {
            $title = $post->title;
            $description = $post->description;
            $kws = $post->keywords->map(function($kw){ return $kw->name; });

            $words = array_filter(
                array_merge(explode(" ",  $title), explode(" ", $description), explode(" ", $kws)),
                function($word) {
                    return !empty($word);
                });

            foreach ($words as $word) {
                if (!isset($index[$word])) $index[$word] = [];
                $index[$word][] = $post->id;
            }
        }

        return $index;
    }
}
