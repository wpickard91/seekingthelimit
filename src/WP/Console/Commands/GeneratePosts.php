<?php

namespace WP\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpFoundation\File\File;
use WP\Categories\Entities\Category;
use WP\Categories\Interfaces\CategoryFactoryInterface;
use WP\Categories\Interfaces\CategoryRepositoryInterface;
use WP\Images\Interfaces\ImageFactoryInterface;
use WP\Keywords\Interfaces\KeywordRepositoryInterface;
use WP\Posts\Interfaces\PostFactoryInterface;
use WP\Support\Generators\LatinTextGenerator;

class GeneratePosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'posts:generate {count}  ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate count posts';

    /**
     * @var number of posts to generate
     */
    protected $count = 0;

    /**
     * @var PostFactoryInterface
     */
    protected $postFactory;

    /**
     * @var ImageFactoryInterface
     */
    protected $imageFactory;

    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * @var CategoryFactoryInterface
     */
    protected $categoryFactory;

    /**
     * @var KeywordRepositoryInterface
     */
    protected $keywordRepository;

    protected $arguments = [
        ['count', InputArgument::REQUIRED, 'Nummber of posts to gen', 0]
    ];

    /**
     * @param PostFactoryInterface $postFactory
     * @param ImageFactoryInterface $imageFactory
     * @param CategoryRepositoryInterface $categoryRepo
     * @param KeywordRepositoryInterface $keywordRepository
     */
    public function __construct(
        PostFactoryInterface $postFactory,
        ImageFactoryInterface $imageFactory,
        CategoryRepositoryInterface $categoryRepo,
        CategoryFactoryInterface $categoryFactory,
        KeywordRepositoryInterface $keywordRepository
    ) {

        parent::__construct();

        $this->postFactory = $postFactory;
        $this->imageFactory = $imageFactory;
        $this->categoryRepository = $categoryRepo;
        $this->categoryFactory = $categoryFactory;
        $this->keywordRepository = $keywordRepository;
    }

    public function getArguments()
    {
        return  [
            ['count', InputArgument::REQUIRED, 'Nummber of posts to gen', 0]
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $count = $this->argument('count');

        $this->info('Creating ' . $count . ' posts...');

        $latinGenerator = new LatinTextGenerator();

        for ($i = 0; $i < $count; $i++) {
            $title = $latinGenerator->generateString(rand(5, 10));
            $description = $latinGenerator->generateString(rand(15, 25));
            $body = $latinGenerator->generateString(rand(500, 2500));
            $images = $this->randomImages(1, 1);
          //  $numKeywords = rand(1, 5);

            $post = $this->postFactory->create(
                $title,
                $description,
                $body,
                1
            );
            $post->published = 1;
            $post->save();

        /*    for ($k = 0; $k < $numKeywords; $k++) {
                $this->postFactory->addKeyword($post, $this->getRandomKeyword());
            }*/

      //      $this->postFactory->assignImagesToPost($images, $post);
            $this->postFactory->setMainImage($post, $images[0]);
            $this->categoryFactory->addPostToCategory($post, $this->getRandomCategory());

        }

        $this->info('Done');
    }



    protected function randomImages($min = 5, $max = 15)
    {
        $number = rand($min, $max);
        $availableImages = [];
        $images = [];

        // Read from the stock image directory
        $directory = base_path() . '\src\WP\Support\Data\StockImages';

        if (file_exists($directory)) {
            $dh = opendir($directory);
            while (false !== ($filename = readdir($dh))) {
                if ($filename !== '.' && $filename !== '..') {
                   // $file = new File($directory . '/' . $filename, true);
                    $availableImages[] = $filename;
                }
            }

            // shuffle the array and take first n elements instead of building an array of indices
            // into the available images array
            shuffle($availableImages);
            $temp = [];
            for ($i = 0; $i < $number; $i++) {
                $filename = $availableImages[$i];
                $newFileName = 'c_' . $availableImages[$i];
                // Create a copy of the chosen file because we will move it
                // when we create the model and we don't want to move the
                // stock photos
                if (copy($directory . '/' . $filename, $directory . '/' . $newFileName)) {
                    $file = new File($directory . '/' . $newFileName, true);
                    $temp[] = [$file , $newFileName];
                }
            }
         //   $availableImages = array_slice($availableImages, 0, $number);
            $availableImages = $temp;


            foreach($availableImages as $availImage) {
                $images[] = $this->imageFactory->create($availImage[0], $availImage[1]);
            }
        }

        return $images;
    }

    /**
     * @return Category
     */
    public function getRandomCategory()
    {
        $categories = $this->categoryRepository->all();
        return $categories[rand(0, count($categories) - 1)];
    }
/*
    public function getRandomKeyword()
    {
        $keywords = $this->keywordRepository->all();
        return $keywords[rand(0, count($keywords) - 1)];
    }*/
}
