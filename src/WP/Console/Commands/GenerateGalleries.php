<?php

namespace WP\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpFoundation\File\File;
use WP\Categories\Entities\Category;
use WP\Categories\Interfaces\CategoryFactoryInterface;
use WP\Categories\Interfaces\CategoryRepositoryInterface;
use WP\Galleries\Interfaces\GalleryFactoryInterface;
use WP\Images\Interfaces\ImageFactoryInterface;
use WP\Keywords\Interfaces\KeywordRepositoryInterface;
use WP\Posts\Interfaces\PostFactoryInterface;
use WP\Support\Generators\LatinTextGenerator;

class GenerateGalleries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'galleries:generate {count}  ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate count galleries';

    /**
     * @var number of posts to generate
     */
    protected $count = 0;

    /**
     * @var GalleryFactoryInterface
     */
    protected $galleryFactory;

    /**
     * @var ImageFactoryInterface
     */
    protected $imageFactory;

    /**
     * @var LatinTextGenerator
     */
    protected $latinGenerator;

    protected $arguments = [
        ['count', InputArgument::REQUIRED, 'Nummber of galeries to gen', 0]
    ];

    /**
     * @param GalleryFactoryInterface $galleryFactory
     * @param ImageFactoryInterface $imageFactory
     */
    public function __construct(
        GalleryFactoryInterface $galleryFactory,
        ImageFactoryInterface $imageFactory
    ) {

        parent::__construct();

        $this->galleryFactory = $galleryFactory;
        $this->imageFactory = $imageFactory;
        $this->latinGenerator = new LatinTextGenerator();
    }

    public function getArguments()
    {
        return  [
            ['count', InputArgument::REQUIRED, 'Nummber of posts to gen', 0]
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $count = $this->argument('count');

        $this->info('Creating ' . $count . ' galleries...');

        for ($i = 0; $i < $count; $i++) {
            $title = $this->latinGenerator->generateString(rand(5, 10));
            $description = $this->latinGenerator->generateString(rand(15, 25));
            $images = $this->randomImages(10, 20);

            $gallery = $this->galleryFactory->create(
                $title,
                $description,
                $images,
                1
            );
            $gallery->published = 1;
            $gallery->save();

            if (count($images) > 0) {
                $this->galleryFactory->setMainImage($gallery, $images[0]);
            }
        }

        $this->info('Done');
    }



    protected function randomImages($min = 5, $max = 15)
    {
        $number = rand($min, $max);
        $availableImages = [];
        $images = [];

        // Read from the stock image directory
        $directory = base_path() . '\src\WP\Support\Data\StockImages';

        if (file_exists($directory)) {
            $dh = opendir($directory);
            while (false !== ($filename = readdir($dh))) {
                if ($filename !== '.' && $filename !== '..') {
                   // $file = new File($directory . '/' . $filename, true);
                    $availableImages[] = $filename;
                }
            }

            // shuffle the array and take first n elements instead of building an array of indices
            // into the available images array
            shuffle($availableImages);
            $temp = [];
            for ($i = 0; $i < $number; $i++) {
                $filename = $availableImages[$i];
                $newFileName = 'c_' . $availableImages[$i];
                // Create a copy of the chosen file because we will move it
                // when we create the model and we don't want to move the
                // stock photos
                if (copy($directory . '/' . $filename, $directory . '/' . $newFileName)) {
                    $file = new File($directory . '/' . $newFileName, true);
                    $temp[] = [$file , $newFileName];
                }
            }
         //   $availableImages = array_slice($availableImages, 0, $number);
            $availableImages = $temp;


            foreach($availableImages as $availImage) {
                $image = $this->imageFactory->create($availImage[0], $availImage[1]);
                $caption = $this->latinGenerator->generateString(rand(5, 25));
                $title = $this->latinGenerator->generateString(rand(1, 5));
                $alt = $this->latinGenerator->generateString(rand(5, 25));
                $this->imageFactory->edit($image, $caption, $title, $alt);
                $images[] = $image;
            }
        }

        return $images;
    }

}
