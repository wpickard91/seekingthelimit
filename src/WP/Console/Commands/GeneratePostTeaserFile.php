<?php

namespace WP\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpFoundation\File\File;
use WP\Categories\Entities\Category;
use WP\Categories\Interfaces\CategoryRepositoryInterface;
use WP\Images\Entities\Image;
use WP\Images\Entities\Thumbnail;
use WP\Images\Interfaces\ImageFactoryInterface;
use WP\Images\Interfaces\ImageRepositoryInterface;
use WP\Images\Interfaces\ThumbnailFactoryInterface;
use WP\Images\ThumbnailFactory;
use WP\Posts\Entities\Post;
use WP\Posts\Interfaces\PostFactoryInterface;
use WP\Posts\Interfaces\PostRepositoryInterface;
use WP\Posts\PostRepository;
use WP\Support\Generators\LatinTextGenerator;

class GeneratePostTeaserFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'posts:generate:teasers {destination?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a json file of data needed for the post teaser component';

    /**
     * @var PostRepositoryInterface
     */
    protected $postRepository;

    protected $arguments = [
        ['destination', InputArgument::OPTIONAL, 'The absolute path of the destination file', ''],
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        PostRepositoryInterface $postRepository
    ) {
        parent::__construct();

        $this->defaultDestination = base_path() . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'postTeasers.json';
        $this->postRepository = $postRepository;
    }

    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $teasers = $this->createTeaserList();
        $destination = $this->argument('destination') ? $this->argument('destination') : $this->defaultDestination;
        $this->info('Destination: ' . $destination);

        file_put_contents($destination, json_encode($teasers));
        $this->info('Done');
    }

    public function createTeaserList()
    {
        $posts = $this->postRepository->all();
        $teasers = [];

        /** @var Post $post */
        foreach ($posts as $post) {
            $teasers[] = [
                'id' => $post->id,
                'title' => $post->title,
                'description' => $post->description,
                'main_image' => $post->mainImage,
                'created_at' => $post->created_at->toDateString(),
                'updated_at' => $post->updated_at->toDateString(),
                'category' => $post->category,
                'keyword_references' => $post->keywordReferences
            ];
        }

        return $teasers;
    }
}
