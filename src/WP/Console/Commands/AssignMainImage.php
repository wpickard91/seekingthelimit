<?php

namespace WP\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpFoundation\File\File;
use WP\Categories\Entities\Category;
use WP\Categories\Interfaces\CategoryRepositoryInterface;
use WP\Images\Entities\Image;
use WP\Images\Interfaces\ImageFactoryInterface;
use WP\Posts\Entities\Post;
use WP\Posts\Interfaces\PostFactoryInterface;
use WP\Posts\Interfaces\PostRepositoryInterface;
use WP\Support\Generators\LatinTextGenerator;

class AssignMainImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'posts:images:assignMain {postId}  ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign main images to posts which do not have them';

    /**
     * @var postId to work for, can also be '*'
     */
    protected $postId = 0;

    /**
     * @var PostFactoryInterface
     */
    protected $postFactory;

    /**
     * @var ImageFactoryInterface
     */
    protected $imageFactory;

    /**
     * @var PostRepositoryInterface
     */
    protected $postRepository;

    protected $arguments = [
        ['postId', InputArgument::REQUIRED, 'postId or *', 0]
    ];
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        PostFactoryInterface $postFactory,
        ImageFactoryInterface $imageFactory,
        PostRepositoryInterface $postRepository
    ) {

        parent::__construct();

        $this->postRepository = $postRepository;
        $this->postFactory = $postFactory;
        $this->imageFactory = $imageFactory;
    }

    public function getArguments()
    {
        $this->arguments;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $postId = $this->argument('postId');
        $posts = new Collection();

        if ($postId == '*') {
            $posts = $this->postRepository->all();
        } else {
            $posts->push($this->postRepository->find($postId));;
        }

        $this->info('Found ' . count($posts) . ' posts');

        /**
         * @var Post $post
         */
        foreach ($posts as $post) {
            if ($post->main_image_id == null) {
                $image = $this->selectRandomImageFromPost($post);
                if ($image) {
                    $this->postFactory->setMainImage($post, $image);
                }
            }
        }

        $this->info('Done');
    }

    public function selectRandomImageFromPost(Post $post)
    {
        $images = new Collection();

        /**
         * @var Image $image
         */
        foreach ($post->images as $image) {
            if ($image->thumbnail) {
                $this->info("Using thumbnail...");
                $images->push($image->thumbnail->image);
            }
        }

        $this->info(count($images) . " possible thumbnails");

        if (count($images) <= 0) {
            $this->info("Using image");
            $images = $post->images;
        }

        if (count($images) <= 0) {
            $this->info("^ignore, cannot find image");
            return null;
        }

        return $images[rand(0, count($images) - 1)];
    }
}
