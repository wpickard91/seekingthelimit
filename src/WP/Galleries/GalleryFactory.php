<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Galleries;

use Illuminate\Support\Collection;
use WP\Galleries\Entities\Gallery;
use WP\Galleries\Entities\GalleryImage;
use WP\Galleries\Interfaces\GalleryFactoryInterface;
use WP\Images\Interfaces\ImageFactoryInterface;
use WP\Images\Interfaces\ImageRepositoryInterface;
use WP\Keywords\Interfaces\KeywordFactoryInterface;
use WP\Images\Entities\Image;

class GalleryFactory implements GalleryFactoryInterface
{
    /*
     * @var ImageFactoryInterface
     */
    protected $imageFactory;

    /**
     * @var ImageRepositoryInterface
     */
    protected $imageRepository;

    /**
     * @var KeywordFactoryInterface
     */
    protected $keywordFactory;

    public function __construct(
        ImageFactoryInterface $imageFactory
    ) {
        $this->imageFactory = $imageFactory;
    }

    public function createAssetDirectory(Gallery $gallery) {
        $images_dir = public_path() . '/images';
        if (!file_exists($images_dir)) {
            if(mkdir($images_dir)) {
                $this->createAssetDirectory($gallery);
            } else {
                return false;
            }
        }

        $galleries_dir =  $images_dir . '/galleries';
        if (!file_exists($galleries_dir)) {
            if(mkdir($galleries_dir)) {
                $this->createAssetDirectory($gallery);
            } else {
                return false;
            }
        }

        $this_gallery_dir = $galleries_dir . '/' . $gallery->id;
        if (!file_exists($this_gallery_dir)) {
            if(mkdir($this_gallery_dir)) {
                return true;
            } else {
                return false;
            }
        }

        return true;
    }

    public function create($title, $description, $images, $user_id)
    {
        $gallery = Gallery::create([
            'title' => $title,
            'description' => $description,
            'user_id' => $user_id
        ]);

        foreach ($images as $image) {
            $this->addImage($gallery, $image);
        }

        return $gallery;
    }

    public function edit(Gallery $gallery, $title, $description, $main_image_id = null, $published = false, array $images = []){
        $gallery->title =  $title;
        $gallery->description = $description;
        $gallery->main_image_id = $main_image_id;
        $gallery->published = $published;

        $gallery->save();

        foreach($images as $image) {
            $this->addImage($gallery, $image);
        }

        return $gallery;
    }

    public function setMainImage(Gallery $gallery, Image $image) {
        return $this->edit($gallery, $gallery->title, $gallery->description, $image->id, $gallery->published, []);
    }

    public function addImage(Gallery $gallery, Image $image)
    {
        if ($this->createAssetDirectory($gallery)) {
            $this->imageFactory->move($image, public_path() . '/images/galleries/' . $gallery->id);
        }

        return GalleryImage::create([
            'image_id' => $image->id,
            'gallery_id' => $gallery->id
        ]);
    }

    public function deleteImageReferences(Collection $galleryImages) {
        foreach($galleryImages as $galleryImage) {
            $galleryImage->delete();
        }

        return count($galleryImages);
    }


    /*  public function assignImagesToPost(array $images, Post $gallery)
      {
          $galleryImages = new Collection();
          foreach($images as $image) {
              $galleryImages->add($this->imageFactory->addToPost($image, $gallery));
          }
          return $galleryImages;
      }

      public function setMainImage(Post $gallery, Image $image)
      {
          $gallery->main_image_id = $image->id;
          $gallery->save();
          return $gallery;
      }

      public function edit(Post $gallery, $title, $description, $body)
      {
          $gallery->title = $title;
          $gallery->description = $description;
          $gallery->body = $body;
          $gallery->save();
          return $gallery;
      }

      public function removeImage(Post $gallery, Image $image)
      {
          /**
           * @var PostImage
           *
          $galleryImage = $this->imageRepository->findPostImage($gallery->id, $image->id);
          $galleryImage->delete();
      }

      public function assignPostToCategory(Post $gallery, Category $category)
      {
          $gallery->category_id = $category->id;
          $gallery->save();
      }

      public function addKeyword(Post $gallery, Keyword $keyword)
      {
          $this->keywordFactory->createReference($keyword, $gallery);
      }*/
}