<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Galleries;

use WP\Galleries\Controllers\GalleryApiController;
use WP\Galleries\Controllers\GalleryController;
use WP\Http\Interfaces\RouteMapper;
use Illuminate\Routing\Router;

class GalleryRouteDirectory implements RouteMapper
{
    public function map (Router $router)
    {
        $router->group(
            ['prefix' => 'galleries'],
            function (Router $router) {
                $router->get('/', GalleryController::class . '@getIndex');
                $router->get('/{id}', GalleryController::class . '@getGallery');
            }
        );

        $router->group(
            ['prefix' => 'admin/galleries'],
            function(Router $router) {
                $router->get('/', GalleryController::class . '@getEditIndex');
                $router->get('/create', GalleryController::class . '@getCreate');
                $router->post('/create', GalleryController::class . '@postCreate');
                $router->get('/edit/{id}', GalleryController::class . '@getEdit');
                $router->post('/edit/{id}', GalleryController::class . '@postEdit');
                $router->post('/delete/{id}', GalleryController::class . '@postDelete');
            }
        );

        $router->group(
            ['prefix' => 'admin/galleries/api/'],
            function(Router $router) {
                $router->post('/edit/{id}', GalleryApiController::class . '@postEdit');
            }
        );
        /*$router->group(
            ['prefix' => '/galleries'],
            function (Router $router) {
                $this->mapPublicRoutes($router);
            }
        );

        $router->group(
            [
                'prefix' => '/admin/galleries',
            //    'middleware' => ['auth','admin']
            ],
            function (Router $router) {
                $this->mapAdminRoutes($router);
            }
        );

        $router->group(
            ['prefix' => '/galleries'],
            function (Router $router) {
                $this->mapApiRoutes($router);
            }
        );

        $router->group(
            ['prefix' => '/admin/stories'],
            function(Router $router) {
                $this->mapStoryRoutes($router);
            }
        );*/
    }

    protected function mapPublicRoutes (Router $router)
    {
        $router->get('/', [
            'as' => 'galleries.index',
            'uses' => PostController::class . '@getIndex'
        ]);

        $router->get('/{id}', [
            'uses' => PostController::class . '@getPost'
        ])->where('id', '[0-9]+');
    }

    protected function mapAdminRoutes (Router $router)
    {
        $router->get('/create', [
            'as' => 'galleries.create',
            'uses' => PostController::class .'@getCreate'
        ]);

        $router->gallery('/create', [
            'as' => 'galleries.galleryCreate',
            'uses' => PostController::class . '@galleryCreate'
        ]);

        $router->get('/edit/{id}', [
            'as' => 'galleries.edit',
            'uses' => PostController::class . '@getEdit'
        ]);

        $router->gallery('/edit/{id}', [
            'as' => 'galleries.galleryEdit',
            'uses' => PostController::class . '@galleryEdit'
        ]);

        $router->gallery('/delete/{id}', [
            'as' => 'galleries.delete',
            'uses' => PostController::class . '@delete'
        ]);
    }

    protected function mapApiRoutes (Router $router)
    {
        $router->group([
            'prefix' => '/galleriesApi/v0'
        ], function (Router $router) {
            $router->get('/', PostApiController::class . '@paginate');
            $router->get('/gallery/{galleryId}', PostApiController::class . '@getSingle')
                ->where('galleryId', '[0-9]+');

            $router->get('/count', PostApiController::class . '@getCount');
            $router->get('/invertedIndex', PostApiController::class . '@getInvertedIndex');
            $router->get('/galleryTeasers', PostApiController::class . '@getPostTeasers');

            $router->gallery('/create', PostApiController::class . '@galleryCreate');

            $router->put('/gallery/{gallery_id}', PostApiController::class . '@edit')
                ->where('gallery_id', '[0-9]+');

            $router->get('/gallery/{gallery_id}/images', ImageApiController::class . '@getImagesForPost')
                ->where('gallery_id', '[0-9]+');
            $router->get('/gallery/{gallery_id}/thumbnails', ImageApiController::class . '@getThumbnailsForPost')
                ->where('gallery_id', '[0-9]+');

            $router->put('/gallery/{gallery_id}/images', [
                'uses' => PostApiController::class . '@putImages'
            ])->where('gallery_id', '[0-9]+');

            $router->put('/gallery/{gallery_id}/images/main/{image_id}', [
                'uses' => PostApiController::class . '@putMainImage'
            ])->where('gallery_id', '[0-9]+')
                ->where('image_id', '[0-9]+');

            $router->delete('/gallery/{gallery_id}/images/{image_id}', PostApiController::class . '@deleteImage')
                ->where('gallery_id', '[0-9]+')
                ->where('image_id', '[0-9]+');

            $router->put('/gallery/{gallery_id}/category', [
                'uses' => PostApiController::class . '@putCategory'
            ])->where('gallery_id', '[0-9]+');

            $router->put('/gallery/{gallery_id}/keywords', PostApiController::class . '@putKeywords')
                ->where('gallery_id', '[0-9]+');
        });
    }

    public function mapStoryRoutes(Router $router)
    {
        $router->get('/create', PostController::class . '@getCreate');
    }
}