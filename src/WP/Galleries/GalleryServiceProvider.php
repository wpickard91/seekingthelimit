<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Galleries;

use Illuminate\Support\ServiceProvider;
use WP\Galleries\Interfaces\GalleryRepositoryInterface;
use WP\Galleries\Interfaces\GalleryFactoryInterface;


class GalleryServiceProvider extends ServiceProvider
{

    public function boot()
    {
        /*Post::saved(function (Post $gallery) {
            Artisan::call('gallerys:generate:index');
            Artisan::call('gallerys:generate:teasers');
            Artisan::call('gallerys:generate:thumbnails', [
                'width' => 300,
                'galleryId' => $gallery->id
            ]);
        });*/
    }

    public function register()
    {
        $this->app->bind(
            GalleryRepositoryInterface::class,
            GalleryRepository::class
        );

        $this->app->bind(
            GalleryFactoryInterface::class,
            GalleryFactory::class
        );
    }
}