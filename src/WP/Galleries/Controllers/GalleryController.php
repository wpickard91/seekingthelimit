<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Galleries\Controllers;

use Illuminate\Auth\Guard;
use Illuminate\Database\Eloquent\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use WP\Common\LaravelUtil;
use WP\Http\Controllers\BaseController;
use WP\Images\Interfaces\ImageFactoryInterface;
use WP\Galleries\Interfaces\GalleryFactoryInterface;
use WP\Galleries\Entities\Gallery;
use WP\Galleries\Interfaces\GalleryRepositoryInterface;
use Illuminate\Http\Request;
use WP\Images\Interfaces\ImageRepositoryInterface;
use WP\Images\Interfaces\ThumbnailFactoryInterface;

class GalleryController extends BaseController
{
    /**
     * @var  GalleryRepositoryInterface
     */
    protected $galleryRepository;

    /**
     * @var GalleryFactoryInterface
     */
    protected $galleryFactory;

    /**
     * @var ImageFactoryInterface
     */
    protected $imageFactory;

    /**
     * @var ThumbnailFactoryInterface
     */
    protected $thumbnailFactory;

    /**
     * @var ImageRepositoryInterface
     */
    protected $imageRepository;

    /**
     * @var Guard
     */
    protected $guard;

    public function __construct (
        ImageFactoryInterface $imageFactory,
        ThumbnailFactoryInterface $thumbnailFactoryInterface,
        ImageRepositoryInterface $imageRepo,
        GalleryRepositoryInterface $galleryRepo,
        GalleryFactoryInterface $galleryFactory,
        Guard $guard
    ) {
        $this->imageFactory = $imageFactory;
        $this->thumbnailFactory = $thumbnailFactoryInterface;
        $this->imageRepository = $imageRepo;
        $this->galleryRepository = $galleryRepo;
        $this->galleryFactory = $galleryFactory;
        $this->guard = $guard;
    }

    /**
     * return the page showing the galleries
     */
    public function getIndex()
    {
        $galleries = $this->galleryRepository->all()
            ->sortByDesc('updated_at')
            ->groupBy(function($entity) {
                return $entity->updated_at->format('F, Y');
            });
        //dd($galleries);
       // $grouped_galleries = LaravelUtil::groupEntitiesByDate($galleries);

    //    $galleries->load('mainImage');

        return view('galleries.index',['grouped_galleries' => $galleries]);
    }

    public function getEditIndex(Request $request) {
        $galleries = $this->galleryRepository->all();

        return view('galleries.editIndex', compact('galleries'));
    }

    public function getGallery(Request $request, $galleryId){
        $gallery = $this->galleryRepository->find($galleryId);
        $isMobile = $request->session()->get('isMobile', false);
        //$gallery->load('mainImage', 'images');

        return view('galleries.gallery', compact('gallery', 'isMobile'));
    }

    /**
     * Show the form for creating a new gallery
     */
    public function getCreate()
    {
        return view('galleries.create');
    }

    public function getEdit(Request $request, $gallery_id)
    {
        $gallery = $this->galleryRepository->find($gallery_id);
        $gallery->load('images');
        return view('galleries.edit')->with('gallery', $gallery);
    }

    /**
     * @param Gallery $gallery The gallery to make the route for
     * @return string
     */
    private function makeEditRoute(Gallery $gallery) {
        return '/admin/galleries/edit/' . $gallery->id;
    }

    public function handleUploadImagesRequest(Request $request){
        $uploadedImages = $request->hasFile('images')
            ? gettype($request->file('images')) === 'array' ? $request->file('images') : [$request->file('images')]
            : [];

        return $this->handleImages($uploadedImages);
    }

    /**
     * @param $images  The array of uploaded images to handle
     * @return array
     */
    public function handleImages(array $images) {
        return array_map(function(UploadedFile $image) {
            $filename = $image->getClientOriginalName();
            return $this->imageFactory->create($image, $filename);
        }, $images);
    }

    /**
     * Captioned images like:
     *  [caption-{image_id} => {caption}]
     *  [alt-...]
     *  [title-...]
     *
     * @param Request $request
     * @return array
     */
    public function handleCaptionedImagesRequest(Request $request) {
        $captions = LaravelUtil::inputWithKeysLike($request, 'caption=');
        $alts = LaravelUtil::inputWithKeysLike($request, 'alt=');
        $titles = LaravelUtil::inputWithKeysLike($request, 'title=');
        $merged = array_merge($captions, $alts, $titles);

        $groupedById = [];

        // combine the three arrays into one associative array with image ids for keys
        // where the corresponding values are associative arrays of field name to desired value
        //  like:
        //  [17 => ['caption' => 'input', 'alt => 'inputalt', 'title' => 'title']...
        foreach($merged as $key => $value) {
            $parts = explode("=", $key);
            $field = $parts[0];
            $image_id = intval($parts[1]);

            if (!isset($groupedById[$image_id])) {
                $groupedById[$image_id] = [
                    'id' => $image_id
                ];
            }

            $groupedById[$image_id][$field] = $value;
        }

        // now update the images and save
        $images_and_nulls = array_map(function($image_data) {
            $image_id = $image_data['id'];
            $image = $this->imageRepository->find($image_id);
            if ($image) {
                $caption = isset($image_data['caption']) ? $image_data['caption'] : $image->caption;
                $alt = isset($image_data['alt']) ? $image_data['alt'] : $image->alt;
                $title = isset($image_data['title']) ? $image_data['title'] : $image->title;

                return $this->imageFactory->edit($image, $caption, $title, $alt);
            } else {
                return null;
            }
        }, $groupedById);

        return array_filter($images_and_nulls, function($image) {
            return $image !== null;
        });
    }

    public function handleDeleteImagesRequest(Request $request, Gallery $gallery) {
        $input = $request->input();
        $delete_image_keys = array_filter(array_keys($input), function($key) {
            return strpos($key, 'delete_image') !== false;
        });

        $delete_images = array_intersect_key($input, array_flip($delete_image_keys));

        $image_ids_to_delete = array_map(function($key) {
            $key_split = explode("=", $key);
            $image_id_string = end($key_split);
            return intval($image_id_string);
        }, array_keys($delete_images));

        $gallery_images_to_delete = $gallery->galleryImages->filter(function($galleryImage) use ($image_ids_to_delete) {
            return array_search($galleryImage->image_id, $image_ids_to_delete) !== false;
        });

        $images_to_delete = array_map(function($image_id) {
            return $this->imageRepository->find($image_id);
        }, $image_ids_to_delete);

        $referencesDeleted = $this->galleryFactory->deleteImageReferences($gallery_images_to_delete);
        foreach ($images_to_delete as $image) {
            if ($image->id !== $gallery->main_image_id) {
                $this->imageFactory->deleteImage($image);
            }
        }

        return $referencesDeleted;
    }

    /**
     * Create a new gallery
     * @param Request $request
     */
    public function postCreate(Request $request)
    {
        // We will trust that the middleware is ensuring that only
        // priveleged users are hitting
        // @var Request
       // $request = app('request');
        $title = $request->input('title');
        $description = $request->input('description');
        // If the uploaded files are not a list, then put them in one

        $images = $this->handleUploadImagesRequest($request);

        foreach ($images as $image) {
            $this->thumbnailFactory->create($image, 800);
        }

        $gallery = $this->galleryFactory->create(
            $title,
            $description,
            $images,
            $this->guard->user()->id
        );

        return redirect($this->makeEditRoute($gallery))->with('gallery', $gallery);
    }

    /**
     * @param Request $request
     * @param $gallery_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit(Request $request, $gallery_id) {
        /**
         * @var Gallery
         */
        $gallery = $this->galleryRepository->find($gallery_id);

        if ($gallery && !empty($request->input())) {
            $number_image_deleted = $this->handleDeleteImagesRequest($request, $gallery);
            $new_images = $this->handleUploadImagesRequest($request);
            $captioned_images = $this->handleCaptionedImagesRequest($request);

            foreach ($new_images as $image) {
                $this->thumbnailFactory->create($image, 800);
            }

            $title = $request->input('title');
            $description = $request->input('description');
            $main_image_id = $request->input('main_image_id');
            $published = $request->has('published');

            /**
             * @var Gallery
             */
            $editedGallery = $this->galleryFactory->edit($gallery, $title, $description, $main_image_id, $published, $new_images);

            foreach($editedGallery->images as $image) {
                if (str_contains($image->thumbnail->image->absolute_path, 'tmp')) {
                    $this->imageFactory->move($image->thumbnail->image,  public_path() . '/images/galleries/' . $gallery->id);
                }
            }


            return redirect($this->makeEditRoute($gallery))->with('gallery', $gallery);
        } else {
            return redirect()->back();
        }
    }

    public function postDelete(Request $request, $gallery_id) {

        return redirect('/admin/galleries');
    }
}