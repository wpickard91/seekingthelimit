<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Galleries\Controllers;

use Illuminate\Auth\Guard;
use Illuminate\Database\Eloquent\Collection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use WP\Common\LaravelUtil;
use WP\Http\Controllers\BaseApiController;
use WP\Http\Controllers\BaseController;
use WP\Images\Interfaces\ImageFactoryInterface;
use WP\Galleries\Interfaces\GalleryFactoryInterface;
use WP\Galleries\Entities\Gallery;
use WP\Galleries\Interfaces\GalleryRepositoryInterface;
use Illuminate\Http\Request;
use WP\Images\Interfaces\ImageRepositoryInterface;
use WP\Images\Interfaces\ThumbnailFactoryInterface;

class GalleryApiController extends BaseApiController
{
    /**
     * @var  GalleryRepositoryInterface
     */
    protected $galleryRepository;

    /**
     * @var GalleryFactoryInterface
     */
    protected $galleryFactory;

    /**
     * @var ImageFactoryInterface
     */
    protected $imageFactory;

    /**
     * @var ThumbnailFactoryInterface
     */
    protected $thumbnailFactory;

    /**
     * @var ImageRepositoryInterface
     */
    protected $imageRepository;

    /**
     * @var Guard
     */
    protected $guard;

    /**
     * @var GalleryController
     */
    protected $galleryController;

    protected $singular = 'gallery';

    protected $plural = 'galleries';

    public function __construct (
        ImageFactoryInterface $imageFactory,
        ThumbnailFactoryInterface $thumbnailFactoryInterface,
        ImageRepositoryInterface $imageRepo,
        GalleryController $galleryController,
        GalleryRepositoryInterface $galleryRepo,
        GalleryFactoryInterface $galleryFactory,
        Guard $guard
    ) {
        $this->imageFactory = $imageFactory;
        $this->thumbnailFactory = $thumbnailFactoryInterface;
        $this->imageRepository = $imageRepo;
        $this->galleryController = $galleryController;
        $this->galleryRepository = $galleryRepo;
        $this->galleryFactory = $galleryFactory;
        $this->guard = $guard;
    }

   public function postEdit(Request $request, $gallery_id) {
       $this->galleryController->postEdit($request, $gallery_id);
       return $this->single($this->galleryRepository->find($gallery_id)->load('images'));
   }
}