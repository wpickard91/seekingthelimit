<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Galleries\Interfaces;

use Illuminate\Support\Collection;
use WP\Categories\Entities\Category;
use WP\Galleries\Entities\Gallery;
use WP\Keywords\Entities\Keyword;
use WP\Galleries\Entities\Post;
use WP\Images\Entities\Image;
/**
 * Interface PostFactoryInterface
 *
 * Define class for creating/manipulating gallerys
 *
 * @package WP\Galleries\Interfaces
 */
interface GalleryFactoryInterface
{
    public function create($title, $description, $images, $user_id);

    public function deleteImageReferences(Collection $galleryImages);

    public function setMainImage(Gallery $gallery, Image $image);

    public function edit(Gallery $gallery, $title, $description, $main_image_id = null, $published = false, array $images = []);

  /*  public function edit(Post $gallery, $title, $description, $body);

    public function assignImagesToPost(array $images, Post $gallery);

    public function setMainImage(Post $gallery, Image $image);

    public function removeImage(Post $gallery, Image $image);

    public function assignPostToCategory(Post $gallery, Category $category);

    public function addKeyword(Post $gallery, Keyword $keyword);*/
}