<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Galleries\Interfaces;

use Illuminate\Database\Eloquent\Collection;

interface GalleryRepositoryInterface
{
    public function all();

    /**
     * @param Integer $id
     * @return mixed
     */
    public function find($id);

    /**
     * @param Integer $count
     * @return mixed
     */
    public function recent($count, $with = []);

    /**
     * @return Collection
     */
    public function paginate($take, $start, $end);
}