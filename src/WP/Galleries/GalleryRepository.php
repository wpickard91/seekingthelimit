<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Galleries;

use Illuminate\Database\Eloquent\Collection;
use WP\Galleries\Interfaces\GalleryRepositoryInterface;
use WP\Galleries\Entities\Gallery;

class GalleryRepository implements GalleryRepositoryInterface
{
    public function find($id)
    {
        return Gallery::find($id);
    }

    public function all()
    {
        /*return Post::with('images')
            ->get();*/

        return Gallery::all();
    }

    public function recent($count, $with = [])
    {
        return Gallery::select('*')
            ->where(['published' => true])
            ->with($with)
            ->orderBy('created_at', 'desc')
            ->limit($count)
            ->get();
    }

    /**
     * @param int $take
     * @param int $start
     * @param int $end
     * @return Collection
     */
    public function paginate($take=10, $start=0, $end=10)
    {
        if ($take <= 0) {
            return Collection::make([]);
        }

        return Gallery::query()
            ->skip($start)
            ->limit($take)
            ->get();
    }
}