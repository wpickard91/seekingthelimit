<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Galleries\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
//use WP\Categories\Entities\Category;
use Illuminate\Support\Collection;
use WP\Images\Entities\Image;
use WP\Users\Entities\User;

/*use WP\Images\Entities\PostImage;
use WP\Images\Entities\Thumbnail;
use WP\Keywords\Entities\Keyword;
use WP\Keywords\Entities\KeywordReference;*/

/**
 * Class Post
 * @package WP\Galleries
 *
 * @property String $title
 * @property String $description
 * @property Integer $id
 * @property Integer $main_image_id
 * @property Integer user_id
 * @property Boolean $published
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Gallery extends Model
{

    protected $table = 'galleries';

    protected $fillable = ['title', 'description', 'user_id', 'published'];

 /*   public function images()
    {
        return $this->belongsToMany(Image::class, 'galleryImages', 'gallery_id', 'image_id');
    }*/

    public function mainImage()
    {
        return $this->hasOne(Image::class, 'id', 'main_image_id');
    }

    /**
     * @return Collection
     */
    public function galleryImages() {
        return $this->hasMany(GalleryImage::class);
    }

    public function images() {
        //return $this->belongsToMany(Image::class, 'gallery_images', 'gallery_id', 'id');
       // return $this->hasManyThrough(Image::class, GalleryImage::class, 'image_id   ', '2', '3');
       /* $images = $this->
        return $this->galleryImages();*/
        return $this->belongsToMany(Image::class, 'gallery_images', 'gallery_id', 'image_id');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

/*
    public function keywordReferences()
    {
        return $this->hasMany(KeywordReference::class);
    }

    public function keywords()
    {
        return $this->belongsToMany(Keyword::class, 'keywordReferences', 'gallery_id', 'keyword_id');
    }*/

/*    public function category()
    {
        return $this->belongsTo(Category::class);
    }*/
}