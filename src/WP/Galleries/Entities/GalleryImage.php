<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Galleries\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use WP\Images\Entities\Image;

/**
 * Class GalleryImage
 * @package WP\Galleries\Entities
 *
 * @property Integer $id
 * @property Integer $gallery_id
 * @property Integer $image_id
 * @property Carbon $created_at
 * @property Carbon $update_at
 */
class GalleryImage extends Model
{
    protected $table = 'gallery_images';

    protected $fillable = ['gallery_id', 'image_id'];

    public function gallery() {
        return $this->hasOne(Gallery::class, 'id', 'gallery_id');
    }

    public function image() {
        return $this->hasOne(Image::class, 'id', 'image_id');
    }
}