<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/

namespace WP\Http\Interfaces;

use Illuminate\Routing\Router;

/**
 * Interface RouteMapper
 *
 * An object capable of mapping routes to a controller or handler
 * @package WP\Http
 */
interface RouteMapper
{
    /**
     * Add route definitions
     *
     * @param Illuminate\Routing\Router
     */
    public function map(Router $router);
}