<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Http\Controllers;

use Illuminate\Auth\AuthManager;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;

class AdminController extends BaseController
{
    /**
     * @var AuthManager
     */
    protected $authManager;

    /**
     * @var Guard
     */
    protected $auth;

    public function __construct(AuthManager $authController, Guard $guard) {
        $this->authManager = $authController;
        $this->auth = $guard;
    }

    public function getHome(Guard $auth) {
        if ($auth->guest()) {
            return redirect()->guest("admin/login");
        }

        return view('front.admin.home');
    }

    public function getLogin() {
        return view('front.admin.login');
    }

    public function postLogin(Request $request) {
        $allowed = ['username', 'password'];
        $inputSansCsrf = array_intersect_key($request->input(), array_flip($allowed));

        if ($this->auth->attempt($inputSansCsrf)) {
            if ($this->auth->user()->is_admin) {
                return redirect('/admin');
            } else {
                return redirect('/home')->with('message', 'Successfully logged in');
            }
        } else {
            return redirect('/login')->with('message', 'Wrong username/password');
        }
    }
}