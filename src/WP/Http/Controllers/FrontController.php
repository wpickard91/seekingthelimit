<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Http\Controllers;

use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use WP\Categories\Interfaces\CategoryRepositoryInterface;
use WP\Galleries\Interfaces\GalleryRepositoryInterface;
use WP\Posts\Interfaces\PostRepositoryInterface;

/**
 * Class FrontController
 *
 * This class handles basic top level routes
 *
 * @package WP\Http\Controllers
 */
class FrontController extends BaseController
{
    /**
     * @var PostRepositoryInterface
     */
    protected $postRepository;

    /**
     * @var GalleryRepositoryInterface
     */
    protected $galleryRepository;

    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * @var Guard
     */
    protected $guard;

    public function __construct(
        PostRepositoryInterface $postRepository,
        GalleryRepositoryInterface $galleryRepository,
        CategoryRepositoryInterface $categoryRepo,
        Guard $guard
    ) {
        $this->postRepository = $postRepository;
        $this->galleryRepository = $galleryRepository;
        $this->categoryRepository = $categoryRepo;
        $this->guard = $guard;
    }

    public function getHome(Request $request)
    {
        $recent_posts = $this->postRepository->recent(4);
        $recent_galleries = $this->galleryRepository->recent(4);
        $categories = $this->categoryRepository->all(['posts']);
        $user = $this->guard->user();
        $isMobile = $request->session()->get('isMobile', true);

        return view('front.home')->with(compact('recent_posts', 'recent_galleries', 'categories', 'user', 'isMobile'));
    }

    public function getArticles(Request $request) {
        return view("front.articles", ['is_mobile' => $request->get('isMobile', false)]);
    }

    public function getGalleries(Request $request) {
        return view("front.galleries", ['is_mobile' => $request->get('isMobile', false)]);
    }

    public function getLogin(Request $request) {
        return view('front.login', ['is_mobile' => $request->get('isMobile', false)]);
    }

    public function getAbout(Request $request) {
        return view('layouts.master', ['is_mobile' => $request->get('isMobile', false)]);
    }
}