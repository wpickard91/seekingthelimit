<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Support\Arrayable;
use WP\Http\Responses\ApiResponse;

class BaseApiController extends BaseController
{
    /**
     * @var Request
     */
    protected $request;

    protected $take = 10;

    protected $plural = 'objects';

    protected $singular = 'object';


    public function __construct(
        Request $request
    ) {
        $this->request = $request;
    }

    public function getHowMany()
    {
        if ($this->request->has('take')) {
            return $this->request->input('take');
        }

        return $this->take;
    }

    public function single($instance) {
        $response = new ApiResponse();

        $response->setProperty(
            $this->singular,
            $instance
        );

        return $response;
    }

    public function collection($items)
    {
        $response = new ApiResponse();

        if ($items instanceof Collection) {
            $response->setProperty(
                $this->plural,
                $items->toArray()
            );
        }

        return $response;
    }
}