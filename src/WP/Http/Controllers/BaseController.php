<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Validation\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * Class BaseController
 *
 * Define the base controller all other controllers in WP
 * inherit from
 *
 * @package WP\Http\Controllers
 */
class BaseController extends Controller
{
    use ValidatesRequests;
}