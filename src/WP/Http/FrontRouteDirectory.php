<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Http;

use WP\Http\Controllers\FrontController;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Redirect;
use WP\Http\Controllers\AdminController;
use WP\Http\Interfaces\RouteMapper;
use WP\Users\Controllers\UserController;

class FrontRouteDirectory implements RouteMapper
{
    public function map(Router $router)
    {
        
        $router->get('/', function() {
            return redirect("home");
        });
        $router->get('/galleries', FrontController::class . '@getGalleries');
        $router->get('/articles', FrontController::class . '@getArticles');
        $router->get('/home', FrontController::class . '@getHome');
        $router->get('/login', FrontController::class . '@getLogin');
        $router->any('/logout', UserController::class . '@logout');
        $router->get('/about', FrontController::class . '@getAbout');

        $router->get('/test', function(Router $router) { return view('test'); });

        $this->mapPostRoutes($router);
        $this->mapAdminRoutes($router);
    }

    public function mapPostRoutes(Router $router) {
        $router->post('/login', AdminController::class . '@postLogin');
    }

    public function mapAdminRoutes(Router $router) {
        $router->group(
            [
                'prefix' => '/admin',
                'middleware' => 'admin'
            ],
            function(Router $router) {
                $router->get('/', AdminController::class . '@getHome');
            }
        );
    }
}