<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Http\Responses;

use Illuminate\Http\Response;

class ApiResponse extends Response
{
    /**
     * @var array
     */
    protected $properties;

    public function __construct($status = 200, $headers = [])
    {
        parent::__construct([], $status, $headers);

        $this->properties = [];
    }

    public function getStatusMessage()
    {
        if ($this->statusCode === self::HTTP_OK) {
            return 'success';
        }

        return 'error';
    }

    /**
     * Render the content of the response into an array
     */
    public function render()
    {
        $responseData = [
            'code' => $this->statusCode,
            'status' => $this->getStatusMessage()
        ];

        foreach ($this->properties as $key => $value) {
            $responseData[$key] = $value;
        }

        return $responseData;
    }

    protected function internalUpdate()
    {
        $this->setContent($this->render());
    }

    public function setProperty($name, $content)
    {
        $this->properties[$name] = $content;
        $this->internalUpdate();
        return $this;
    }
}