<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Http;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use WP\Posts\PostRouteDirectory;
use WP\Users\UserRouteDirectory;
use WP\Categories\CategoryRouteDirectory;
use WP\Images\ImageRouteDirectory;
use WP\Keywords\KeywordsRouteDirectory;
use WP\Contact\ContactRequestRouteDirectory;
use WP\Galleries\GalleryRouteDirectory;
use WP\GearReviews\GearRouteDirectory;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * List of route mappers
     *
     * @var array
     */
    protected $mappers = [
        FrontRouteDirectory::class,
        PostRouteDirectory::class,
        GalleryRouteDirectory::class,
        UserRouteDirectory::class,
        CategoryRouteDirectory::class,
        ImageRouteDirectory::class,
        KeywordsRouteDirectory::class,
        ContactRequestRouteDirectory::class,
        GearRouteDirectory::class
    ];

    public function register()
    {
        /** @var Router $router */
        $router = $this->app['router'];

        foreach ($this->mappers as $mapper) {
            app($mapper)->map($router);
        }
    }
}