<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Users;

use WP\Users\Interfaces\UserRepositoryInterface;
use WP\Users\Entities\User;

class UserRepository implements UserRepositoryInterface
{
    public function find($id)
    {
        return User::find($id);
    }

    public function withEmail($email)
    {
        return User::where('email', $email)->first();
    }

    public function all() {
        return User::all();
    }
}