<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Users;

use WP\Users\Interfaces\UserFactoryInterface;
use WP\Users\Entities\User;

class UserFactory implements UserFactoryInterface
{
    public function create($username, $password, $is_admin = false) {
        return User::create(compact('username', 'password', 'is_admin'));
    }

    public function edit(User $user, $first_name, $last_name, $bio, $current_location, $image_id) {
        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->bio = $bio;
        $user->current_location = $current_location;
        $user->image_id = $image_id;
        $user->save();
        return $user;
    }

}