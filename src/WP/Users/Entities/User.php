<?php

namespace WP\Users\Entities;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use WP\Posts\Entities\Post;
use WP\Images\Entities\Image;

/**
 * Class User
 * @package WP\Users\Entities
 *
 * @property String $username
 * @property String $password
 * @property Boolean $is_admin
 * @property Integer $image_id
 * @property String $bio
 * @property String $first_name
 * @property String $last_name
 * @property String $current_location
 */
class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username', 'email', 'password', 'is_admin', 'image_id', 'bio', 'first_name', 'last_name', 'current_location'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function posts (){
        return $this->hasMany(Post::class);
    }

    public function image() {
        return $this->belongsTo(Image::class);
    }
}
