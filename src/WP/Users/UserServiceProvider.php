<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Users;

use Illuminate\Support\ServiceProvider;
use WP\Users\Interfaces\UserFactoryInterface;
use WP\Users\Interfaces\UserRepositoryInterface;

class UserServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            UserFactoryInterface::class,
            UserFactory::class
        );

        $this->app->bind(
            UserRepositoryInterface::class,
            UserRepository::class
        );
    }
}