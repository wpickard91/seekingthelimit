<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Users\Interfaces;

use WP\Users\Entities\User;

interface UserFactoryInterface
{
    /**
     * @param $email
     * @param $password
     * @param bool|false $is_admin
     * @return User
     */
    public function create($email, $password, $is_admin = false);

    /**
     * @param User $user
     * @param $first_name
     * @param $last_name
     * @param $bio
     * @param $current_location
     * @param $image
     * @return User
     */
    public function edit(User $user, $first_name, $last_name, $bio, $current_location, $image);
}