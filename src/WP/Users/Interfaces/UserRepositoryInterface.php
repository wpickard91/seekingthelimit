<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Users\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use WP\Users\Entities\User;

interface UserRepositoryInterface
{
    /**
     * @param $id
     * @return User
     */
    public function find($id);

    public function withEmail($email);

    /**
     * @return Collection
     */
    public function all();
}