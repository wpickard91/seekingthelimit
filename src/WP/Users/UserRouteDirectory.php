<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Users;

use Illuminate\Routing\Router;
use WP\Http\Interfaces\RouteMapper;
use WP\Users\Controllers\UserController;

class UserRouteDirectory implements RouteMapper
{
    public function map(Router $router)
    {
        $router->post('/users/create', [
            'as' => 'users.postCreate',
            //'before' => ['auth', 'admin'],
            'uses' => UserController::class . '@postCreate'
        ]);

        $router->get('/users/create', [
            'as' => 'users.getCreate',
            //'before' => ['auth', 'admin'],
            'uses' => UserController::class . '@getCreate'
        ]);

        $router->get('/admin/users/', UserController::class . '@getAdminIndex');
        $router->get('/users/edit/{id}', UserController::class . '@getEdit');
        $router->post('/users/edit/{id}', UserController::class . '@postEdit');
        $router->get('/users/{id}', UserController::class . '@getUser');
    }
}