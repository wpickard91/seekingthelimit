<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Users\Controllers;

use Illuminate\Auth\Guard;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Http\Request;
use WP\Http\Controllers\BaseController;
use WP\Images\Interfaces\ImageFactoryInterface;
use WP\Users\Entities\User;
use WP\Users\Interfaces\UserFactoryInterface;
use WP\Users\Interfaces\UserRepositoryInterface;
use WP\Users\UserFactory;

class UserController extends BaseController
{
    /**
     * @var UserFactory
     */
    protected $userFactory;

    /**
     * @var Hasher
     */
    protected $hasher;

    /**
     * @var Guard
     */
    protected $guard;

    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * @var ImageFactoryInterface
     */
    protected $imageFactory;

    public function __construct(
        UserFactoryInterface $userFactory,
        UserRepositoryInterface $userRepo,
        ImageFactoryInterface $imageFactory,
        Hasher $hasher,
        Guard $guard
    ) {
        $this->userFactory = $userFactory;
        $this->userRepository = $userRepo;
        $this->imageFactory = $imageFactory;
        $this->hasher = $hasher;
        $this->guard = $guard;
    }

    public function getCreate(Request $request)
    {
        $active_user = $this->guard->user();
        return view('users.create', compact('active_user'));
    }

    public function getEdit(Request $request, $user_id) {
        $user = $this->userRepository->find($user_id);
        $active_user = $this->guard->user();
        $user_can_do_that = $active_user && $active_user->id == $user->id;

        if ($user_can_do_that) {
            return view('users.edit', compact('user'));
        } else {
            return redirect('/users/' . $user_id);
        }
    }

    public function getUser(Request $request, $user_id) {
        $user = $this->userRepository->find($user_id);

        return view('users.user', compact('user'));
    }

    public function getAdminIndex(Request $request) {
        $users = $this->userRepository->all();
        return view('users.adminIndex', compact('users'));
    }

    public function postCreate(Request $request) {
        if ($this->guard->user() && !$this->guard->user()->is_admin) {
            return redirect('/home')->with('message', 'You must be logged out to do that');
        }

        $this->validate($request, [
            'username' => 'unique:users',
        ]);

        $username = $request->input('username');
        $password = $this->hasher->make($request->input('password'));
        $is_admin = $request->has('is_admin') ? $request->input('is_admin') : false;

        /**
         * @var User
         */
        $user = $this->userFactory->create($username, $password, $is_admin);

        if ($this->guard->user() && $this->guard->user()->is_admin) {
            return redirect('/users/create')->with('created_user', $user);
        } else {
            $this->guard->login($user);
            return redirect('/home')->with('message', 'Welcome ' . $user->username . '!');
        }
    }

    public function postEdit(Request $request, $user_id) {
        $user = $this->userRepository->find($user_id);
        $active_user = $this->guard->user();
        $user_can_edit = $active_user && ($active_user->id == $user_id || $active_user->is_admin);

        if ($user_can_edit) {
            $first_name = $request->input('first_name');
            $last_name = $request->input('last_name');
            $bio = $request->input('bio');
            $current_location = $request->input('current_location');

            $uploaded_file = $request->file('image');
            $new_user_image = $uploaded_file
                ? $this->imageFactory->create($uploaded_file, $uploaded_file->getClientOriginalName())
                : null;
            $image_id = $new_user_image ? $new_user_image->id : $user->image_id;

            $user = $this->userFactory->edit($user, $first_name, $last_name, $bio, $current_location, $image_id);
            return view('users.edit', compact('user'));
        } else {
            return redirect('/home')->with('message', 'Nice try bub');
        }
    }

    public function logout(Request $request) {
        if ($this->guard->user()) {
            $this->guard->logout();
            return redirect('/home')->with('message', 'Successfully Logged out');
        } else {
            return redirect('/home')->with('message', 'You need to be logged in to do that');
        }
    }
}