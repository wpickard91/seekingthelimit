<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Contact\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Matching\ValidatorInterface;
use WP\Contact\Interfaces\ContactRequestFactoryInterface;
use WP\Contact\Interfaces\ContactRequestRepositoryInterface;
use WP\Http\Controllers\BaseApiController;
use Illuminate\Validation\Validator;

class ContactRequestApiController extends BaseApiController
{
    protected $plural = 'contact_requests';

    protected $singular = 'contact_request';

    /**
     * @var ContactRequestFactoryInterface
     */
    protected $contactFactory;

    /**
     * @var ContactRequestRepositoryInterface
     */
    protected $contactRepository;

    public function __construct(
        Request $request,
        ContactRequestFactoryInterface $contactFactory,
        ContactRequestRepositoryInterface $contactRepo
    ) {
        parent::__construct($request);

        $this->contactFactory = $contactFactory;
        $this->contactRepository = $contactRepo;
    }

    public function getContactRequest($id)
    {
        return $this->single($this->contactRepository->find($id));
    }

    public function getAll()
    {
        return $this->collection($this->contactRepository->all());
    }

    public function postCreate()
    {
        $this->validate($this->request, [
            'email' => 'required|email'
        ]);

        $email = $this->request->input('email');
        $name = $this->request->input('name');
        $body = $this->request->input('body');
        $new_posts = $this->request->has('new_posts') ? $this->request->input('new_posts') : false;

        return $this->single(
            $this->contactFactory->create($name, $email, $body, $new_posts)
        );
    }
}