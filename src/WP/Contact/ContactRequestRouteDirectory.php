<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Contact;

use Illuminate\Routing\Router;
use WP\Http\Interfaces\RouteMapper;
use WP\Contact\Controllers\ContactRequestApiController;

class ContactRequestRouteDirectory implements RouteMapper
{
    public function map(Router $router)
    {
        $router->group(
            ['prefix' => '/contactApi/v0'],
            function (Router $router) {
                $router->get('/', ContactRequestApiController::class . '@getAll');
                $router->get('/{id}', ContactRequestApiController::class . '@getContactRequest');
                $router->post('/create', ContactRequestApiController::class . '@postCreate');
            }
        );
    }
}