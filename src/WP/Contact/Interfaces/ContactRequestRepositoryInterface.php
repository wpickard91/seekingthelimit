<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Contact\Interfaces;

interface ContactRequestRepositoryInterface
{
    public function find($id);

    public function withEmail($email);

    public function newPosts();

    public function all();
}