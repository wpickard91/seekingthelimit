<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Contact\Interfaces;

interface ContactRequestFactoryInterface
{
    public function create($name, $email, $body, $new_posts = false);
}