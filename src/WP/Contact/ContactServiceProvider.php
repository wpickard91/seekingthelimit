<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Contact;

use Illuminate\Support\ServiceProvider;
use WP\Contact\Interfaces\ContactRequestFactoryInterface;
use WP\Contact\Interfaces\ContactRequestRepositoryInterface;
use WP\Contact\ContactRequestFactory;

class ContactServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            ContactRequestFactoryInterface::class,
            ContactRequestFactory::class
        );

        $this->app->bind(
            ContactRequestRepositoryInterface::class,
            ContactRequestRepository::class
        );
    }
}