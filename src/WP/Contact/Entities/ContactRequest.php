<?php

namespace WP\Contact\Entities;

use Illuminate\Database\Eloquent\Model;


/**
 * Class User
 * @package WP\Users\Entities
 *
 * @property String $username
 * @property String $password
 * @property Boolean $is_admin
 */
class ContactRequest extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contacts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'body', 'new_posts'];
}
