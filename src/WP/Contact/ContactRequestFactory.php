<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Contact;

use WP\Contact\Interfaces\ContactRequestFactoryInterface;
use WP\Contact\Entities\ContactRequest;

class ContactRequestFactory implements ContactRequestFactoryInterface
{
    public function create($name, $email, $body, $new_posts = false)
    {
        return ContactRequest::create([
            'name' => $name,
            'email' => $email,
            'body' => $body,
            'new_posts' => $new_posts
        ]);
    }
}