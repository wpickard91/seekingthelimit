<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Contact;

use WP\Contact\Interfaces\ContactRequestRepositoryInterface;
use WP\Contact\Entities\ContactRequest;

class ContactRequestRepository implements ContactRequestRepositoryInterface
{
    public function find($id)
    {
        return ContactRequest::find($id);
    }

    public function withEmail($email)
    {
        return ContactRequest::where('email', $email)->first();
    }

    public function newPosts()
    {
        return ContactRequest::where('new_posts', true)->get();
    }

    public function all()
    {
        return ContactRequest::all();
    }
}