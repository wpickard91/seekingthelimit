<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Support\Generators;

class LatinTextGenerator
{
    protected $latinBlock = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vehicula, dolor a pulvinar convallis, mauris elit tempus sem, vel laoreet sapien felis vitae tortor. Nulla non leo quis leo mattis ornare nec vehicula lectus. Vestibulum sit amet suscipit ligula. Vivamus dolor tortor, volutpat a auctor gravida, iaculis ac nulla. Cras efficitur accumsan felis, dapibus congue arcu pellentesque et. Suspendisse sed nunc sollicitudin, lobortis erat ut, faucibus libero. In malesuada, metus sit amet consectetur efficitur, eros lorem volutpat odio, eget convallis mauris ligula vitae turpis.

Suspendisse scelerisque dui eu augue suscipit imperdiet. Integer tempor lectus in erat placerat pharetra. Vestibulum placerat maximus neque at elementum. Sed facilisis, lectus eu iaculis dictum, magna sapien rutrum erat, eget finibus lectus tellus sed est. Vivamus congue rutrum sapien, ut gravida lacus tempor sed. Nam velit felis, dignissim volutpat dapibus a, volutpat et lacus. Aenean ullamcorper maximus sollicitudin. Pellentesque id dolor finibus magna imperdiet luctus. Suspendisse enim sem, ultricies in ipsum quis, feugiat laoreet leo. Mauris ut magna ac justo tristique dictum. Nam urna risus, laoreet in tempor non, scelerisque at tortor. Mauris pellentesque diam eget elit efficitur, non aliquam felis cursus. Pellentesque nec quam et metus blandit porta. Nunc quis molestie orci. In laoreet gravida dui vitae tempor.

Fusce turpis justo, blandit nec mi a, lacinia euismod urna. Donec venenatis ut mauris eget rutrum. Aenean vel neque vel orci interdum posuere sed vitae orci. Suspendisse facilisis bibendum magna ut accumsan. Vestibulum viverra augue vel commodo consectetur. Sed id augue eget orci efficitur bibendum. Maecenas faucibus nulla ac nibh imperdiet euismod. Pellentesque sollicitudin eros orci, tristique cursus magna pharetra quis. Nulla nec euismod mi. Proin non libero et libero tincidunt suscipit. Maecenas aliquet sed dui in viverra. Nulla bibendum elementum sem ac vestibulum. Vestibulum vehicula mauris nibh, vitae iaculis eros condimentum ac. Morbi eget mattis sem. Suspendisse non erat non augue lacinia viverra. Quisque venenatis euismod hendrerit.

Proin eu ligula pretium, ornare tellus rutrum, malesuada eros. Etiam fringilla mi lorem, in porta nibh vehicula ac. Nulla aliquam, dui faucibus facilisis pulvinar, magna odio condimentum arcu, lacinia feugiat leo dui vel nulla. Quisque nec sem a nibh gravida euismod eu eu sapien. Pellentesque ut justo ligula. Donec placerat, ligula vel dictum molestie, odio turpis lacinia purus, sed tincidunt ipsum libero a nulla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec quis massa vitae lorem bibendum finibus eget non tellus. Nunc sed ante et sapien euismod posuere a quis est. Nulla nec magna fermentum, eleifend enim non, bibendum lectus. Duis consectetur sit amet mauris nec fermentum. Cras sed ullamcorper mauris. Ut gravida tortor ac sem ultrices consectetur. Cras vel turpis dictum, tincidunt massa ut, vehicula augue.

Aliquam mattis eu leo vitae faucibus. Curabitur quis nibh id nisl aliquet tristique ac sit amet ex. Vestibulum luctus semper tincidunt. Integer id est erat. Cras ultrices, turpis eget dignissim aliquam, neque urna bibendum sapien, ut porta dui urna sed orci. Pellentesque lectus tortor, volutpat ac sagittis ut, consectetur quis sem. Ut a eleifend eros. Donec erat tellus, auctor eu semper iaculis, facilisis vel purus. Maecenas rhoncus, massa ornare viverra fringilla, nunc dolor tempor elit, id vulputate enim eros vel libero. Integer hendrerit facilisis interdum.";

    protected $words;

    public function __construct()
    {
        $this->words = explode(' ', $this->latinBlock);
    }

    public function generateString($numWords = 10)
    {
        $string = '';
        $max = count($this->words);
        for ($i = 0; $i < $numWords; $i++) {
            $string .= $this->words[rand(0, $max - 1)];
            $string .= ' ';
        }
        return $string;
    }
}