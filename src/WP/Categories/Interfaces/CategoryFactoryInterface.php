<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Categories\Interfaces;

use WP\Categories\Entities\Category;
use WP\Categories\Entities\PostCategory;
use WP\Images\Entities\Image;
use WP\Posts\Entities\Post;

interface CategoryFactoryInterface
{
    public function create($name, Image $image);

    public function edit(Category $category, $name, Image $image);

    /**
     * @param Post $post
     * @param Category $category
     * @return PostCategory
     */
    public function addPostToCategory(Post $post, Category $category);

    public function removePostFromCategory(Post $post, Category $category);

    public function deletePostCategory(PostCategory $postCategory);

}