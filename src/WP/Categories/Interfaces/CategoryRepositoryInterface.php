<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Categories\Interfaces;

use WP\Categories\Entities\Category;
use WP\Posts\Entities\Post;

interface CategoryRepositoryInterface
{
    public function all($with = []);

    public function find($categoryId);

    public function getPostCategory(Post $post, Category $category);

    public function findByName($name, $with = []);
}