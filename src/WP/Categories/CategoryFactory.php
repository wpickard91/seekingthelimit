<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Categories;

use WP\Categories\Entities\Category;
use WP\Categories\Entities\PostCategory;
use WP\Categories\Interfaces\CategoryFactoryInterface;
use WP\Categories\Interfaces\CategoryRepositoryInterface;
use WP\Images\Entities\Image;
use WP\Posts\Entities\Post;

class CategoryFactory implements CategoryFactoryInterface
{

    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;

    public function __construct(
        CategoryRepositoryInterface $categoryRepo
    ) {
        $this->categoryRepository = $categoryRepo;
    }

    public function create($name, Image $image)
    {
        return Category::firstOrCreate([
            'name' => $name,
            'image_id' => $image->id
        ]);
    }

    public function edit(Category $category, $name, Image $image = null) {
        $category->name = $name;
        if ($image) {
            $category->image_id = $image->id;
        }
        $category->save();
        return $category;
    }

    /**
     * @param Post $post
     * @param Category $category
     * @return PostCategory
     */
    public function addPostToCategory(Post $post, Category $category) {
        return PostCategory::firstOrCreate([
            'post_id' => $post->id,
            'category_id' => $category->id
        ]);
    }

    public function removePostFromCategory(Post $post, Category $category) {
        $postCategory = $this->categoryRepository->getPostCategory($post, $category);
        if ($postCategory) {
                $this->deletePostCategory($postCategory);
        }
    }

    public function deletePostCategory(PostCategory $postCategory) {
        $postCategory->delete();
    }
}