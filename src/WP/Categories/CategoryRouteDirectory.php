<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Categories;

use Illuminate\Routing\Router;
use WP\Http\Interfaces\RouteMapper;
use WP\Categories\Controllers\CategoriesApiController;
use WP\Categories\Controllers\CategoriesController;

class CategoryRouteDirectory implements RouteMapper
{
    public function map(Router $router)
    {
        $router->group(
            ['prefix' => '/categories'],
            function(Router $router) {
                $router->get('/', CategoriesController::class . '@getIndex');
                $router->get('/{name}', CategoriesController::class . '@getCategory');

            }
        );

        $router->group(
            ['prefix' => '/admin/categories'],
            function(Router $router) {
                $router->get('/create', CategoriesController::class . '@getCreate');
                $router->post('/create', CategoriesController::class . '@postCreate');
                $router->get('/', CategoriesController::class . '@getAdminIndex');
                $router->get('/edit/{id}', CategoriesController::class . '@getEdit');
                $router->post('/edit/{id}', CategoriesController::class . '@postEdit');
                $router->post('/delete/{id}', CategoriesController::class . '@postDelete');
            }
        );

        $router->group(
            ['prefix' => '/categories'],
            function(Router $router) {
                $this->mapApiRoutes($router);
            }
        );
    }

    protected function mapApiRoutes(Router $router)
    {
        $router->group(
            ['prefix' => '/categoriesApi/v0'],
            function (Router $router) {
                $router->get('/', CategoriesApiController::class . '@getIndex');
                $router->get('/find/{id}', CategoriesApiController::class . '@getSingle')
                    ->where('id', '[0-9]+');
                $router->post('/create', CategoriesApiController::class . '@postNew');
            }
        );
    }
}