<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Categories\Controllers;

use WP\Http\Controllers\BaseApiController;
use WP\Categories\Interfaces\CategoryRepositoryInterface;
use WP\Categories\Interfaces\CategoryFactoryInterface;
use Illuminate\Http\Request;

class CategoriesApiController extends BaseApiController
{
    /**
     * @var CategoryFactoryInterface
     */
    protected $categoryFactory;

    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;


    protected $plural = 'categories';

    protected $singular = 'category';

    public function __construct(
        Request $request,
        CategoryRepositoryInterface $categoryRepository,
        CategoryFactoryInterface $categoryFactory
    ) {
        parent::__construct($request);
        $this->categoryRepository = $categoryRepository;
        $this->categoryFactory = $categoryFactory;
    }

    public function getIndex()
    {
        return $this->collection($this->categoryRepository->all());
    }

    public function getSingle($categoryId)
    {
        return $this->single($this->categoryRepository->find($categoryId));
    }

    public function postNew()
    {
        $category = $this->categoryFactory->create($this->request->input('name'));

        return $this->single($category);
    }
}