<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Categories\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use WP\Categories\Interfaces\CategoryFactoryInterface;
use WP\Categories\Interfaces\CategoryRepositoryInterface;
use WP\Http\Controllers\BaseController;
use WP\Images\Interfaces\ImageFactoryInterface;
use WP\Images\Interfaces\ThumbnailFactoryInterface;

class CategoriesController extends BaseController
{
    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * @var ImageFactoryInterface
     */
    protected $imageFactory;

    /**
     * @var ThumbnailFactoryInterface
     */
    protected $thumbnailFactory;

    /**
     * @var CategoryFactoryInterface
     */
    protected $categoryFactory;

    public function __construct(
        CategoryRepositoryInterface $categoryRepo,
        CategoryFactoryInterface $categoryFactory,
        ImageFactoryInterface $imageFactory,
        ThumbnailFactoryInterface $thumbnailFactoryInterface
    ) {
        $this->categoryRepository = $categoryRepo;
        $this->categoryFactory = $categoryFactory;
        $this->imageFactory = $imageFactory;
        $this->thumbnailFactory = $thumbnailFactoryInterface;
    }

    public function handleUploadedImage(UploadedFile $image = null) {
        if ($image) {
            $filename = $image->getClientOriginalName();
            return $this->imageFactory->create($image, $filename);
        } else {
            return null;
        }
    }

    public function getIndex()
    {
        $categories = $this->categoryRepository->all();
        return view('categories.index', compact('categories'));
    }

    public function getCategory(Request $request, $name) {
        $category = $this->categoryRepository->findByName($name, ['posts']);
        return view('categories.category', compact('category'));
    }

    public function getAdminIndex() {
        $categories = $this->categoryRepository->all();
        return view('categories.adminIndex', compact('categories'));
    }

    public function getCreate()
    {
        return view('categories.create');
    }

    public function postEdit(Request $request, $category_id) {
        $category = $this->categoryRepository->find($category_id);
        $name = $request->input('name');
        $image = $this->handleUploadedImage($request->file('image'));

        if ($image) {
            $this->thumbnailFactory->create($image, 600);
        }

        $category = $this->categoryFactory->edit($category, $name, $image);

        return view('categories.edit', compact('category'));
    }

    public function getEdit(Request $request, $category_id) {
        $category = $this->categoryRepository->find($category_id);

        return view('categories.edit', compact('category'));
    }

    public function postCreate(Request $request) {
        $name = $request->input('name');
        $image = $this->handleUploadedImage($request->file('image'));

        if ($image) {
            $this->thumbnailFactory->create($image, 600);
        }

        $category = $this->categoryFactory->create($name, $image);

        return redirect('/admin/categories/edit/' . $category->id);
    }

    public function postDelete(Request $request, $category_id) {
        $this->categoryRepository->find($category_id)->delete();
        return redirect('/admin/categories');
    }
}