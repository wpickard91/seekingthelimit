<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Categories;

use Illuminate\Support\ServiceProvider;
use WP\Categories\Interfaces\CategoryRepositoryInterface;
use WP\Categories\Interfaces\CategoryFactoryInterface;
use WP\Categories\CategoryFactory;
use WP\Categories\CategoryRepository;

class CategoryServiceProvider extends ServiceProvider
{
    public function register()
    {
       $this->app->bind(
            CategoryFactoryInterface::class,
            CategoryFactory::class
        );

        $this->app->bind(
            CategoryRepositoryInterface::class,
            CategoryRepository::class
        );
    }
}