<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Categories\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use WP\Images\Entities\Image;
use WP\Posts\Entities\Post;

/**
 * Class Category
 * @package WP\Categories\Entities
 *
 * @property Integer $id
 * @property String $name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = ['name', 'image_id'];

    public function image() {
        return $this->belongsTo(Image::class);
    }

    public function posts() {
        return $this->belongsToMany(Post::class, 'post_categories', 'category_id', 'post_id');
    }
}