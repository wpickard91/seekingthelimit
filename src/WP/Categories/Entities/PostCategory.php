<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Categories\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use WP\Posts\Entities\Post;

/**
 * Class PostCategory
 * @package WP\Categories\Entities
 *
 * @property Integer $id
 * @property Integer $post_id
 * @property Integer $category_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class PostCategory extends Model
{
    protected $table = 'post_categories';

    protected $fillable = ['post_id', 'category_id'];

    public function post() {
        return $this->hasOne(Post::class);
    }

    public function category() {
        return $this->hasOne(Category::class);
    }
}