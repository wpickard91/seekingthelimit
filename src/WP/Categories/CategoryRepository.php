<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Categories;

use WP\Categories\Entities\Category;
use WP\Categories\Interfaces\CategoryRepositoryInterface;
use WP\Categories\Entities\PostCategory;
use WP\Posts\Entities\Post;

class CategoryRepository implements CategoryRepositoryInterface
{
    public function all($with = [])
    {
        return Category::with($with)
            ->get();
    }

    public function find($categoryId) {
        return Category::find($categoryId);
    }

    public function getPostCategory(Post $post, Category $category) {
        return PostCategory::where([
            'post_id' => $post->id,
            'category_id' => $category->id
        ])->firstOrFail();
    }

    public function findByName($name, $with = []) {
        return Category::where([
            'name' => $name
        ])
            ->with($with)
            ->firstOrFail();
    }
}