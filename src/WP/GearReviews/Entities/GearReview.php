<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\GearReviews\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
//use WP\Categories\Entities\Category;
use Illuminate\Support\Collection;
use WP\Categories\Entities\Category;
use WP\Images\Entities\Image;
use WP\Posts\Entities\Post;
use WP\Users\Entities\User;

/*use WP\Images\Entities\PostImage;
use WP\Images\Entities\Thumbnail;
use WP\Keywords\Entities\Keyword;
use WP\Keywords\Entities\KeywordReference;*/

/**
 * Class GearReview
 * @package WP\GearReviews
 *
 * @property Integer $gear_post_id
 * @property Integer $review_post_id
 * @property Integer $id
 * @property Integer $rating
 * @property Integer $user_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class GearReview extends Model
{

    protected $table = 'gear_reviews';

    protected $fillable = ['gear_post_id', 'review_post_id', 'review', 'user_id'];

    public function gearPost() {
        return $this->belongsTo(Post::class, 'gear_post_id');
    }

    public function reviewPost() {
        return $this->belongsTo(Post::class, 'review_post_id');
    }

    public function images() {
        return $this->belongsToMany(Image::class, 'gear_review_images', 'gear_review_id', 'image_id');
    }
}