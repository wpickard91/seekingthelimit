<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\GearReviews\Entities;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class GearDetails
 * @package WP\GearReviews\Entities
 * @property Integer $id
 * @property Integer $gear_review_id
 * @property String $body
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class GearDetails extends Model
{
    protected $table = 'gear_details';
    protected $fillbable = ['gear_review_id', 'body'];
}