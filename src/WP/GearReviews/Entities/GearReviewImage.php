<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\GearReviews\Entities;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class GearReviewImage
 * @package WP\GearReviews\Entities
 * @property Integer $id
 * @property Integer $gear_review_id
 * @property Integer $image_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class GearReviewImage extends Model
{
    protected $table = 'gear_review_images';
    protected $fillbable = ['gear_review_id', 'image_id'];
}