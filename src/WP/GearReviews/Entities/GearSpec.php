<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\GearReviews\Entities;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class GearSpec
 * @package WP\GearReviews\Entities
 * @property Integer $id
 * @property Integer $gear_review_id
 * @property String $label
 * @property String $body
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class GearSpec extends Model
{
    protected $table = 'gear_specs';
    protected $fillbable = ['gear_review_id', 'label', 'body'];
}