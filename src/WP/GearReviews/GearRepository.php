<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\GearReviews;

use WP\GearReviews\Entities\GearReview;
use WP\GearReviews\Interfaces\GearRepositoryInterface;

class GearRepository implements GearRepositoryInterface
{
    public function find($id) {
        return GearReview::find($id);
    }

    public function all() {
        return GearReview::all();
    }
}