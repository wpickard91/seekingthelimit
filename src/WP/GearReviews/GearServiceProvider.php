<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\GearReviews;

use Illuminate\Support\ServiceProvider;
use WP\GearReviews\Interfaces\GearRepositoryInterface;

class GearServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind(
            GearRepositoryInterface::class,
            GearRepository::class
        );

       /* $this->app->bind(
            PostFactoryInterface::class,
            PostFactory::class
        );*/
    }
}