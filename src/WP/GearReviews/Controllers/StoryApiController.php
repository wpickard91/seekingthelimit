<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Posts\Controllers;

use Illuminate\Http\Request;
use WP\Http\Controllers\BaseApiController;
use WP\Posts\Interfaces\StoryFactoryInterface;
use WP\Posts\Interfaces\StoryRepositoryInterface;

class StoryApiController extends BaseApiController
{
    protected $singular = 'story';

    protected $plural = 'stories';

    /**
     * @var StoryRepositoryInterface
     */
    protected $storyRepository;

    /**
     * @var StoryFactoryInterface
     */
    protected $storyFactory;

    public function __construct(
        Request $request,
        StoryRepositoryInterface $storyRepo,
        StoryFactoryInterface $storyFactory
    ) {
        parent::__construct($request);
        $this->storyRepository = $storyRepo;
        $this->storyFactory = $storyFactory;
    }

    public function getIndex()
    {
        return $this->collection($this->storyRepository->all());
    }

    public function getStory($story_id)
    {
        return $this->single($this->storyRepository->findStory($story_id));
    }

    public function getChapter($chapter_id)
    {
        $t = $this->plural;
        $this->singular = 'chapter';
        $response = $this->single($this->storyRepository->findChapter($chapter_id));
        $this->singular = $t;
        return $response;
    }

    public function getChaptersForStory($story_id)
    {
        // workaround to avoid having to make different class
        $t = $this->plural;
        $this->plural = "chapters";
        $response = $this->collection($this->storyRepository->findChaptersForStory($story_id));
        $this->plural = $t;
        return $response;
    }

    public function postCreate()
    {
        $post_ids = $this->request->input('post_ids');
        return $this->single($this->storyFactory->create($post_ids));
    }

    public function deleteStory($story_id)
    {
        $this->storyFactory->delete($story_id);
    }

    public function postNextChapter($this_chapter_id, $next_chapter_id)
    {
        return $this->storyFactory->assignNextChapter($this_chapter_id, $next_chapter_id);
    }

    public function postPrevChapter($this_chapter_id, $next_chapter_id)
    {
        return $this->storyFactory->assignPrevChapter($this_chapter_id, $next_chapter_id);
    }
}