<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\GearReviews\Controllers;

use WP\GearReviews\Interfaces\GearRepositoryInterface;
use WP\Http\Controllers\BaseController;

class GearController extends BaseController
{
    public function __construct (
        GearRepositoryInterface $gearRepositoryInterface
    ) {
        $this->gearRepository = $gearRepositoryInterface;
    }

    public function getIndex() {
        return $this->gearRepository->all();
    }

    public function getCreate() {
        return view('gearreviews.create');
    }
}