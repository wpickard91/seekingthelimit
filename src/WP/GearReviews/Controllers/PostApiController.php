<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Posts\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use WP\Http\Controllers\BaseApiController;
use WP\Images\Entities\Image;
use WP\Images\Interfaces\ImageRepositoryInterface;
use WP\Keywords\Interfaces\KeywordFactoryInterface;
use WP\Keywords\Interfaces\KeywordRepositoryInterface;
use WP\Posts\Entities\Post;
use WP\Posts\Interfaces\PostFactoryInterface;
use WP\Posts\Interfaces\PostRepositoryInterface;

class PostApiController extends BaseApiController
{
    /**
     * @var PostRepositoryInterface
     */
    protected $postRepository;

    /**
     * @var PostFactoryInterface
     */
    protected $postFactory;

    /**
     * @var ImageRepositoryInterface
     */
    protected $imageRepository;

    /**
     * @var KeywordFactoryInterface
     */
    protected $keywordFactory;

    /**
     * @var KeywordRepositoryInterface
     */
    protected $keywordRepository;

    public function __construct(
        Request $request,
        PostRepositoryInterface $postRepository,
        PostFactoryInterface $postFactory,
        ImageRepositoryInterface $imageRepository,
        KeywordFactoryInterface $keywordFactory,
        KeywordRepositoryInterface $keywordRepository
    ) {
        parent::__construct($request);
        $this->postFactory = $postFactory;
        $this->postRepository = $postRepository;
        $this->imageRepository = $imageRepository;
        $this->keywordFactory = $keywordFactory;
        $this->keywordRepository = $keywordRepository;

        $this->plural = 'posts';
        $this->singular = 'post';
    }

    public function paginate()
    {
        $take = $this->request->get('take');
        $start = $this->request->input('start');
        $end = $this->request->input('end');

        return $this->collection(
            $this->postRepository->paginate($take, $start, $end)
                ->load('images', 'mainImage', 'keywords', 'keywordReferences', 'category')
        );
    }

    public function getIndex()
    {
        $take = $this->getHowMany();

        $posts = $this->postRepository->newest($take, ['mainImage', 'keywordReferences', 'category']);

        return $this->collection($posts);
    }

    public function getCount()
    {
        return count($this->postRepository->all());
    }

    public function getInvertedIndex()
    {
        $path = base_path().DIRECTORY_SEPARATOR.
            'resources'.DIRECTORY_SEPARATOR.
            'data'.DIRECTORY_SEPARATOR.
            'invertedIndex.json';

        return (file_get_contents($path));
    }

    public function getPostTeasers()
    {
        $path = base_path() . DIRECTORY_SEPARATOR .
                'resources' . DIRECTORY_SEPARATOR .
                'data'      . DIRECTORY_SEPARATOR .
                'postTeasers.json';

        if (file_exists($path)) {
            return (file_get_contents($path));
        } else {
            return "";
        }
    }

    public function getSingle($post_id)
    {
        $post = $this->postRepository->find($post_id);

        if (!$post) {
            return null;
        }

        $post->load([
            'mainImage',
            'images',
            'images.thumbnail.image',
            'category',
            'keywords'
        ]);

        // hack the thumbnails because I can't figure out the model relation :(
        // 03/01/2016/
        /*
        $post->thumbnails = [];
        foreach ($post->images as $image) {
            if ($image->thumbnail) {
                $post->thumbnails[] = $image->thumbnail;
            }
        }*/

        return $this->single($post);
    }

    public function postCreate()
    {
        $title = $this->request->input('title');
        $description = $this->request->input('description');
        $body = $this->request->input('body');

        $post = $this->postFactory->create($title, $description, $body);

        return $this->single($post);
    }

    public function edit($post_id)
    {
        $post = $this->postRepository->find($post_id);
        $title = $this->request->input('title');
        $description = $this->request->input('description');
        $body = $this->request->input('body');

        $post = $this->postFactory->edit($post, $title, $description, $body);

        if ($this->request->has('main_image_id')) {
            $this->putMainImage($post_id, $this->request->input('main_image_id'));
        }

        return $this->single($post);
    }

    public function putImages($postId)
    {
        // Get the post
        $post = $this->postRepository->find($postId);
        if($this->request->has('images')) {
            $images = [];
            foreach($this->request->input('images') as $imageId) {
                $images[] = $this->imageRepository->find($imageId);
            }
            $this->postFactory->assignImagesToPost($images, $post);
            $post->load('images');
        }

        return $this->single($post);
    }

    public function putMainImage($post_id, $image_id)
    {
        $post = $this->postRepository->find($post_id);
        $image = $this->imageRepository->find($image_id);

        if($post && $image) {
            $this->postFactory->setMainImage($post, $image);
        }

        return $this->single($post);
    }

    public function deleteImage($post_id, $image_id)
    {
        $post = $this->postRepository->find($post_id);
        $image = $this->imageRepository->find($image_id);
        $this->postFactory->removeImage($post, $image);
    }

    public function putKeywords($post_id)
    {
        $post = $this->postRepository->find($post_id);

        $keyword_ids = $this->request->input('keyword_ids');

        foreach($keyword_ids as $keyword_id) {
            $this->keywordFactory->createReference(
                $this->keywordRepository->find($keyword_id),
                $post
            );
        }

        return $this->single($post);
    }

    public function putCategory($post_id)
    {
        $post = $this->postRepository->find($post_id);
        $post->category_id = $this->request->input('category_id');
        $post->save();
        return $this->single($post);
    }
}