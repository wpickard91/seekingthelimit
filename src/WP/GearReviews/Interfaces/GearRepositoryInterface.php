<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\GearReviews\Interfaces;

interface GearRepositoryInterface
{
    public function all();

    /**
     * @param Integer $id
     * @return mixed
     */
    public function find($id);
}