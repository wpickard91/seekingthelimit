<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\GearReviews;

use WP\Http\Interfaces\RouteMapper;
use Illuminate\Routing\Router;
use WP\GearReviews\Controllers\GearController;

class GearRouteDirectory implements RouteMapper
{
    public function map (Router $router)
    {
        $router->group(
            ['prefix' => '/admin/reviews'],
            function (Router $router) {
                $router->get('/create', GearController::class . '@getCreate');
            }
        );

        $router->group(
            ['prefix' => '/reviews'],
            function (Router $router) {
                $router->get('/', GearController::class . '@getIndex');
            }
        );
    }

}