<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Auth\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Auth\Guard;

class Admin
{
    /**
     * @var Guard
     */
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle (Request $request, Closure $next)
    {

        if ($this->auth->guest()) {
            return redirect('login');
        }

        return $next($request);
    }
}