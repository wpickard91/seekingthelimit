<?php

/**
 * @author Will Pickard <wpickard91@gmail.com>
 *
 **/
namespace WP\Auth\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Auth\Guard;

class Auth
{
    /**
     * @var Guard
     */
    protected $auth;

    public function __construct (Guard $guard)
    {
        $this->auth = $guard;
    }

    public function handle (Request $request, Closure $next)
    {
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorizde', 401);
            } else {
                return redirect()->guest('login');
            }
        }

        return $next($request);
    }
}