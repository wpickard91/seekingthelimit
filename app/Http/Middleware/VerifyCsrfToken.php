<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/admin/*',
        '/admin/posts/create',
        '/posts/postsApi/*',
        '/categories/categoriesApi/*',
        '/images/imageApi/*',
        '/posts/postsApi/*',
        '/images/imageApi/*',
        '/keywords/keywordsApi/*',
        '/api/images/upload'
    ];
}
