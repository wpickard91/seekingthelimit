<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \WP\Console\Commands\GeneratePosts::class,
        \WP\Console\Commands\AssignMainImage::class,
        \WP\Console\Commands\GenerateKeywords::class,
        \WP\Console\Commands\GenerateInvertedIndex::class,
        \WP\Console\Commands\GeneratePostTeaserFile::class,
        \WP\Console\Commands\GenerateGalleries::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();
    }
}
